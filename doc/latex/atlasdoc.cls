\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{atlasdoc}[16/03/2004 v1.0 ATLAS document class]
\LoadClass[a4paper]{book}

\usepackage[pdftex]{graphicx}
\usepackage{times}
\usepackage{color}
\usepackage{a4wide}
\usepackage{makeidx}
\usepackage{fancyhdr}
\usepackage{float}
\usepackage{alltt}
\usepackage{calc}
\usepackage{array}
\usepackage{titlesec}

\makeindex
\setcounter{tocdepth}{1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ATLAS stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Title page command
\newcommand{\atlastitlepage}{%
  \thispagestyle{plain}

  \includegraphics{atlas}
%  \parbox{0.5\linewidth}{\footnotesize \textsf{ATLAS}}

  \begin{flushright}
    \begin{minipage}[t]{0.9\linewidth}

      \textsf{\Huge \bfseries \mygroup\ \mysubgroup}
      
      \vspace{1cm}

      \rule{\linewidth}{1.5mm}

      \vspace{0.5cm}

      \textsf{\Huge \bfseries \mytitle}
    
      \vspace{1cm}
  
      Document Version: \myversion \\
      Document ID: \myid \\
      Document Date: \mydate \\
      Document Status: \mystatus \\

      \vspace{1cm}

      \rule{\linewidth}{1.5mm}

      \vspace{0.5cm}

      \textsf{Abstract}\\
      \myabstract
    
      \vspace{\baselineskip}

      Institutes and Authors:

      \vspace{\baselineskip}

      \myauthors

    \end{minipage}
  \end{flushright} 
}

\newcommand{\atlasrevisiontable}{%
  \chapter*{Document Change Record}
  \myrevisionTable 
}

%make section marks similar to the ones for an article format
%make section marks similar to the ones for an article format
\titleclass{\chapter}{straight}
\titleformat{\chapter}[hang]{\newpage\normalfont\LARGE\bfseries}{\thechapter}{1em}{}
\titlespacing{\chapter}{0pt}{0pt}{0pt}
\titleformat{\section}[hang]{\normalfont\LARGE\bfseries}{\thesection}{1em}{}
\titlespacing{\section}{0pt}{0pt}{0pt}
\titleformat{\subsection}[hang]{\normalfont\Large\bfseries}{\thesubsection}{1em}{}
\titlespacing{\subsection}{0pt}{0pt}{0pt}
\titleformat{\subsubsection}[hang]{\normalfont\large\bfseries}{}{0pt}{}
\titlespacing{\subsubsection}{0pt}{0pt}{0pt}

%%First set the page geometric layout as specified by the Connect forum
\usepackage[pdftex,a4paper,truedimen,includeheadfoot,vmargin=1.8cm,hmargin=2.4cm]{geometry}
\setlength{\headwidth}{\textwidth}

%%How the text is formatted
\usepackage{indentfirst}

\pagestyle{fancyplain}

%% The next commands make it easier for you to change the page headings,
%% just follow the obvious names and you will be fine. There's only one
%% major difference to make: 'normal' variants are those applied to the
%% normal text page, while the 'plain' ones are those applied to the 
%% pages where normally you don't have any fancy headings or footers, like
%% in TOC's, chapter or title pages (I've left all those empty, producing 
%% an equivalent default effect, but you may change them.

%% Normal (text) pages
\newcommand{\leftevennormalheader}{\footnotesize \textsf{\mygroup\ 
    \mysubgroup\\
    \rightmark}}
\newcommand{\leftoddnormalheader}{\footnotesize \textsf{\mygroup\ 
    \mysubgroup\\
    \rightmark}}
\newcommand{\rightevennormalheader}{\footnotesize \textsf{\mytitle\\ Version/Issue: \myversion/\myissue}}
\newcommand{\rightoddnormalheader}{\footnotesize \textsf{\mytitle\\ Version/Issue: \myversion/\myissue}}
\newcommand{\centerevennormalheader}{}
\newcommand{\centeroddnormalheader}{}

\newcommand{\leftevennormalfooter}{\footnotesize \bf \textsf{page \thepage}}
\newcommand{\leftoddnormalfooter}{\small\textsf{CERN}}
\newcommand{\rightevennormalfooter}{\small\textsf{CERN}}
\newcommand{\rightoddnormalfooter}{\footnotesize \bf \textsf{page \thepage}}
\newcommand{\centerevennormalfooter}{\footnotesize \textsf{\mystatus \\
  \mygroup\ \mysubgroup\ \mytitle}}
\newcommand{\centeroddnormalfooter}{\footnotesize \textsf{\mystatus \\
  \mygroup\ \mysubgroup\ \mytitle}}

%% Plain (chapter-like) pages
\newcommand{\leftevenplainheader}{}
\newcommand{\leftoddplainheader}{}
\newcommand{\rightevenplainheader}{}
\newcommand{\rightoddplainheader}{}
\newcommand{\centerevenplainheader}{}
\newcommand{\centeroddplainheader}{}

\newcommand{\leftevenplainfooter}{\footnotesize \bf \textsf{page \thepage}}
\newcommand{\leftoddplainfooter}{\small\textsf{CERN}}%%\includegraphics{cern}}
\newcommand{\rightevenplainfooter}{\small\textsf{CERN}}%%\includegraphics{cern}}
\newcommand{\rightoddplainfooter}{\footnotesize \bf \textsf{page \thepage}}
\newcommand{\centerevenplainfooter}{\footnotesize \textsf{\mystatus \\
  \mygroup\ \mysubgroup\ \mytitle}}
\newcommand{\centeroddplainfooter}{\footnotesize \textsf{\mystatus \\
  \mygroup\ \mysubgroup\ \mytitle}}

%% Define the headers of the page
%% - Inside brackets: 
%% 1) left argument: even numbered plain (title, contents) page
%% 2) right argument: even numbered normal (text) page
%% - Inside braces: 
%% 1) left argument: odd numbered plain page
%% 2) right argument: odd numbered normal page
\lhead[\fancyplain{\lefteventplainheader}{\leftevennormalheader}]%
        {\fancyplain{\leftoddplainheader}{\leftoddnormalheader}}
\rhead[\fancyplain{\righteventplainheader}{\rightevennormalheader}]%
        {\fancyplain{\rightoddplainheader}{\rightoddnormalheader}}
\chead[\fancyplain{\centereventplainheader}{\centerevennormalheader}]%
        {\fancyplain{\centeroddplainheader}{\centeroddnormalheader}}

%% Define the footers of the page (same analogy)
\lfoot[\fancyplain{\lefteventplainfooter}{\leftevennormalfooter}]%
        {\fancyplain{\leftoddplainfooter}{\leftoddnormalfooter}}
\rfoot[\fancyplain{\righteventplainfooter}{\rightevennormalfooter}]%
        {\fancyplain{\rightoddplainfooter}{\rightoddnormalfooter}}
\cfoot[\fancyplain{\centereventplainfooter}{\centerevennormalfooter}]%
        {\fancyplain{\centeroddplainfooter}{\centeroddnormalfooter}}

%% Makes the foot ruler thiker than the default (0pt)
\renewcommand{\headrulewidth}{0.8pt}

%% Makes the foot ruler thiker than the default (0pt)
\renewcommand{\footrulewidth}{0.8pt}

%% Makes the foot ruler thiker than the default (0pt)
\renewcommand{\plainfootrulewidth}{0.8pt}

%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
%\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

%% Defines to document date
\newcommand{\docdate}[1]{\newcommand{\mydate}{#1}}

%% Defines to which group the document belongs to
\newcommand{\docgroup}[1]{\newcommand{\mygroup}{#1}}

%% Defines to which subgroup the document belongs to
\newcommand{\docsubgroup}[1]{\newcommand{\mysubgroup}{#1}}

%% Defines which component is being described
\newcommand{\doctitle}[1]{\newcommand{\mytitle}{#1}}

%% Defines the current version of this document
\newcommand{\docversion}[1]{\newcommand{\myversion}{#1}}

%% Defines the current issue of this document
\newcommand{\docissue}[1]{\newcommand{\myissue}{#1}}

%% Defines the state of the current document. Maybe either Draft or Final
\newcommand{\docstatus}[1]{\newcommand{\mystatus}{#1}}

%% Defines the abstract for the document
\newcommand{\docabstract}[1]{\newcommand{\myabstract}{#1}}

%% Defines the author list
\newcommand{\docauthors}[1]{\newcommand{\myauthors}{#1}}

%% Defines the document id
\newcommand{\docid}[1]{\newcommand{\myid}{#1}}


%% This defines the revision table: MODIFY ONLY WHERE MARKED
\newcommand{\docrevisiontable}[1]{\newcommand{\myrevtablecontent}{#1}}
\newcommand{\myrevisionTable}{%
  \begin{tabular}{|c|c|c|l|} \hline
    \multicolumn{4}{|l|}{{\bf \textsf{Title:}} \mygroup\ \mysubgroup\ \mytitle } \\ \hline
    \multicolumn{4}{|l|}{{\bf \textsf{ID:}} \myid} \\ \hline
    {\bf \textsf{Version}}\hspace{0.5cm} & {\bf \textsf{Issue}}\hspace{0.5cm} & {\bf \textsf{Date}}\hspace{2cm} & {\bf \textsf{Comment}}\hspace{6cm} \\ \hline
    \myrevtablecontent
  \end{tabular}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following is to be compatible with Doxygen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\clearemptydoublepage}{\newpage{\pagestyle{empty}\cleardoublepage}}

\newenvironment{CompactList}
{\begin{list}{}{
  \setlength{\leftmargin}{0.5cm}
  \setlength{\itemsep}{0pt}
  \setlength{\parsep}{0pt}
  \setlength{\topsep}{0pt}
  \renewcommand{\makelabel}{}}}
{\end{list}}
\newenvironment{CompactItemize}
{
  \begin{itemize}
  \setlength{\itemsep}{-3pt}
  \setlength{\parsep}{0pt}
  \setlength{\topsep}{0pt}
  \setlength{\partopsep}{0pt}
}
{\end{itemize}}
\newcommand{\PBS}[1]{\let\temp=\\#1\let\\=\temp}
\newlength{\tmplength}
\newenvironment{TabularC}[1]
{
\setlength{\tmplength}
     {\linewidth/(#1)-\tabcolsep*2-\arrayrulewidth*(#1+1)/(#1)}
      \par\begin{tabular*}{\linewidth}
             {*{#1}{|>{\PBS\raggedright\hspace{0pt}}p{\the\tmplength}}|}
}
{\end{tabular*}\par}
\newcommand{\entrylabel}[1]{
   {\parbox[b]{\labelwidth-4pt}{\makebox[0pt][l]{\textbf{#1}}\\}}}
\newenvironment{Desc}
{\begin{list}{}
  {
    \settowidth{\labelwidth}{40pt}
    \setlength{\leftmargin}{\labelwidth}
    \setlength{\parsep}{0pt}
    \setlength{\itemsep}{-4pt}
    \renewcommand{\makelabel}{\entrylabel}
  }
}
{\end{list}}
\newenvironment{Indent}
  {\begin{list}{}{\setlength{\leftmargin}{0.5cm}}
      \item[]\ignorespaces}
  {\unskip\end{list}}
\setlength{\parindent}{0cm}
\setlength{\parskip}{0.2cm}
\addtocounter{secnumdepth}{1}
\sloppy

