//$Id$

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  File: EthSequentialDataChannel.cpp					*/
/*  Author: M.Joos							*/
/*									*/
/*** C 2005 - Ecosoft: Made from at least 80% recycled source code ******/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>  
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>  // struct sockaddr_in ...
#include <netinet/tcp.h> // TCP_NODELAY
#include <sys/socket.h>

#include <sys/ioctl.h>

#include "ROSModules/EthSequentialDataChannel.h"
#include "ROSModules/ModulesException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;

/******************************************************************************************/
EthSequentialDataChannel::EthSequentialDataChannel(u_int id, u_int configId,
				                   u_int rolPhysicalAddress,
				                   bool usePolling,
				                   DFCountedPointer<Config> configuration,
                                                   EthSequentialDataChannelInfo* info) :
   SequentialDataChannel(id, configId, rolPhysicalAddress, usePolling, configuration, info),
   m_fileDescriptor(0)
/******************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::constructor: Entered");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::constructor: id = " << id);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::constructor: rolPhysicalAddress = " << rolPhysicalAddress);
  m_statistics = info;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::constructor: m_statistics = " << m_statistics);

  m_physaddr = rolPhysicalAddress;

  // Set up the local address
  sockaddr_in sin;
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family      = AF_INET;
  sin.sin_addr.s_addr = htonl(INADDR_ANY);   
  sin.sin_port        = rolPhysicalAddress;
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::constructor: sin.sin_port = " << sin.sin_port);
  sin.sin_port        = htons(sin.sin_port);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::constructor: Address family = " << sin.sin_family << " Address = " << sin.sin_addr.s_addr << " Address port = " << sin.sin_port);

  int data = socket(AF_INET, SOCK_STREAM, 0);
  if (data == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::constructor: Error from socket()");
    CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_SOCKET, "port = " << sin.sin_port << ", channel id = " << id << ", configId = " << configId);
    throw(ex1);
  }
  if (SetSocketOptions(data))
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::constructor: Error from SetSocketOptions()");
    CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_SSO, "port = " << sin.sin_port << ", channel id = " << id << ", configId = " << configId);
    throw(ex1);
  }
  
  // Bind local address
  if (bind(data, (struct sockaddr *)&sin, sizeof(sin)) == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::constructor: Error from bind()");
    CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BIND, "port = " << sin.sin_port << ", channel id = " << id << ", configId = " << configId);
    throw(ex1);
  }
  
  // Listen for connections on local address  
  if (listen (data, SOMAXCONN) == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::constructor: Error from listen()");
    CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_LISTEN, "port = " << sin.sin_port << ", channel id = " << id << ", configId = " << configId);
    throw(ex1);
  }
  
  m_socketDescriptor = data;
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::constructor: TCP connection opened locally. m_socketDescriptor = " << data);

  m_usePolling = usePolling;
  m_triggerReady = false;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::constructor: Done");
}


/***************************************************/
EthSequentialDataChannel::~EthSequentialDataChannel() 
/***************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::destructor: Called");
}


/**********************************************************************************************/
int EthSequentialDataChannel::getNextFragment(u_int* buffer, int max_size, u_long /*pciAddress*/)
/**********************************************************************************************/
{
  u_int local_errno, nrecv = 0;
  char buf[4];                  // place to store the msg size
 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::getNextFragment: Entered");
  // First read the size of the message
  while(nrecv < sizeof(int)) 
  {
    int nread = read(m_fileDescriptor, buf + nrecv, sizeof(int) - nrecv); 
    local_errno = errno;
    if (nread <= 0) 
    {
      if (local_errno != EINTR)
      {
        DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::getNextFragment: Error from first read()");
        CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BADSIZE, "errno = " << local_errno << ", nrecv = " << nrecv);
        throw(ex1);
      }
    }
    else 
      nrecv += nread;
  }

  // Get the size of the ROD/Full fragment
  uint32_t* lbuf=(uint32_t*) buf;
  uint32_t msg_size=ntohl(*lbuf);
  // Now receive the data
  if ((u_int)max_size < msg_size)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::getNextFragment: ROD/FULL fragment from ROL " << m_physaddr << " will be truncated");
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::getNextFragment: size of fragment = " << msg_size);
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::getNextFragment: max. allowed size = " << max_size);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, ALL_TRUNCATE, "");
    throw(ex1);
  }

  u_int msg_nrecv = 0;
  while(msg_nrecv < msg_size) 
  {
    int nread = read(m_fileDescriptor, (void *)((u_long)buffer + msg_nrecv), msg_size - msg_nrecv);
    if (nread <= 0) 
    {
      if (errno != EINTR) 
      {
        DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::getNextFragment: Error from second read()");
        CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BADLINK, "");
        throw(ex1);
      }	
    }
    else 
      msg_nrecv += nread;
  }

  m_statistics->fragments_input++;  

  DEBUG_TEXT(DFDB_ROSFM, 10, "EthSequentialDataChannel::getNextFragment: Fragment of " << msg_nrecv << " bytes received");
  DEBUG_TEXT(DFDB_ROSFM, 10, "EthSequentialDataChannel::getNextFragment: First word = 0x" << std::hex << *buffer << std::dec << " at " << buffer);
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::getNextFragment: Done");
  return(msg_nrecv);
}


/**************************************/
int EthSequentialDataChannel::poll(void)
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::poll: Entered");
  if (m_triggerReady == false)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::poll: Done(1)");
    return(0);
  }
    
  if(PollEth()) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::poll: Done(2)");
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::poll: Data waiting");
    return(1);
  }
  else 
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::poll: Done(3)");
    return(0);
  }
}


/*****************************************/
void EthSequentialDataChannel::stopEB(void)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::stopEB: Entered");
  m_triggerReady = false;

  cleanupLink();
 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::stopEB: Done");
}


/******************************************/
void EthSequentialDataChannel::connect(void)
/******************************************/
{
  struct sockaddr_in sout;
  socklen_t slen;
 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::connect: Entered");

  slen = (socklen_t)sizeof(struct sockaddr_in);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::connect: calling accept for m_socketDescriptor = " << m_socketDescriptor);
  int data = accept(m_socketDescriptor, (struct sockaddr *)&sout, &slen);
  if (data == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::connect: accept failed");
    CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_ACCEPT,"");
    throw(ex1);
  }
  m_fileDescriptor = data; 
  
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::connect: sd " << m_socketDescriptor << " converted to fd " << m_fileDescriptor);
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::connect: Done");
}


/*********************************************/
void EthSequentialDataChannel::disconnect(void)
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::disconnect: Entered");
  if (close(m_fileDescriptor) == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::disconnect: Error from close()");
    CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_CLOSE,"");
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::disconnect: Done");
}


/********************************************/
void EthSequentialDataChannel::configure(void)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::configure: Called");
}


/**********************************************/
void EthSequentialDataChannel::unconfigure(void)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::unconfigure: Entered");

  if (close(m_socketDescriptor) == -1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::unconfigure: Error from close()");
    CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_CLOSE,"");
    throw(ex1);
  }
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::unconfigure: Done");
}


/************************************************/
void EthSequentialDataChannel::prepareForRun(void)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::prepareForRun: Entered");

  cleanupLink();
  m_statistics->fragments_input = 0;

  m_triggerReady = true;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::prepareForRun: Done");
}  
 
/**************************************/
void EthSequentialDataChannel::probe()
/**************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::probe: called");

  SequentialDataChannel::probe();

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::probe: done");
}

/****************************************************************/
  ISInfo*  EthSequentialDataChannel::getISInfo()
/****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::getIsInfo: called");
    return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::getIsInfo: done");
}
 

/********************************************/
void EthSequentialDataChannel::clearInfo(void)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::clearInfo: Entered");
  SequentialDataChannel::clearInfo();

  m_statistics->fragments_input = 0;
  DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::clearInfo: Done");
}


/**********************************************/
void EthSequentialDataChannel::cleanupLink(void)
/**********************************************/
{
  char p;
  int i = 0;

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::cleanupLink: Entered");

  while (PollEth())   {
    read(m_fileDescriptor, &p, 1);
    i++;
  }
  std::cout << " EthSequentialDataChannel::cleanupLink flushed " << i << " bytes." << std::endl;

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::cleanupLink: Done");
}


/****************************************************/
int EthSequentialDataChannel::SetSocketOptions(int fd)
/****************************************************/
{
  int sockopt = 0;
  int SOCKET_ERROR = -1;
  static const int c_so_rcvbuf = 256 * 1024; // limit set by Linux!
  static const int c_so_sndbuf = 256 * 1024; // limit set by Linux!

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::SetSocketOptions: Entered");

  // setsockopt SO_REUSEADDR - enable/disable local address reuse
  sockopt = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt1");
    return -1;
  }

  // setsockopt SO_SNDBUF - set buffer size for output
  sockopt = c_so_sndbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt2");
    return -1;
  }
 
  // setsockopt SO_RCVBUF - set buffer size for input
  sockopt = c_so_rcvbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt3");
    return -1;
  }

  // setsockopt TCP_NODELAY - to defeat the packetization algorithm
  sockopt = 0; // 0 to enable Nagle algorithm
  if (setsockopt(fd, getprotobyname("tcp")->p_proto, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt4");
    return -1;
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::SetSocketOptions: Done OK");
  return 0;
}


/*****************************************/
int EthSequentialDataChannel::PollEth(void)        
/*****************************************/
{     
  struct timeval c_select_timeout = {0, 0};  //disable time out; just poll

  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::PollEth: Entered");
  FD_ZERO(&m_recv_fds);
  FD_SET(m_fileDescriptor, &m_recv_fds);
  int sel = select(m_fileDescriptor + 1, &m_recv_fds, NULL, NULL, &c_select_timeout);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::PollEth: back from select with sel = " << sel);

  if (sel == 0)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::PollEth: no data waiting");
    return(0);
  }
  else if (sel > 0) 
  {
    if (FD_ISSET(m_fileDescriptor, &m_recv_fds))
    {
      DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialDataChannel::PollEth: data waiting");
      int length;
      int status=ioctl(m_fileDescriptor, FIONREAD, &length);
      if (status) {
         std::cout << "Error reading input size\n";
      }
      if (length) {
         //         std::cout << length << " bytes availabe for reading\n";
         return(1);
      }
      else {
         std::cout << "Nothing to read, eof??\n";
         m_triggerReady=false;
         CREATE_ROS_EXCEPTION(ex0,ModulesException,ETH_SELECT,"fd flagged but no data avialable probable EOF");
         ers::error(ex0);
         return(0);
      }
    }
    else
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::PollEth: Error 1 in select");
      CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT,"");
      throw(ex1);
    }
  }
  else // (select < 0)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::PollEth: Error 2 in select");
    CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT,"");
    throw(ex1);
  }
}

