//$Id$
/************************************************************************/
/*									*/
/* File             : EmulatedSingleFragmentReadoutModule.cpp	        */
/*									*/
/* Authors          : G. Crone, B.Gorini,  J.Petersen ATD/CERN		*/
/*									*/
/********* C 2007 - ROS/RCD  ********************************************/

#include <unistd.h>
#include <iostream>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/EmulatedSingleFragmentDataChannel.h"
#include "ROSModules/EmulatedSingleFragmentReadoutModule.h"


using namespace ROS;


/************************************************************************/
EmulatedSingleFragmentReadoutModule::EmulatedSingleFragmentReadoutModule()  :
   m_numberOfDataChannels(0)
/************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::constructor: Entered");
}


/*************************************************************************/
EmulatedSingleFragmentReadoutModule::~EmulatedSingleFragmentReadoutModule() noexcept
/*************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


// setup gets parameters from configuration
/*************************************************************************************/
void EmulatedSingleFragmentReadoutModule::setup(DFCountedPointer<Config> configuration) 
/*************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::setup: Entered");

  m_configuration = configuration;

  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSingleFragmentReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);

  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
  }

  for (int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_id[chan] = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSingleFragmentReadoutModule::setup: chan = "
                                << chan << "  id = " << m_id[chan] << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
  }
}


/*&*****************************************************/
void EmulatedSingleFragmentReadoutModule::configure(const daq::rc::TransitionCmd&)
/*******************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::configure: Entered");

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {

    // chan == the Config Id
    EmulatedSingleFragmentDataChannel *channel = new EmulatedSingleFragmentDataChannel(
						 m_id[chan],
					         chan,
						 m_rolPhysicalAddress[chan],
						 m_configuration);
    m_dataChannels.push_back(channel);
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::configure: Done");
}



/***************************************************/
void EmulatedSingleFragmentReadoutModule::clearInfo()
/***************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentReadoutModule::clearInfo: Entered. Empty Function!");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createEmulatedSingleFragmentReadoutModule();
}
ReadoutModule* createEmulatedSingleFragmentReadoutModule()
{
  return (new EmulatedSingleFragmentReadoutModule());
}
