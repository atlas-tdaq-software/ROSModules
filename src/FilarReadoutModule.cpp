//$Id$
/************************************************************************/
/*									*/
/* File             : FilarReadoutModule.cpp				*/
/*									*/
/* Author           : Markus Joos, CERN					*/
/*									*/
/* The purpose of this class is (by means of a thread) to input         */
/* real event fragments via a FILAR module into a number of event  	*/
/* buffers, one per ReadOut link. It will be started and controlled     */ 
/* from the IOManager    						*/
/*									*/
/********* C 2004 - The software with that certain something ************/

#include "ROSModules/FilarReadoutModule.h"
#include "ROSModules/ModulesException.h"
#include "ROSCore/PCMemoryDataChannel.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSfilar/filar.h"

using namespace ROS;


/**************************************/
FilarReadoutModule::FilarReadoutModule() 
/**************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::constructor: Entered");
  char mutexName[]="REQUESTMUTEX";
  m_fastAllocateMutex = DFFastMutex::Create(mutexName);
}


/***************************************/
FilarReadoutModule::~FilarReadoutModule() noexcept
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::destructor: Entered");
  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;
  }

  m_fastAllocateMutex->destroy();
}


/********************************************************************/
void FilarReadoutModule::setup(DFCountedPointer<Config> configuration) 
/********************************************************************/
{ 
  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: m_numberOfDataChannels = " << m_numberOfDataChannels);

  // Initialize the vectors
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
    {
      m_eventInputManager.push_back(0);
      m_memoryPool.push_back(0);
      m_filar_device.push_back(-1);
      m_statistics.rod_L1id_prev.push_back(0);
      m_rod_L1id_ecr.push_back(0);
      m_ecr.push_back(0);
      m_mem_page.push_back(0);
      m_statistics.fragments_input.push_back(0);
      m_statistics.stat_head.push_back(0);
      m_statistics.stat_size.push_back(0);
      m_statistics.stat_l1id.push_back(0);
    }

  //In case of the filar all memoryPool[n]PageSize parameters must be identical
  int refsize = configuration->getInt("memoryPoolPageSize", 0);
  
  for(int chan = 1; chan < m_numberOfDataChannels; chan++)
  {
    int ppsize = configuration->getInt("memoryPoolPageSize", chan);
    if (ppsize != refsize)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::setup: Memory pool page sizes are not all identical");
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::setup: refsize = " << refsize << " current size = " << ppsize << " index = " << chan);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_PSIZE,"");
      throw(ex1);
    }
  }

  //In order to save a parameter we derive the FILAR page size from the page size of the Pool
  if      (refsize < 256) {
    CREATE_ROS_EXCEPTION(ex1, ModulesException, FIL_ILLSIZE, "");
    throw(ex1);
  }
  else if (refsize < 1024)    m_filarpagesize = 0;
  else if (refsize < 2048)    m_filarpagesize = 1;
  else if (refsize < 4096)    m_filarpagesize = 2;
  else if (refsize < 16384)   m_filarpagesize = 3;
  else if (refsize < 65536)   m_filarpagesize = 4;
  else if (refsize < 262144)  m_filarpagesize = 5;
  else if (refsize < 4194296) m_filarpagesize = 6;
  else                        m_filarpagesize = 7;
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: m_filarpagesize = " << m_filarpagesize);

  if ((refsize > 256) && (refsize != 1024) && (refsize != 2048) && (refsize != 4096) &&
                         (refsize != 16384) && (refsize != 65536) &&
                         (refsize != 262144) && (refsize != 4194296)) {
    CREATE_ROS_EXCEPTION(ex10, ModulesException, FIL_PSIZE2, "FilarReadoutModule::setup: The FILAR page size was set to "<< m_filarpagesize << " (encoded) and is therefore smaller than the memory pool page size. This may cause you trouble.");
    ers::warning(ex10);
  }

  m_triggerQueueFlag = configuration->getBool("triggerQueue");
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: triggerQueue = " << m_triggerQueueFlag); 
  if (m_triggerQueueFlag) 
  {
    m_triggerQueueSize = configuration->getInt("triggerQueueSize");
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: triggerQueueSize = " << m_triggerQueueSize);

    m_triggerQueueUnblockOffset = configuration->getInt("triggerQueueUnblockOffset");
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: triggerQueueUnblockOffset = " << m_triggerQueueUnblockOffset);

    m_inputToTriggerQueue = TriggerInputQueue::instance();
    m_inputToTriggerQueue->setSize(m_triggerQueueSize);
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: Q pointer = " << m_inputToTriggerQueue << " Q size = " << m_triggerQueueSize);
  }

  m_fastTestMode = configuration->getInt("FastTestMode");
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: m_fastTestMode = " << m_fastTestMode);

  m_bswap = configuration->getInt("ByteSwapping");
  
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    u_int id = configuration->getInt("channelId", chan);
    u_int rolPhysicalAddress = configuration->getInt("ROLPhysicalAddress", chan);
    PCMemoryDataChannel *channel = new PCMemoryDataChannel(id, chan, rolPhysicalAddress, configuration);
    m_dataChannels.push_back(channel);

    //Get object pointers from the data channel
    m_eventInputManager[chan] = channel->getEIM(); 
    m_memoryPool[chan] = m_eventInputManager[chan]->getMemoryPool();
  
    m_filar_device[chan] = rolPhysicalAddress;
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: id = " << id);
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: rolPhysicalAddress (Filar channel 0..N) = " << m_filar_device[chan]);

    err_type r_code = FILAR_Open();
    if (r_code) 
    { 
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::setup: Error from FILAR_Open");
      rcc_error_string(m_rcc_err_str, r_code);
      int slen = strlen(m_rcc_err_str);
      sprintf(m_rcc_err_str + slen - 1, "\nThis error came from ROL with physical address = %d\n", m_filar_device[chan]); 
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_OPEN,m_rcc_err_str);
      throw(ex1);
    }

    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: Resetting FILAR " << (m_filar_device[chan]) << "  chan = " << chan);
    r_code = FILAR_Reset(m_filar_device[chan]);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::setup, Error from FILAR_Reset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_RESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: FILAR_Reset called for rol = " << (m_filar_device[chan]));

    r_code = FILAR_LinkReset(m_filar_device[chan]);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::setup: Error from FILAR_LinkReset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_LINKRESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::setup: FILAR_LinkReset called for rol = " << (m_filar_device[chan]));

    m_statistics.rod_L1id_prev[chan] = INIT_L1ID;
    m_rod_L1id_ecr[chan] = INIT_L1ID;
    m_ecr[chan] = 0;
  }
}


/****************************************/
void FilarReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::unconfigure: Entered");
  
  err_type r_code = FILAR_Close();
  if (r_code) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::unconfigure, Error from FILAR_Close");
    rcc_error_string(m_rcc_err_str, r_code);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_CLOSE,m_rcc_err_str);
    throw(ex1);
  }

  if (m_triggerQueueFlag) 
  {
    m_inputToTriggerQueue->destroy();
  }

  m_eventInputManager.clear();
  m_memoryPool.clear();
  m_filar_device.clear();
  m_statistics.rod_L1id_prev.clear();
  m_rod_L1id_ecr.clear();
  m_ecr.clear();
  m_mem_page.clear();
  m_statistics.fragments_input.clear();
  m_statistics.stat_head.clear();
  m_statistics.stat_size.clear();
  m_statistics.stat_l1id.clear();
}

    
/**************************************/
void FilarReadoutModule::configure(const daq::rc::TransitionCmd&)
/**************************************/
{

  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::configure: Entered");

  //According the the 3rd ROD workshop the RODs should deliver big-endian data. It has turned out that several RODs
  //did not respect this convention and provide the data in little endian byte ordering.
  //As this could not be changed quickly enough we finally decided to make the byte ordering configurable.
  //If 0x1000 is added to the parameter "rolPhysicalAddress" in the database it will tell the S/W below
  //not to enable the byte swapping. Byte swapping can only be enabled globally for all channels.

  FILAR_config_t fconfig;  

  for(int loop = 0; loop < MAXROLS; loop++)
    fconfig.enable[loop] = 0; 

  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::configure: Now enabling " << m_numberOfDataChannels << " channels");
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::configure: m_filar_device[" << chan << "] = " << m_filar_device[chan]);
    if (m_filar_device[chan] != -1)
    {
      fconfig.enable[m_filar_device[chan]] = 1; 
      DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::configure: ROL " << chan << " exists");
    }
  }

  if (m_bswap)
  {
    fconfig.bswap = 1;
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::configure: Byte swapping enabled");
  }
  else
  {      
    fconfig.bswap = 0;
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::configure: Byte swapping disabled");
  }   

  fconfig.psize = m_filarpagesize;
  fconfig.wswap = 0;  //Not yet supported
  fconfig.scw = 0xb0f0000;    //Not yet used
  fconfig.ecw = 0xe0f0000;    //Not yet used      

  err_type r_code = FILAR_Init(&fconfig);
  if (r_code) 
  { 
    DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::configure: Error from FILAR_Init");
    rcc_error_string(m_rcc_err_str, r_code);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_INIT,m_rcc_err_str);
    throw(ex1);
  }
}



/******************************************/    
void FilarReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/******************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::prepareForRun: Entered");
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    u_int r_code = FILAR_Reset(m_filar_device[chan]);
    if (r_code)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::prepareForRun: Error from FILAR_Reset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_RESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::prepareForRun: FILAR_Reset called for rol = " << (m_filar_device[chan]));

    r_code = FILAR_LinkReset(m_filar_device[chan]);
    if (r_code)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::prepareForRun: Error from FILAR_LinkReset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_LINKRESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::prepareForRun: FILAR_LinkReset called for rol = " << (m_filar_device[chan]));

    m_statistics.fragments_input[chan] = 0;
    m_statistics.stat_head[chan] = 0;
    m_statistics.stat_size[chan] = 0;
    m_statistics.stat_l1id[chan] = 0;
  }
  m_statistics.yield = 0;
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::prepareForRun: starting FILAR input thread");
  startExecution(); 
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::prepareForRun: FILAR input thread started");
}
    

/***********************************/
void FilarReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::stopGathering: Entered");

  //Stop the thread 
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::stopGathering: stopping Dummy input thread");
  stopExecution();
  waitForCondition(DFThread::TERMINATED, 1);	 // FIXME: handle timeout
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::stopGathering: Dummy input thread stopped");

  if (m_triggerQueueFlag)
    m_inputToTriggerQueue->reset();

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_statistics.rod_L1id_prev[chan] = INIT_L1ID;
    m_rod_L1id_ecr[chan] = INIT_L1ID;
    m_ecr[chan] = 0;

    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::stopGathering: Resetting FILAR " << (m_filar_device[chan]) << "  chan = " << chan);
  
    u_int r_code = FILAR_Reset(m_filar_device[chan]);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::stopGathering: Error from FILAR_Reset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_RESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::stopGathering: FILAR_Reset called for rol = " << (m_filar_device[chan]));

    r_code = FILAR_LinkReset(m_filar_device[chan]);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::stopGathering: Error from FILAR_LinkReset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_LINKRESET,m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::stopGathering: FILAR_LinkReset called for rol = " << (m_filar_device[chan]));
    DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::stopGathering: done");
  }

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_eventInputManager[chan]->reset();
  }
}


/**********************************/
void FilarReadoutModule::clearInfo()
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::clearInfo: Entered");
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_statistics.rod_L1id_prev[chan] = INIT_L1ID;
    m_statistics.fragments_input[chan] = 0;
    m_statistics.stat_head[chan] = 0;
    m_statistics.stat_size[chan] = 0;
    m_statistics.stat_l1id[chan] = 0;
  }

  m_statistics.yield = 0;
}


/****************************************************************/
bool FilarReadoutModule::FragmentsAreThere(u_int l1id, u_int chan)
/****************************************************************/
{
  // Possible scenarios
  // ==================
  // 1) On channel has not yet received the first fragment
  // -> We retuen false
  //
  // 2) No channel has passed the ECR point
  // -> We compare the L1IDs in m_statistics.rod_L1id_prev    
  //
  // 3) The current channel has not passed the ECR point but some other channels have  
  // -> We compare the L1IDs in m_statistics.rod_L1id_prev which has the most recent pre-ECR L1IDs
  // -> The most recent past-ECR L1ID of a past-ECR channel is not interesting
  //
  // 4) The current channel has passed the ECR but (at least) one other channel is still behind
  // -> The event cannot be complete and we return false 
  //
  // 5) The current channel and all other channels have passed the ECR
  // -> This should not be possible as the m_ecr vector will be reset immediately if this 
  //    situation is detected by the run() function

  std::vector<u_int>::iterator max_it = max_element(m_statistics.rod_L1id_prev.begin(), m_statistics.rod_L1id_prev.end());
  if (*max_it == INIT_L1ID)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::FragmentsAreThere: INIT_L1ID found. Returning false");
    return false;
  }
      
  if (m_ecr[chan] == 1)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::FragmentsAreThere: Channel has already passed ECR point. Returning false");
    return false;
  }
  
  std::vector<u_int>::iterator min_it = min_element(m_statistics.rod_L1id_prev.begin(), m_statistics.rod_L1id_prev.end());
  if (l1id == *min_it)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::FragmentsAreThere: Event complete. Returning true");
    return true;
  }
  else
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::FragmentsAreThere: Event not yet complete. Returning false");
    return false;  
  }
}


/********************************/
void FilarReadoutModule::run(void)
/********************************/
{
  bool busy;
  FILAR_out_t fout;
  u_int eslim = (sizeof(RODFragment::RODHeader) + sizeof(RODFragment::RODTrailer))/sizeof(int);
 
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::run: Entered");

  while(1)
  {
    //Flush the FILAR to make sure that all events that have arrived can also be seen
    err_type r_code = FILAR_Flush();
    if (r_code)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run, Error from FILAR_Flush");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_FLUSH,m_rcc_err_str);
      throw(ex1);
    }

    busy = true;
    while(busy)
    {
      busy = false;
      for(int chan = 0; chan < m_numberOfDataChannels; chan++)
      {
        //Get event fragments from the Filar
        err_type r_code = FILAR_PagesOut(m_filar_device[chan], &fout);
        if (r_code) 
        {
          DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run, Error from FILAR_PagesOut");
          rcc_error_string(m_rcc_err_str, r_code);
          int slen = strlen(m_rcc_err_str);
          sprintf(m_rcc_err_str + slen - 1, "\nThis error came from ROL with physical address = %d\n", m_filar_device[chan]);
          CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_PAGESOUT,m_rcc_err_str);
          throw(ex1);
        }
        DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: " << fout.nvalid << " fragments received from ROL " << chan);
        if (fout.nvalid)
          busy = true;

        //Re-fill the IN-FIFO of the FILAR
        filar_fill(chan);

        if (fout.nvalid)
        {
          for(u_int loop = 0; loop < fout.nvalid; loop++)
          {
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Processing new fragment " << loop);
            // Check for event truncation
            if (fout.fragstat[loop] & NO_ECW)
            {   
              DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run: ROD fragment from ROL " << chan << " will be truncated");
              char mytext[100];
              sprintf(mytext, " Fragment from ROL with physical address = %d \n", m_filar_device[chan]);
              CREATE_ROS_EXCEPTION(ex1,ModulesException,ALL_TRUNCATE,mytext);
              throw(ex1);
            }

            //Check if the new fragment has at least the minimal size
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Event size = " << fout.fragsize[loop] << " words");
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Event size limit = " << eslim << " words");
            if (fout.fragsize[loop] < eslim)
            {
              DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run: Fragment of " << fout.fragsize[loop] << " words received");
              DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run: This is too little for a ROD fragment");
              char mytext[100];
              sprintf(mytext, " The bad fragment came from ROL with physical address = %d \n", m_filar_device[chan]);
              CREATE_ROS_EXCEPTION(ex1,ModulesException,ALL_TOOSMALL,mytext);
              throw(ex1);
            }

            u_int pci_addr = fout.pciaddr[loop] - sizeof(ROBFragment::ROBHeader);
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   PCI address: 0x" << HEX(pci_addr) << "   Fragment size: 0x" << fout.fragsize[loop] << "   Fragment status: 0x" << fout.fragstat[loop]);

            //Get the event descriptor for the PCI address from the Event Input Manager
            m_fastAllocateMutex->lock();   //Lock event creation
            evDesc_t *ed = m_eventInputManager[chan]->getEventDescriptor(pci_addr);
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   Event descriptor for PCI address 0x" << HEX(pci_addr) << " is at " << ed);

            //Get the pointer to the memory page descriptor
            MemoryPage *mem_page = ed->myPage;
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   Memory page descriptor of this Event descriptor is at " << HEX(mem_page));

            //Get the pointer to the memory page
            u_int *rod_data = (u_int *)((u_long)mem_page->address() + sizeof(ROBFragment::ROBHeader));
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   ROD fragment is at " << HEX(rod_data));

	    //Check L1ID consistency, ECR and wrap round
	    u_int newl1id = rod_data[EventFragment::rod_l1id_offset];
	    if ((m_statistics.rod_L1id_prev[chan] != INIT_L1ID) && (newl1id != (m_statistics.rod_L1id_prev[chan] + 1)))
	    {
	      if (!(newl1id & 0xffffff))
	      {
		m_ecr[chan] = 1;
		m_rod_L1id_ecr[chan] = newl1id;
	        DEBUG_TEXT(DFDB_ROSFM, 10, "FilarReadoutModule::run: While processing new events from ROL " << chan << " event with (24-bit) L1ID = 0 detected");
		DEBUG_TEXT(DFDB_ROSFM, 10, "FilarReadoutModule::run: New L1ID is " << newl1id << ". L1ID of last event was " << m_statistics.rod_L1id_prev[chan]);

		//ECR done on all channels?
		std::vector<u_int>::iterator allecr = max_element(m_ecr.begin(), m_ecr.end());
		if (*allecr == 1)
		{
	          DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Now all channels had an ECR");
		  for (int cn = 0; cn < m_numberOfDataChannels; cn++)
		  {
        	    m_ecr[chan] = 0;
		    m_statistics.rod_L1id_prev[chan] = m_rod_L1id_ecr[chan];
		  }
		}
	      }
	      else
	      {
		m_statistics.stat_l1id[chan]++;
		CREATE_ROS_EXCEPTION(e, ModulesException,ALL_SEQUENCE, "\n Last L1ID = " << m_statistics.rod_L1id_prev[chan] << ", New L1Id = " << newl1id << ",ROL = " << chan);
		ers::warning(e);
	      }
	    }
	    else
	    {
	      if (m_ecr[chan] == 1)
	      {
	        DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Storing L1ID " << newl1id << " in m_rod_L1id_ecr of channel " << chan);
		m_rod_L1id_ecr[chan] = newl1id;
	      }
	      else
   	      {
	        DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Storing L1ID " << newl1id << " in m_statistics.rod_L1id_prev of channel " << chan);
		m_statistics.rod_L1id_prev[chan] = newl1id;
	      }
	    }

            //Check if the number of words received is consistent with the information in the ROD trailer
            //Compute address of ROD trailer
            u_int trailer_index = fout.fragsize[loop] - (sizeof(RODFragment::RODTrailer) >> 2);
            u_int rodfragsize = (sizeof(RODFragment::RODHeader) + (sizeof(RODFragment::RODTrailer) + rod_data[trailer_index] * 4 + rod_data[trailer_index + 1] * 4));
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   trailer_index = " << trailer_index << " Expected fragment size = " << rodfragsize << "   real fragment size = " << 4 * fout.fragsize[loop]);
            if (rodfragsize != (4 * fout.fragsize[loop]))
            {
              DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::run: Size mismatch:  rodfragsize = " << rodfragsize << " fout.fragsize = " << fout.fragsize[loop] << " ROL = " << chan);
              m_statistics.stat_size[chan]++;
	      CREATE_ROS_EXCEPTION(e, ModulesException, ALL_ILLSIZE2,
				     "\n L1ID = " << rod_data[EventFragment::rod_l1id_offset] <<
				     ", received  size = " << 4 * fout.fragsize[loop] <<
				     ", expected size = " << rodfragsize);
	      ers::warning(e);
            } 


            //Initialize the event descriptor
            ed->L1id            = rod_data[EventFragment::rod_l1id_offset];
            ed->RODFragmentSize = fout.fragsize[loop];   //For the ROB header
            u_int specific = (fout.fragstat[loop] & 0xffff) << 16; //MJ: Put error bits into specific field. To be reviewed
            u_int generic  = 0;
            if (specific)
              generic = EventFragment::STATUS_ILLDATA;
            ed->RODFragmentStatus = specific + generic;

            //Insert the event into the Event Input Manager
            m_eventInputManager[chan]->createEvent(ed);    

            //Tell the page how much data it contains
            mem_page->reserve(4 * fout.fragsize[loop] + sizeof(ROBFragment::ROBHeader));
            m_fastAllocateMutex->unlock();

            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   RODFragmentSize   = " << 4 * fout.fragsize[loop] << " bytes");
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   sizeof(ROBHeader) = " << sizeof(ROBFragment::ROBHeader) << " bytes");
            DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run:   " << 4 * fout.fragsize[loop] + sizeof(ROBFragment::ROBHeader) << " bytes reserved in mem_page");
            DEBUG_TEXT(DFDB_ROSFM, 10, "FilarReadoutModule::run:   The event with L1ID " << rod_data[EventFragment::rod_l1id_offset] << " has been created for ROL " << chan);
            DEBUG_TEXT(DFDB_ROSFM, 11, "FilarReadoutModule::run:   Word 1 =  0x" << HEX(rod_data[0]));
            DEBUG_TEXT(DFDB_ROSFM, 11, "FilarReadoutModule::run:   Word 2 =  0x" << HEX(rod_data[1]));
            DEBUG_TEXT(DFDB_ROSFM, 11, "FilarReadoutModule::run:   Word 3 =  0x" << HEX(rod_data[2]));
            DEBUG_TEXT(DFDB_ROSFM, 11, "FilarReadoutModule::run:   Word 4 =  0x" << HEX(rod_data[3]));
            DEBUG_TEXT(DFDB_ROSFM, 11, "FilarReadoutModule::run:   Word 5 =  0x" << HEX(rod_data[4]));

            //Some statistics
            m_statistics.fragments_input[chan]++;

            if (m_triggerQueueFlag)	// only in self-triggering mode
            {
              bool complete = FragmentsAreThere(ed->L1id, chan); 
              DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: complete = " << complete);

              if (complete)  
              {
                if (m_fastTestMode == 0) 	// normal mode
                  m_inputToTriggerQueue->push(ed->L1id);
                else if (m_fastTestMode == 1)	// skip output completely
                {
                  DEBUG_TEXT(DFDB_ROSFM, 10, "FilarReadoutModule::run: remove L1id " << ed->L1id);
                  m_fastAllocateMutex->lock();
                  for(int l_chan = 0; l_chan < m_numberOfDataChannels; l_chan++)	// within one channel loop ..
                    m_eventInputManager[l_chan]->deleteById(ed->L1id);
                  m_fastAllocateMutex->unlock();
                }
                else if (m_fastTestMode >= 1)	// 1% to output
                {
                  if ((ed->L1id % 100) == 0) 
                    m_inputToTriggerQueue->push(ed->L1id);
                  else
                  {
                    DEBUG_TEXT(DFDB_ROSFM, 10, "FilarReadoutModule::run: remove L1id " << ed->L1id);
                    m_fastAllocateMutex->lock();
                    for(int l_chan = 0; l_chan < m_numberOfDataChannels; l_chan++)	// within one channel loop ..
                      m_eventInputManager[l_chan]->deleteById(ed->L1id);
                    m_fastAllocateMutex->unlock();
                  }
                }
              }
            }
	    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: event input OK");
          }     // for (fout)

	  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::run: Mutex unlocked");
        }       // if (fout.nvalid)
      }	  	// for(chan)
    }	  	// while(busy)
    // We have scanned all ROLs but none has data or all pages are used in the memory pool of the
    // respective ROL. There is nothing to do. Therefore: 
    DFThread::yieldOrCancel();  
    m_statistics.yield++;
  }  // while(1)
}


/********************************/
void FilarReadoutModule::cleanup()
/********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::cleanup: Empty method");
}


/*****************************************/
int FilarReadoutModule::filar_fill(int rol)
/*****************************************/
{
  FILAR_in_t fin;
  err_type r_code;
  u_int loop, pci_addr, npages, nmin, nfree;
   
  DEBUG_TEXT(DFDB_ROSFM, 15, "FilarReadoutModule::filar_fill called for rol " << rol);

  //How many pages could be written to the FILAR?
  r_code = FILAR_InFree(m_filar_device[rol], &nfree);   
  if (r_code) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::filar_fill, Error from FILAR_InFree");
    rcc_error_string(m_rcc_err_str, r_code);
    int slen = strlen(m_rcc_err_str);
    sprintf(m_rcc_err_str + slen - 1, "\nThis error came from ROL with physical address = %d\n", m_filar_device[rol]);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_INFFREE,m_rcc_err_str);
    throw(ex1);
  }
  
  m_fastAllocateMutex->lock();  // lock memory allocation
  npages = m_memoryPool[rol]->numberOfFreePagesNonReserved();

  if (nfree > npages)
    nmin = npages;
  else
    nmin = nfree;

  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::filar_fill:  nfree = " << nfree << " npages = " << npages << " nmin = " << nmin);
  
  if (!nmin) 
  {
    m_fastAllocateMutex->unlock();
    return(0);  //Nothing more to do
  }
  
  fin.nvalid = 0;      
  fin.channel = m_filar_device[rol];

  for(loop = 0; loop < nmin; loop++)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::filar_fill: requesting page " << loop);

    pci_addr = m_eventInputManager[rol]->getPCIAddressOfFreePage();  //MJ: no error code available!!
    fin.pciaddr[loop] = pci_addr + sizeof(ROBFragment::ROBHeader);
    
    //The FILAR requires 64-bit aligned PCI addresses
    if (fin.pciaddr[loop] & 0x7)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::filar_fill: The PCI address 0x" << HEX(fin.pciaddr[loop]) << " is not 64 bit aligned");
      CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_ALIGN,"");
      throw(ex1);
    }

    DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::filar_fill:  Writing PCI address 0x" << HEX(fin.pciaddr[loop]) << " to FILAR");
    fin.nvalid++;
  }
  m_fastAllocateMutex->unlock();
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::filar_fill: page allocation OK");

  r_code = FILAR_PagesIn(&fin);
  if (r_code) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "FilarReadoutModule::filar_fill, Error from FILAR_PagesIn");
    rcc_error_string(m_rcc_err_str, r_code);
    int slen = strlen(m_rcc_err_str);
    sprintf(m_rcc_err_str + slen - 1, "\nThis error came from ROL with lphysical address = %d\n", m_filar_device[rol]);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,FIL_PAGESIN,m_rcc_err_str);
    throw(ex1);
  }       
  DEBUG_TEXT(DFDB_ROSFM, 20, "FilarReadoutModule::filar_fill: FILAR_PagesIn OK");
  return(0);
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createFilarReadoutModule();
}
ReadoutModule* createFilarReadoutModule()
{
  return (new FilarReadoutModule());
}




