// $Id$
/****************************************************************/
/*                                                              */
/*  file test_BusbasedRobIn                                     */
/*                                                              */
/*  exerciser for the ROS Modules & Channels Class              */
/*                                                              */
/*  The program allows to:                                      */
/*  . exercise the methods of the ROS Modules & Channels Class	*/
/*  . measure the performance                                   */
/*                                                              */
/***C 2008 - A nickel program worth a dime***********************/


/****************************************************************/
/*                                                              */
/* Oct. 2007: added non-interactive performance measurements,   */
/*            can be used by supplying command line parameters  */
/*            -h parameter outputs a list of options            */
/*                                                              */
/* 2009,2010: iterative mode added, bug that caused wrong rates */
/*            to be reported if < 3 links are used removed.     */
/*                                                              */
/*           Jos Vermeulen, Nikhef                              */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <iostream>
#include <signal.h>
#include <iomanip>
#include <errno.h>
#include <exception>
#include <vector>
#include <string>
#include <sys/types.h>
#include <getopt.h>
#include <stdio.h>
#include <fcntl.h>
#include <is/info.h>
#include <ipc/partition.h>
////#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModules/RobinReadoutModule.h"
#include "ROSModules/RobinDataChannel.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFSubSystemItem/Config.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"


#include "RunControl/FSM/FSMCommands.h"

#ifndef HOST
  #define HOST
#endif
//#include "robin_ppc/robin.h"

using namespace ROS;

enum 
{ 
  TIMING4 = 1,
  TIMING5,
  TIMING6,
  TIMING7,
  TIMING8,
  CREATE,
  CONFIGURE,
  START,
  REQUEST,
  GET,
  GC,
  RELEASEMANY,
  USERCOMMAND,
  STOP,
  UNCONFIGURE,
  DELETE,
  DUMPMANY,
  GETINFO,
  SETCONFIG,
  RESET,  
  SETDEBUG,
  HELP,
  QUIT = 100
};


//Constants
#define FM_POOL_NUMB 5      //Max. number of Robin cards


// Prototypes
int checkData(ROBFragment *fragment, int id, int rol, int mode);
int getnext(float percent, float limit, int nextl1id);
int setdebug(void);
int getparams(DFCountedPointer<Config> configuration, u_int mode);
int getparams2(DFCountedPointer<Config> configuration, u_int robin_nr);
void dumpData(ROBFragment *fragment, int rol);
void sigquit_handler(int signum);
void usage(void);


// Globals
int getstat;
u_int dblevel = 0, dbpackage = DFDB_ROSFM;
u_long crctab16[65536]; // for CRC calculation


// ==================== ConfigInfoObject start ===================
class ConfigInfoObject
{
  public:
    ConfigInfoObject(void);
    int SetParams(DFCountedPointer<Config> configuration);
    void SetLinkMask(int mask);
    void SetRemoteStartCmd(char *cmd);
    void SetRemoteComputer(char *name);
    void SetRemoteKillCmd(char *cmd);
    void SendRemoteCommand(void);
    void SendRemoteKill(void);

    char dolarScriptName[80], dolarComputerName[80], nameOfConfigFile[80], nameOfKillScript[80];
    char remoteStartCmd[200], remoteKillCmd[200];
    bool externalDataGeneratorFlag, oneCycleFlag, nonIntPerformMeasFlag, verboseFlag;
    float setupNeventFraction, initialStepReducFactor, initialValueReducFactor, requestFraction;
    int deleteGroupSize, numberOfLVL1AcceptsPerCycle, linkMask;
    u_int maxRXPages, memoryPoolPageSize, memoryPoolNumPages;
    u_int preScaleFrag, preScaleMsg, preScaleFpf;
    u_int numberOfOutstandingReq;
    int pageSize, numPages, numberOfWordsPerFragment, pciSlotNumber;
    u_int softwareVersion, firmwareVersion, firmwareVersionPCI, firmwareVersionPCIe, check_verbosity;
    bool dataCheckFlag, dataDumpFlag, iterateFlag, clearAllFlag, noClearsFlag, busIsPCIFlag;
    float targetDeleteRate;
};


/**********************************/
ConfigInfoObject::ConfigInfoObject()
/**********************************/
{
  externalDataGeneratorFlag   = false;
  setupNeventFraction         = 0.5;
  deleteGroupSize             = 100;
  numberOfLVL1AcceptsPerCycle = 1000000;
  linkMask                    = 7;
  oneCycleFlag                = false;
  initialStepReducFactor      = 0.02;
  initialValueReducFactor     = 0.9;
  requestFraction             = 100.0;
  pageSize                    = 512;
  numPages                    = 32768;
  maxRXPages                  = 2;
  memoryPoolPageSize          = 16384;
  memoryPoolNumPages          = 10;
  preScaleFrag                = 1;
  preScaleMsg                 = 1;
  preScaleFpf                 = 10;
  numberOfOutstandingReq      = 20;
  verboseFlag                 = false;
  numberOfWordsPerFragment    = 100;
  pciSlotNumber               = 0;
  softwareVersion             = 0xb0001;
  firmwareVersionPCI          = 0x5033001;
  firmwareVersionPCIe         = 0x5033801;
  firmwareVersion             = firmwareVersionPCI;
  check_verbosity             = 2;
  targetDeleteRate			  = 100.0;
  nonIntPerformMeasFlag       = false;
  dataCheckFlag               = false;
  dataDumpFlag                = false;
  iterateFlag                 = false;
  clearAllFlag                = false;
  noClearsFlag                = false;
  busIsPCIFlag                = true;


  strcpy (dolarScriptName,   "dolar");
  strcpy (dolarComputerName, "osiris");
  strcpy (nameOfKillScript,  "killdolar");
  strcpy (nameOfConfigFile,  "testRobinConfig.txt");

  remoteStartCmd[0] = 0;
  strcat(remoteStartCmd, "ssh ");
  strcat(remoteStartCmd, dolarComputerName);
  strcat(remoteStartCmd, " -x ");
  strcat(remoteStartCmd, dolarScriptName);
  strcat(remoteStartCmd, " ");

  remoteKillCmd[0] = 0;
  strcat(remoteKillCmd, "ssh ");
  strcat(remoteKillCmd, dolarComputerName);
  strcat(remoteKillCmd, " -x ");
  strcat(remoteKillCmd, nameOfKillScript);
}


/*************************************************/
void ConfigInfoObject::SetRemoteStartCmd(char *cmd)
/*************************************************/
{
  if (strlen(cmd) > 80)
  {
    printf("Remote start command too long\n");
    exit(0);
  }

  remoteStartCmd[0] = 0;
  strcat(remoteStartCmd, "ssh ");
  strcat(remoteStartCmd, dolarComputerName);
  strcat(remoteStartCmd, " -x ");
  strcat(remoteStartCmd, cmd);
  strcat(remoteStartCmd, " ");
}


/**************************************************/
void ConfigInfoObject::SetRemoteComputer(char *name)
/**************************************************/
{
  if (strlen(name) > 80)
  {
    printf("Remote computer name too long\n");
    exit(0);
  }

  dolarComputerName[0] = 0;
  strcat(dolarComputerName, name);
  SetRemoteStartCmd(dolarScriptName);
  SetRemoteKillCmd(nameOfKillScript);
}


/************************************************/
void ConfigInfoObject::SetRemoteKillCmd(char *cmd)
/************************************************/
{
  if (strlen(cmd) > 80)
  {
    printf("Remote kill command too long\n");
    exit(0);
  }

  remoteKillCmd[0] = 0;
  strcat(remoteKillCmd, "ssh ");
  strcat(remoteKillCmd, dolarComputerName);
  strcat(remoteKillCmd, " -x ");
  strcat(remoteKillCmd, cmd);
}


/********************************************/
void ConfigInfoObject::SendRemoteCommand(void)
/********************************************/
{
  char nWords[20];

  sprintf(nWords, "%d", numberOfWordsPerFragment);
  strcat(remoteStartCmd, nWords);
  if (verboseFlag == true)
    printf("Remote command: %s\n", remoteStartCmd);
  else
    strcat(remoteStartCmd, " > /dev/null");
  system(remoteStartCmd);
}


/*****************************************/
void ConfigInfoObject::SendRemoteKill(void)
/*****************************************/
{
  if (verboseFlag == true)
    printf("Remote kill command: %s\n", remoteKillCmd);
  else
    strcat(remoteKillCmd, " > /dev/null");
  system(remoteKillCmd);
}



/*********************************************************************/
int ConfigInfoObject::SetParams(DFCountedPointer<Config> configuration)
/*********************************************************************/
{
  // derived from getparams
  
  // Definitions:
  // PhysicalAddress        = The PCI slot number of the Robin card
  // timeout                = The message time-out in seconds
  // numberOfChannels       = The number of ROLs on one Robin card
  // memoryPoolNumPages     = The number of memory pages for ROB fragments
  // memoryPoolPageSize     = The size of a memory page for ROB fragments
  // numberOfOutstandingReq = The number of entries to be reserved in the message FIFO of a Robin card
  // msgInputMemorySize     = The amount of message DPM to be reserved in one Robin card
  // miscSize               = The amount of PC memory to be reserved for misc. messages
  // ChannelId              = The logical ID of a ROL
  // ROLPhysicalAddress     = The physical number of a ROL on a Robin card
  // cfgParms[l_index].name = The Robin configuration parameters
 
  u_int noChannels;
  int activeChannels[3];

  configuration->set("PhysicalAddress", pciSlotNumber); 
  configuration->set("EbistEnabled", 0); 
  configuration->set("RevisionNumber", softwareVersion); 
  configuration->set("FPGAVersion", firmwareVersion);
  configuration->set("Timeout", 100);
  configuration->set("ResetRobin", 0);
  activeChannels[0] = linkMask & 1;
  activeChannels[1] = (linkMask & 2) / 2;
  activeChannels[2] = (linkMask & 4) / 4;
  noChannels = (linkMask & 1) + ((linkMask & 2) >> 1) + ((linkMask & 4) >> 2);
  configuration->set("numberOfChannels", noChannels);
  configuration->set("NumberOfOutstandingReq", numberOfOutstandingReq);

  /*
    Some of the benchmarking functions do not run if there is insufficient DPM memory
    These functions are designed such that they generate a certain number of outstanding 
    requests for fragments. These messages block parts of the available DPM space. At one moment 
    the S/W will try to release a group of events. The message with the L1IDs is large 
    (~400 bytes) and the release function is blocking (i.e. it will not return if it cannot send 
    the message due to a lack of DPM memory). If the release function fails to get 
    DPM memory because one of the outstanding fragment requests is in the way the release function 
    cannot do its job and blocks the whole process.   
  */

  configuration->set("MsgInputMemorySize", 0x2000);
  configuration->set("MiscSize", 0x2000);
  configuration->set("Interactive", 1);
  configuration->set("Prescalefrag", preScaleFrag);
  configuration->set("Prescalemsg", preScaleMsg);
  configuration->set("Prescalefpf", preScaleFpf);
  configuration->set("SubDetectorId", 0x51);
  configuration->set("Pagesize", pageSize);
  configuration->set("Numpages", numPages);
  configuration->set("MaxRxPages", maxRXPages);
  configuration->set("triggerQueue", 0);

  for (u_int loop = 0; loop < noChannels; loop++)
  {
    configuration->set("ChannelId", loop, loop);
    configuration->set("ROLPhysicalAddress", loop, loop);
    configuration->set("memoryPoolNumPages", memoryPoolNumPages, loop);
    configuration->set("memoryPoolPageSize", memoryPoolPageSize, loop);

    if ((pageSize * 4 * maxRXPages) > memoryPoolPageSize)
    {
      std::cout << "With the parameters you have selected the ROBIN can send fragments of up to " << pageSize * 4 * maxRXPages << " bytes" << std::endl;
      std::cout << "But the local buffer manager can only handle fragments of at most " << memoryPoolPageSize << " bytes" << std::endl;
      std::cout << "Relevant parameters: pageSize ROBIN = " << pageSize << " words, maxRXPages = " << maxRXPages << std::endl;
      std::cout << "Therefore maximum fragment size = " << pageSize * maxRXPages << " words = " << pageSize * maxRXPages * 4 << " bytes" << std::endl;
      std::cout << "This should fit into one page of the memory pool in the ROS (memoryPoolPageSize) of " << memoryPoolPageSize << " bytes" << std::endl;
      exit(0);
    }

    if (externalDataGeneratorFlag == false) 
      configuration->set("RolDataGen", 1, loop);
    else 
      configuration->set("RolDataGen", 0, loop);
      
    configuration->set("UID", "dummy", loop);
    configuration->set("Rolemu", 0, loop);
    configuration->set("TestSize", numberOfWordsPerFragment, loop);
    configuration->set("Keepfrags", 0, loop);  
    configuration->set("ForceL1ID", 0, loop);
    configuration->set("CrcCheckInterval", 100, loop);
  }

  return(noChannels);
}


/******************************************/
void ConfigInfoObject::SetLinkMask(int mask)
/******************************************/
{
  if ((mask >= 0) && (mask < 8))
    linkMask = mask;
  else
  {
    printf("invalid mask %d\n", mask);
    exit(0);
  }
  if ( (linkMask & 1) == 0)
  {
      printf("link 0 should always be enabled, but is not enabled for the linkmask (%d) used\n", linkMask);
      exit(0);
  }
}

ConfigInfoObject gConfInfo;

// ==================== ConfigInfoObject end ===================


/**********************************************************/
void DoPerformanceMeasurement(ReadoutModule** ReadoutModule)
/**********************************************************/
{
  std::vector<DataChannel *> *channels;
  std::vector <u_int> idgroup, idgroup2, idgroup3;
  const u_int maxNumberOfTickets = 4;
  u_int nevents[3], nevents2[3], nextticket[3], nextl1id[3], tickets[3][maxNumberOfTickets][2];
  u_int isempty, loop, looprol, rol, datal[3], datay[3], datan[3][maxNumberOfTickets], datar[3][maxNumberOfTickets];
  u_int lastReceivedLevel1Id[3] = {1, 1, 1};
  u_int lastClearedLevel1Id = 0, lastClearedLevel1Id2 = 0, lastClearedLevel1Id3 = 0, nrols = 3, nrolsUsed;
  u_int numberOfEvents, numberOfEventsFirst, numberOfEventsLeft, maxNumberOfReasks1, maxNumberOfReasks2;
  int numberOfClearsQueued = 0, numberOfClearsQueued2 = 0, numberOfClearsQueued3 = 0;
  int rolCounter[3] = {0, 0, 0}, requeueCounter[3], deletesPerGroup;
  ROBFragment *fragment;
  tstamp ts0, ts1, ts2, tStartDelay[3][maxNumberOfTickets], tStopDelay, tLastStartDelay[3], tsRef;
  double stepInLvl1Ids, percent = 10.0, delta1, delta2, nextLvl1IdNotRounded[3];
  double reducFactorStep = 0.01, reducFactor = 0.0, timeWaited, minWaitInterval, waitingTime[3][maxNumberOfTickets];
  double startupModFactor;
  double theRate = 0.0, theL1Rate = 0.0;
  double maxRatex = 0.0, maxRatey = 0.0, maxRatex1 = 0.0, maxRatey1 = 0.0;
  const double initialWaitingTime = 5000000.0, waitIncrement = 1.0;
  double waitingTimeIncrement = 0.0;
  bool firstCycle = true, reAskFlag, stopFlag = false;
  float stepSize = 1.0;
  int downCount = 0;
  int upCount = 0;
  bool increaseReducFactorFlag = true;
  bool firstFlag = true;
  float bw1, bw2;
  int fragSize, receivedFragSize;
  RobinDataChannel *robinChannel0;


  deletesPerGroup = gConfInfo.deleteGroupSize;
  percent = gConfInfo.requestFraction;
  nrolsUsed = (gConfInfo.linkMask & 1) + ((gConfInfo.linkMask & 2) >> 1) + ((gConfInfo.linkMask & 4) >> 2);
  std::cout << "Number of ROLs used = " << nrolsUsed << " link mask = " << gConfInfo.linkMask << std::endl;
  if (gConfInfo.noClearsFlag)
  {
      std::cout << "No clears sent!" << std::endl;
      gConfInfo.iterateFlag = false;
  }
  else
  {
      if (gConfInfo.clearAllFlag)
      {
          std::cout << "Combined clears" << std::endl;
      }
  }
  // determine minimum waitingTime assuming 5 us minimum per trigger, with a threshold 
  for (u_int i = 0; i < maxNumberOfTickets; i++)
  {
    datal[i] = 0;
    datay[i] = 0;
    for (looprol = 0; looprol < nrols; looprol++)
    {
      datan[looprol][i] = 0;
      datar[looprol][i] = 0;
      waitingTime[looprol][i] = 0.0;
    }
  }

  numberOfEvents = gConfInfo.numberOfLVL1AcceptsPerCycle;
  numberOfEventsFirst = (u_int) (gConfInfo.setupNeventFraction * (float) numberOfEvents);
  numberOfEventsLeft = numberOfEvents - numberOfEventsFirst;
  nevents2[0] = numberOfEventsFirst * (gConfInfo.linkMask & 1);
  nevents2[1] = numberOfEventsFirst * ((gConfInfo.linkMask & 2) / 2);
  nevents2[2] = numberOfEventsFirst * ((gConfInfo.linkMask & 4) / 4);
  nevents[0]  = (int) ( (float) nevents2[0] * percent / 100.0);     
  nevents[1]  = (int) ( (float) nevents2[1] * percent / 100.0);
  nevents[2]  = (int) ( (float) nevents2[2] * percent / 100.0);
  if (!gConfInfo.noClearsFlag)
  {
      stepInLvl1Ids = 100.0 / percent;
  }
  else
  {
      stepInLvl1Ids = 0.0;
  }
  nextLvl1IdNotRounded[0] = 1.0;
  nextLvl1IdNotRounded[1] = 1.0;
  nextLvl1IdNotRounded[2] = 1.0;

  nextl1id[0] = 1;
  nextl1id[1] = 1;
  nextl1id[2] = 1;
	
  channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[0]->channels());
  robinChannel0 = (RobinDataChannel *) (*channels)[0];

  timeWaited = 0.0;
  ts_clock(&tsRef);
  while (timeWaited < initialWaitingTime)
  {
    ts_clock(&tStopDelay);
    timeWaited = 1000000.0 * ts_duration(tsRef, tStopDelay);
  }

  idgroup.clear();
  idgroup2.clear();
  idgroup3.clear();

  //Fill the message queue
  for (looprol = 0; looprol < nrols; looprol++)
  {
    if (nevents2[looprol] > 0)
    {
      for (loop = 0; loop < maxNumberOfTickets; loop++)
      {
			tickets[looprol][loop][0] = (*channels)[looprol]->requestFragment(nextl1id[looprol]);
			tickets[looprol][loop][1] = nextl1id[looprol];

			nextLvl1IdNotRounded[looprol] += stepInLvl1Ids;
			nextl1id[looprol] = (int) nextLvl1IdNotRounded[looprol];
      }
    }
    nextticket[looprol] = 0;
  }
  rol = 0;

  reAskFlag = false;
  requeueCounter[0] = 0;
  requeueCounter[1] = 0;
  requeueCounter[2] = 0;
  	
	// start loop over request fractions
	while ( (fabs(theRate -  gConfInfo.targetDeleteRate) > 0.1) && (stepSize > 0.05) )
	{
		maxRatex = 0.0;
		maxRatey = 0.0;
		
	  ts_clock(&ts0);
		// here is the first starting point for the measurement of the length of the time interval 
		// between this request and the next request
	  for (looprol = 0; looprol < nrols; looprol++)
	  {
		  for (loop = 0; loop < maxNumberOfTickets; loop++)
			{
				ts_clock(&tStartDelay[looprol][loop]);
			}
		}
      if (!gConfInfo.noClearsFlag)
      {
          stepInLvl1Ids = 100.0 / percent;
      }
      else
      {
          stepInLvl1Ids = 0.0;
      }

	  nevents2[0] = numberOfEventsFirst * (gConfInfo.linkMask & 1);
	  nevents2[1] = numberOfEventsFirst * ((gConfInfo.linkMask & 2) / 2);
	  nevents2[2] = numberOfEventsFirst * ((gConfInfo.linkMask & 4) / 4);
	  nevents[0]  = (int) ( (float) nevents2[0] * percent / 100.0);     
	  nevents[1]  = (int) ( (float) nevents2[1] * percent / 100.0);
	  nevents[2]  = (int) ( (float) nevents2[2] * percent / 100.0);
	  do 
	  {
		 while((nevents[0] != 0) || (nevents[1] != 0) || (nevents[2] != 0))
		 {
			rolCounter[rol]++;
			isempty = 1;       
			while(isempty)  //Wait for a complete ROB fragment
			{
			  if (reAskFlag == false)
			  {
		  if (nevents[0] < nevents[1])
					if (nevents[1] < nevents[2]) 
				rol = 2; 
			 else 
				rol = 1;
		  else if (nevents[0] < nevents[2]) 
			 rol = 2; 
		  else 
			 rol = 0;
		}
		else
		{
		  switch (gConfInfo.linkMask)
		  {
			 case 1: rol = 0; break;
			 case 2: rol = 1; break;
			 case 3: if (requeueCounter[0] > requeueCounter[1]) 
						  rol = 1; 
						else 
					rol = 0; break;
			 case 4: rol = 2; break;
			 case 5: if (requeueCounter[0] > requeueCounter[2]) 
						  rol = 2; 
				 else 
					rol = 0; break;
			 case 6: if (requeueCounter[1] > requeueCounter[2]) 
						  rol = 2; 
				 else 
					rol = 1; break;
			 case 7:
				 if (requeueCounter[0] > requeueCounter[1])
						  if (requeueCounter[1] > requeueCounter[2]) 
				rol = 2; 
					else 
				rol = 1;
				 else if (requeueCounter[2] < requeueCounter[0]) 
					rol = 2; 
				 else rol = 0;
				 break;
			 default:
				 break;
		  }
		}

		  fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));
		  if (fragment) 
          {
              receivedFragSize = (u_int)fragment->size();       //Total fragment size
              if (gConfInfo.dataCheckFlag)
                  checkData(fragment, tickets[rol][nextticket[rol]][1], rol, gConfInfo.check_verbosity);
              if (gConfInfo.dataDumpFlag)
                  dumpData(fragment, rol);


              if((fragment->status() & 0xf0000000) == 0)
              {
                  // good fragment (with data)
                  datay[rol]++;
                  lastReceivedLevel1Id[rol] = fragment->level1Id();
                  delete fragment;
                  if (nevents[rol])
                      nevents[rol]--;

                  if (!gConfInfo.noClearsFlag)
                  {
                      if (!gConfInfo.clearAllFlag)
                      {
                          if (rol == 0)
                          {
                              while ((lastClearedLevel1Id < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued < deletesPerGroup))
                              {
                                  lastClearedLevel1Id++;
                                  idgroup.push_back(lastClearedLevel1Id);
                                  numberOfClearsQueued++;
                                  if (numberOfClearsQueued == deletesPerGroup)
                                  {
                                      (*channels)[0]->releaseFragment(&idgroup);
                                      idgroup.clear();
                                      numberOfClearsQueued = 0;
                                  }
                              }
                          }
                          if (rol == 1)
                          {
                              while ((lastClearedLevel1Id2 < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued2 < deletesPerGroup))
                              {
                                  lastClearedLevel1Id2++;
                                  idgroup2.push_back(lastClearedLevel1Id2);
                                  numberOfClearsQueued2++;
                                  if (numberOfClearsQueued2 == deletesPerGroup)
                                  {
                                      (*channels)[1]->releaseFragment(&idgroup2);
                                      idgroup2.clear();
                                      numberOfClearsQueued2 = 0;
                                  }
                              }
                          }
                          if (rol == 2)
                          {
                              while ((lastClearedLevel1Id3 < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued3 < deletesPerGroup))
                              {
                                  lastClearedLevel1Id3++;
                                  idgroup3.push_back(lastClearedLevel1Id3);
                                  numberOfClearsQueued3++;
                                  if (numberOfClearsQueued3 == deletesPerGroup)
                                  {
                                      (*channels)[2]->releaseFragment(&idgroup3);
                                      idgroup3.clear();
                                      numberOfClearsQueued3 = 0;
                                  }
                              }
                          }
                      }
                      else
                      {
                          if (rol == 0)
                          {
                              if (rol == 0)
                              {
                                  bool continueFlag = true;


                                  if (gConfInfo.linkMask&1)
                                  {
                                      continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[0]);
                                  }
                                  if (gConfInfo.linkMask&2)
                                  {
                                      continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[1]);
                                  }
                                  if (gConfInfo.linkMask&4)
                                  {
                                      continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[2]);
                                  }


                                  while ( continueFlag && (numberOfClearsQueued < deletesPerGroup))
                                  {
                                      lastClearedLevel1Id++;
                                      idgroup.push_back(lastClearedLevel1Id);
                                      numberOfClearsQueued++;
                                      if (numberOfClearsQueued == deletesPerGroup)
                                      {
                                          robinChannel0->releaseFragmentAll(&idgroup);
                                          idgroup.clear();
                                          numberOfClearsQueued = 0;
                                      }
                                      if (gConfInfo.linkMask&1)
                                      {
                                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[0]);
                                      }
                                      if (gConfInfo.linkMask&2)
                                      {
                                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[1]);
                                      }
                                      if (gConfInfo.linkMask&4)
                                      {
                                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[2]);
                                      }
                                  }
                              }
                          }
                      }
                  }
                  isempty = 0;
              }
              else
              {
                  // fragment was not OK (no data)
                  datal[rol]++;
                  delete fragment;
                  // reqeue request: this is necessary, as the original request was not succesful, probably due to data not yet available, so we try again
                  tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
                  // increase delay if necessary
                  waitingTime[rol][nextticket[rol]] += waitingTimeIncrement;
                  datan[rol][nextticket[rol]]++;

                  nextticket[rol]++;
                  if (nextticket[rol] == maxNumberOfTickets)
                      nextticket[rol] = 0;

                  reAskFlag = true;
                  requeueCounter[rol]++;
              }
          }
      }

			int reql1id = nextl1id[rol];
			int n = nextticket[rol];
			// Wait if necessary, then queue a new L1ID
			ts_clock(&tStopDelay);
			timeWaited = 1000000.0 * ts_duration(tStartDelay[rol][n], tStopDelay);
			while (timeWaited < waitingTime[rol][n])
			{
                ts_clock(&tStopDelay);
                timeWaited = 1000000.0 * ts_duration(tStartDelay[rol][n], tStopDelay);
			}
			tickets[rol][n][0] = (*channels)[rol]->requestFragment(reql1id);
			tickets[rol][n][1] = reql1id;

			// here is the starting point for the  measurement of the length of the time interval between this request and the next request
			ts_clock(&tStartDelay[rol][n]);
			tLastStartDelay[rol] = tStartDelay[rol][n];

            nextLvl1IdNotRounded[rol] += stepInLvl1Ids;
			nextl1id[rol] = (int) nextLvl1IdNotRounded[rol];
			nextticket[rol]++;
			if (nextticket[rol] == maxNumberOfTickets)
                nextticket[rol] = 0;
			reAskFlag = false;
        }

		 nevents2[0] = numberOfEventsLeft * (gConfInfo.linkMask & 1);
		 nevents2[1] = numberOfEventsLeft * ((gConfInfo.linkMask & 2) / 2);
		 nevents2[2] = numberOfEventsLeft * ((gConfInfo.linkMask & 4) / 4);
		 nevents[0]  = (int) ((float) nevents2[0] * percent / 100.0);     
		 nevents[1]  = (int) ((float) nevents2[1] * percent / 100.0);
		 nevents[2]  = (int) ((float) nevents2[2] * percent / 100.0);

		 // determine overall minimum time interval between successive requests
		 {
			minWaitInterval = 0.0;
			int nActiveRols = 0;
			for (u_int i = 0; i < nrols; i++)
			{
                if (nevents2[i] > 0)
                {
                    for (u_int j = 0; j < maxNumberOfTickets; j++)
                    {
                        minWaitInterval += waitingTime[i][j];
                    }
                    nActiveRols++;
                }
			}
            minWaitInterval = reducFactor * minWaitInterval / ((float) (maxNumberOfTickets * nActiveRols));
            // minWaitInterval = minWaitInterval / ((float) (maxNumberOfTickets * nActiveRols));
            minWaitInterval = minWaitInterval / ((float) (maxNumberOfTickets));
         }
		 reAskFlag = false;
		 requeueCounter[0] = 0;
		 requeueCounter[1] = 0;
		 requeueCounter[2] = 0;

		 ts_clock(&ts1);

		 while((nevents[0] != 0) || (nevents[1] != 0) || (nevents[2] != 0))
		 {
			isempty = 1;       
			while(isempty)  //Wait for a complete ROB fragment
			{
			  if (reAskFlag == false)
			  {
		  if (nevents[0] < nevents[1])
			 if (nevents[1] < nevents[2]) 
				rol = 2; 
			 else 
				rol = 1;
		  else if (nevents[0] < nevents[2]) 
			 rol = 2; 
		  else 
			 rol = 0;
			  }
		else
		{
		  switch (gConfInfo.linkMask)
		  {
			 case 1: rol = 0; break;
			 case 2: rol = 1; break;
			 case 3: if (requeueCounter[0] > requeueCounter[1]) 
					rol = 1; 
				 else 
					rol = 0; 
				 break;
			 case 4: rol = 2; break;
			 case 5: if (requeueCounter[0] > requeueCounter[2]) 
					rol = 2; 
				 else 
					rol = 0; 
				 break;
			 case 6: if (requeueCounter[1] > requeueCounter[2]) 
						  rol = 2; 
				 else 
					rol = 1; 
				 break;
			 case 7:
				 if (requeueCounter[0] > requeueCounter[1])
					if (requeueCounter[1] > requeueCounter[2]) 
					  rol = 2; 
					else 
					  rol = 1;
				 else
					if (requeueCounter[2] < requeueCounter[0]) 
					  rol = 2; 
					else 
					  rol = 0;
				 break;
			 default:
				 break;
		  }
		}

		fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));
		if (fragment) 
		{ // good fragment (with data)
		  if (gConfInfo.dataCheckFlag)
					checkData(fragment, tickets[rol][nextticket[rol]][1], rol, gConfInfo.check_verbosity);
		  if (gConfInfo.dataDumpFlag)
					dumpData(fragment, rol);

          if((fragment->status() & 0xf0000000) == 0)
          {
             datay[rol]++;
             lastReceivedLevel1Id[rol] = fragment->level1Id();
             delete fragment;
             if (nevents[rol])
               nevents[rol]--;

          if (!gConfInfo.noClearsFlag)
          {
              if (!gConfInfo.clearAllFlag)
          {
              if (rol == 0)
              {
                  while ((lastClearedLevel1Id < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued < deletesPerGroup))
                  {
                      lastClearedLevel1Id++;
                      idgroup.push_back(lastClearedLevel1Id);
                      numberOfClearsQueued++;
                      if (numberOfClearsQueued == deletesPerGroup)
                      {
                          (*channels)[0]->releaseFragment(&idgroup);
                          idgroup.clear();
                          numberOfClearsQueued = 0;
                      }
                  }
              }
              if (rol == 1)
              {
                  while ((lastClearedLevel1Id2 < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued2 < deletesPerGroup))
                  {
                      lastClearedLevel1Id2++;
                      idgroup2.push_back(lastClearedLevel1Id2);
                      numberOfClearsQueued2++;
                      if (numberOfClearsQueued2 == deletesPerGroup)
                      {
                          (*channels)[1]->releaseFragment(&idgroup2);
                          idgroup2.clear();
                          numberOfClearsQueued2 = 0;
                      }
                  }
              }
              if (rol == 2)
              {
                  while ((lastClearedLevel1Id3 < lastReceivedLevel1Id[rol]) && (numberOfClearsQueued3 < deletesPerGroup))
                  {
                      lastClearedLevel1Id3++;
                      idgroup3.push_back(lastClearedLevel1Id3);
                      numberOfClearsQueued3++;
                      if (numberOfClearsQueued3 == deletesPerGroup)
                      {
                          (*channels)[2]->releaseFragment(&idgroup3);
                          idgroup3.clear();
                          numberOfClearsQueued3 = 0;
                      }
                  }
              }
          }
              else
              {
                  if (rol == 0)
                  {
                      bool continueFlag = true;


                      if (gConfInfo.linkMask&1)
                      {
                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[0]);
                      }
                      if (gConfInfo.linkMask&2)
                      {
                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[1]);
                      }
                      if (gConfInfo.linkMask&4)
                      {
                          continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[2]);
                      }


                      while ( continueFlag && (numberOfClearsQueued < deletesPerGroup))
                      {
                          lastClearedLevel1Id++;
                          idgroup.push_back(lastClearedLevel1Id);
                          numberOfClearsQueued++;
                          if (numberOfClearsQueued == deletesPerGroup)
                          {
                              robinChannel0->releaseFragmentAll(&idgroup);
                              idgroup.clear();
                              numberOfClearsQueued = 0;
                          }
                          if (gConfInfo.linkMask&1)
                          {
                              continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[0]);
                          }
                          if (gConfInfo.linkMask&2)
                          {
                              continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[1]);
                          }
                          if (gConfInfo.linkMask&4)
                          {
                              continueFlag = continueFlag && (lastClearedLevel1Id < lastReceivedLevel1Id[2]);
                          }
                      }
                  }
              }
          }
          isempty = 0;
		}
		else 
		{
           delete fragment;
          // requeue request: this is necessary, as the original request was not succesful, probably due to data not yet available, so we try again
		  tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
		  datar[rol][nextticket[rol]]++;
		  nextticket[rol]++;
		  if (nextticket[rol] == maxNumberOfTickets)
			 nextticket[rol] = 0;
		  requeueCounter[rol]++;
		  reAskFlag = true;
			  }
            }
    }

			int reql1id = nextl1id[rol];
			int n = nextticket[rol];
			// Wait if necessary, then queue a new L1ID
			ts_clock(&tStopDelay);
			timeWaited = 1000000.0 * ts_duration(tLastStartDelay[rol], tStopDelay); 
			while (timeWaited < minWaitInterval)
			{
                ts_clock(&tStopDelay);
                timeWaited = 1000000.0 * ts_duration(tLastStartDelay[rol], tStopDelay);
			}
			tickets[rol][n][0] = (*channels)[rol]->requestFragment(reql1id);
			tickets[rol][n][1] = reql1id;
			// here is the starting point for the  measurement of the length of the time interval between this request and the next request
			ts_clock(&tLastStartDelay[rol]);
			nextLvl1IdNotRounded[rol] += stepInLvl1Ids;
			nextl1id[rol] = (int) nextLvl1IdNotRounded[rol];
			// nextl1id[rol]++;
			nextticket[rol]++;
			if (nextticket[rol] == maxNumberOfTickets)
                nextticket[rol] = 0;

			reAskFlag = false;
		 }

		 ts_clock(&ts2);
		 delta1 = 1000000.0 * ts_duration(ts1, ts2);

         maxNumberOfReasks1 = 0;
         maxNumberOfReasks2 = 0;
		 for (looprol = 0; looprol < nrols; looprol++)
		 {
			u_int n;

			if (nevents2[looprol] > 0)
			{
		for (n=0;n<maxNumberOfTickets;n++)
		{
          if (datan[looprol][n] > maxNumberOfReasks1)
             maxNumberOfReasks1 = datan[looprol][n];

          if (datar[looprol][n] > maxNumberOfReasks2)
             maxNumberOfReasks2 = datar[looprol][n];
		}
		if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
		{
		  std::cout << std::endl;
		  std::cout << "ROL " << looprol << " : Number of getFragment with data                   = " << datay[looprol] << std::endl;
		  std::cout << "ROL " << looprol << " : Number of lost events                             = " << datal[looprol] << std::endl;
		  std::cout << "ROL " << looprol << " : Number of getFragment without data during startup = ";
		  for (n = 0; n < maxNumberOfTickets; n++)
			 std::cout << datan[looprol][n] << " ";

		  std::cout << std::endl;
		  std::cout << "ROL " << looprol << " : Number of getFragment without data during running = ";
		  for (n = 0; n < maxNumberOfTickets; n++)
			 std::cout << datar[looprol][n] << " ";

		  std::cout << std::endl;
		  std::cout << "ROL " << looprol << " : Minimum time interval between successive requests for same ticket = ";
		  for (n = 0; n < maxNumberOfTickets; n++)
			 std::cout << waitingTime[looprol][n] << " ";

		  std::cout << " us" << std::endl << std::endl;
		}
			}
		 }

		 if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
		 {
            std::cout << "Minimum time interval between successive requests = " << minWaitInterval / ( (float) nrolsUsed)  << " us" << std::endl;
			std::cout << "Minimum time per event = " << percent * minWaitInterval / 100.0 << " us" << std::endl << std::endl;
			std::cout << "Measuring time     =  " << delta1 << " us" << std::endl;
			std::cout << "Number of events   = " << (nevents2[0] | nevents2[1] | nevents2[2]) << std::endl;
			if ((gConfInfo.linkMask&1) == 1)
			  printf("rol 0 lastClearedLevel1Id %d lastReceivedLevel1Id %d numberOfClearsQueued %d\n", lastClearedLevel1Id, lastReceivedLevel1Id[0], numberOfClearsQueued);
            if ( ((gConfInfo.linkMask&2) == 2) && (!gConfInfo.clearAllFlag) )
			  printf("rol 1 lastClearedLevel1Id %d lastReceivedLevel1Id %d numberOfClearsQueued %d\n", lastClearedLevel1Id2, lastReceivedLevel1Id[1], numberOfClearsQueued2);
            if ( ((gConfInfo.linkMask&4) == 4) && (!gConfInfo.clearAllFlag) )
			  printf("rol 2 lastClearedLevel1Id %d lastReceivedLevel1Id %d numberOfClearsQueued %d\n", lastClearedLevel1Id3, lastReceivedLevel1Id[1], numberOfClearsQueued3);
		 }
		 delta2 = delta1 / ( (float) (nevents2[0] | nevents2[1] | nevents2[2]) );
         theRate = 1000.0/delta2;
         if (!gConfInfo.noClearsFlag)
         {
             theL1Rate = theRate;
         }
         else
         {
             theL1Rate = 0.0;
         }



		 if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
		 {
			printf ("Time per event       = %f us\n\n", delta2);
            printf ("LVL1 rate            = %f kHz\n", theL1Rate);
            printf ("request rate         = %f kHz\n", ((float) nrolsUsed) * percent *10.0/delta2);
			printf ("request fraction     = %f%c\n", percent, '%');
			printf ("fragment size        = %d words\n",  gConfInfo.numberOfWordsPerFragment);
			printf ("min. time per event / measured time = %f\n", (percent * minWaitInterval) / (100.0 * delta2));
            printf ("max # of reasks 1    = %d\n", maxNumberOfReasks1);
			printf ("# of requests 1      = %d\n", (int) (percent * (float) numberOfEventsFirst /100.0));
            printf ("max # of reasks 2    = %d\n", maxNumberOfReasks2);
			printf ("# of requests 2      = %d\n", (int) (percent * (float) nevents2[0] / 100.0));
			printf ("reducFactor          = %f\n", reducFactor);
			printf ("min. time per event  = %f us\n", percent * minWaitInterval / 100.0);
			printf ("size of delete block = %d\n", gConfInfo.deleteGroupSize);
			printf ("link mask            = %d\n", gConfInfo.linkMask);
			std::cout << std::endl << "Compact representation of results (for use in Excel):" << std::endl;
			printf ("LVL1 rate (kHz), request rate (kHz), request fraction (%c), fragment size (words),\n", '%');
			printf ("min. time per event / measured time ,max # of reasks 1, # of requests 1,\n");
			printf ("max # of reasks 2, # of requests 2, reducFactor, min. time per event\n");
			printf ("size of delete block, link mask\n");
		 }
		 if (gConfInfo.nonIntPerformMeasFlag)
		 {
            if (maxNumberOfReasks2 == 0)
             {
                printf ("noreasks   ");
                if (theRate > maxRatex)
                {
                    maxRatex = theRate;
                    maxRatex1 = theL1Rate;
                }
			}
			else
			{
                printf ("withreasks ");
                if (theRate > maxRatey)
                {
                    maxRatey = theRate;
                    maxRatey1 = theL1Rate;
                }
			}


            printf("%7.3f %7.3f %5.3f %d %5.3f %7d %d %7d %d %6.4f %5.3f %d %d\n", theL1Rate, ((float) nrolsUsed) * percent *10.0/delta2, percent, gConfInfo.numberOfWordsPerFragment,
                (percent * minWaitInterval) / (100.0 * delta2), maxNumberOfReasks1, (int) (percent * (float) numberOfEventsFirst /100.0),
                 maxNumberOfReasks2, (int) (percent * (float) nevents2[0] / 100.0), reducFactor, percent * minWaitInterval / 100.0, gConfInfo.deleteGroupSize, gConfInfo.linkMask);

			delta1 = 1000000.0 * ts_duration(ts0, ts1);
			delta2 = delta1 / ( (float) numberOfEventsFirst);
			if (gConfInfo.verboseFlag == true)
			{
		std::cout << std::endl << "Startup time     =  " << delta1 << " us" << std::endl;
		std::cout << "Number of events = " << numberOfEventsFirst << std::endl;
		std::cout << "Time per event   =  " << delta2 << " us" << std::endl;
			}
		}

		 if (firstCycle == true)
		 {
             // now we start with minimum time intervals
             reducFactor = gConfInfo.initialValueReducFactor;
             reducFactorStep = gConfInfo.initialStepReducFactor;
             for (u_int i = 0; i < maxNumberOfTickets; i++)
             {
                 // waiting time is maxNumberOfTickets * 5 us
                 waitingTime[0][i] = 100.0 * ((float) maxNumberOfTickets * 5.0) / percent;
                 waitingTime[1][i] = waitingTime[0][i];
                 waitingTime[2][i] = waitingTime[0][i];
                 waitingTimeIncrement = waitIncrement;
             }
             firstCycle = false;
             if (maxNumberOfReasks2 == 0 || gConfInfo.oneCycleFlag == true)
             {
                 stopFlag = true;
             }
		 }
		 else
		 {
             // get original value of minWaitInterval back;
            minWaitInterval = minWaitInterval / reducFactor;
            startupModFactor = 1.0;

            // strategy: first get in startup phase reasks below 50, then try in measurement phase to get reasks to 0
            if (maxNumberOfReasks1 > 0.0001 * (nevents2[0] | nevents2[1] | nevents2[2]) )
            {
                startupModFactor = 1.0 + reducFactorStep;
            }
            else
            {
                if (maxNumberOfReasks2 == 0)
                {
                    if (firstFlag == false)
                    {
                        if (increaseReducFactorFlag == true)
                        {
                            stopFlag = true;
                        }
                    }
                    else
                    {
                        increaseReducFactorFlag = false;
                    }
                }
                else
                {
                    // maxNumberOfReasks2 != 0
                    if (firstFlag == false)
                    {
                        if (increaseReducFactorFlag == false)
                        {
                            // return to maxNumberOfReasks2 == 0
                            increaseReducFactorFlag = true;
                        }
                    }
                    else
                    {
                        increaseReducFactorFlag = true;
                    }
                }
                if (increaseReducFactorFlag == true)
                {
                    reducFactor = reducFactor + reducFactorStep;
                }
                else
                {
                    reducFactor = reducFactor - reducFactorStep;
                }
                if (firstFlag == true)
                {
                    firstFlag = false;
                }
            }
            printf (">>>> new startupModFactor %f, reducFactor %f, step %f\n", startupModFactor, reducFactor, reducFactorStep);
            for (u_int i = 0; i < maxNumberOfTickets; i++)
			{
                waitingTime[0][i] = minWaitInterval * (float) (maxNumberOfTickets) * startupModFactor;
                waitingTime[1][i] = waitingTime[0][i];
                waitingTime[2][i] = waitingTime[0][i];
			}
         }

		 for (u_int i = 0; i < maxNumberOfTickets; i++)
		 {
			datal[i] = 0;
			datay[i] = 0;
			for (looprol = 0; looprol < nrols; looprol++)
			{
		datan[looprol][i] = 0;
		datar[looprol][i] = 0;
			}
		 }

		 nevents2[0] = numberOfEventsFirst * (gConfInfo.linkMask & 1);
		 nevents2[1] = numberOfEventsFirst * ((gConfInfo.linkMask & 2) / 2);
		 nevents2[2] = numberOfEventsFirst * ((gConfInfo.linkMask & 4) / 4);
		 nevents[0]  = (int) ((float) nevents2[0] * percent / 100.0);     
		 nevents[1]  = (int) ((float) nevents2[1] * percent / 100.0);
		 nevents[2]  = (int) ((float) nevents2[2] * percent / 100.0);

		 reAskFlag = false;
		 requeueCounter[0] = 0;
		 requeueCounter[1] = 0;
		 requeueCounter[2] = 0;

		 ts_clock(&ts0);
      }
     while ((stopFlag == false) && (reducFactor < 1.1));
  
		gConfInfo.requestFraction = percent;
		if (theRate > gConfInfo.targetDeleteRate)
		{
			if (downCount > 0)
			{
				stepSize = stepSize / 2.0;
				downCount = 0;
			}
			percent = percent + stepSize;
			upCount++;
		}
		else
		{
			if (upCount > 0)
			{
				stepSize = stepSize / 2.0;
				upCount = 0;
			}
			percent = percent - stepSize;
			downCount++;
		}
        if (!gConfInfo.iterateFlag)
        {
            break;
        }
        else
        {
            // next iteration step, reinit flags and timeintervals
            firstCycle = true;
            stopFlag = false;
            firstFlag = true;
            for (u_int i = 0; i < maxNumberOfTickets; i++)
            {
              datal[i] = 0;
              datay[i] = 0;
              for (looprol = 0; looprol < nrols; looprol++)
              {
                datan[looprol][i] = 0;
                datar[looprol][i] = 0;
                waitingTime[looprol][i] = 0.0;
              }
            }
        }
	}

    fragSize = gConfInfo.numberOfWordsPerFragment + (gConfInfo.externalDataGeneratorFlag ? 10 : 23);
    if (receivedFragSize != fragSize)
    {
        printf("Size received fragments: %d words != expected size = %d words\n", receivedFragSize, fragSize);
    }
    bw1 = ((float) nrolsUsed) * gConfInfo.requestFraction * theRate * 4.0 * ((float) receivedFragSize)/ 100000.0;
    printf("Next line: LVL1 (kHz), req. rate (kHz), req. fraction (%c), fragment size (words), size+hdrs/trailer (words), bw (MByte/s)\n",'%');
    printf("Final   %7.3f %7.3f %5.3f %d %d %5.3f\n",
           theL1Rate, 3.0 * gConfInfo.requestFraction * theRate / 100.0, gConfInfo.requestFraction,
           gConfInfo.numberOfWordsPerFragment, receivedFragSize, bw1);


  if (gConfInfo.nonIntPerformMeasFlag)
  {

    if (gConfInfo.verboseFlag )
        printf ("\nLVL1 rate (kHz), request rate (kHz), request fraction (%c), fragment size (words), size+hdrs/trailer (words), bandwidth (MByte/s)\n",'%');

    bw1 = ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatex * 4.0 * ((float) receivedFragSize)/ 100000.0;
    printf ("maxnoreasks   %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRatex1, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatex / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, bw1);
    bw2 = ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatey * 4.0 * ((float) receivedFragSize)/ 100000.0;
    printf ("maxwithreasks %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRatey1, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatey / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, bw2);
    if (maxRatex > maxRatey)
        printf ("maxall        %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRatex1, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatex / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, bw1);
    else
        printf ("maxall        %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRatey1, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRatey / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, bw2);
  }
}


/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  getstat = 1;
}


/**********************/
void documentation(void)
/**********************/
{
 std::string docString[] =
 {"Documentation for performance measurements:",
  "",
  "A performance measurement is done with either the internal data generator or an", 
  "external S-link source. In the latter case the dolar-slink_src program has to",
  "be controlled from the computer from which test_Robin is run. This is done with",
  "the help of ssh, which invokes a script setting up the appropriate environment,",
  "followed by starting the dolar_slink_src program with appropriate parameters. ",
  "For example, the script (with name dolar) could contain:",
  "",
  "#!/bin/tcsh",
  "setenv CMTCONFIG i686-slc4-gcc34-opt",
  "source /project/atlas/opt/tdaq/tdaq/tdaq-01-08-03/installed/setup.csh",
  "dolar_slink_src -c 1110 -s $1 -w 4  >>&! dolarout.txt &",
  "",
  "which is then started (assuming dolar is found in the home directory) using:",
  "",
  "ssh machine -x dolar fragsize",
  "",
  "where \"fragsize\" is used as argument of the -s parameter of the dolar_slink_src",
  "program, \"machine\" is the name of the computer with the DOLAR card(s).",
  "",
  "NB: use -w 10 for the PCIe ROBIN.",
  "",
  "The script name can be specified as parameter using the -a option.",
  "In that case fragsize does not need to be specified, it is taken from the",
  "-w command line parameter.",
  "",
  "At the end of the execution of test_Robin the dolar_slink_src program on the",
  "remote computer has to be stopped. This is again done with an appropriate",
  "script, which contains (if the name of the script is killdolar)",
  "",
  "foreach name ( `ps aux | grep \"dolar\" | grep -v grep | grep -v killdolar | awk '{print $2}'` )",
  "kill -TERM $name",
  "",
  "Running it is done with:",
  "",
  "ssh machine  -x killdolar",
  "",
  "or by supplying the name of the script as argument of the -z parameter.",
  "",
  "For each measurement there are two phases, a setup phase followed by a measuring",
  "phase. For the first setup and measuring phase the number of requests for which",
  "no fragment is returned (i.e. that have to be re-issued) are counted. The program",
  "finishes if for all requests during the measuring phase a freagment was returned,",
  "otherwise the program continues with a new setup and measuring phase.",
  "During the setup phase requests for which no fragment was returned give rise to an",
  "increase in the minimum time between successive requests for a ROL and for a ",
  "certain \"ticket\" (= outstanding request). There are at max. 3 ROLs and 4",
  "tickets per ROL, so 12 delays are adjusted. After the setup phase the average",
  "of the delays is computed, multiplied by a certain factor (the \"reduction ",
  "factor\") and with these delays a measurement of the fragment input rate per ROL",
  "(the \"LVL1 rate\") is done. After the first setup and measuring phase a second",
  "cycle is started only if there were requests not returning a fragment during",
  "the measurement phase. In that case first the minimum time interval between",
  "successive requests during the setup phase is set by adjusting it until",
  "there are less than 0.001*nevents reasks. Next the reduction",
  "factor is adjusted upward until in a next cycle no reasks are necessary. The",
  "factor is reduced if no reasks have been seen until reasks are necessary and",
  "then it is increased one step. Then the process stops and the LVL1",
  "rate measured is the outcome of the measurement.",
  "",
  "Example usage: test_Robin -p -r 10 -v : here a request fraction of 10% and",
  "verbose output is selected, the internal data generator is used.",
  ""};

  for (int i = 0; i < 62; i++)
  {
    std::cout << docString[i] << std::endl;
  }

}



/**************/
void usage(void)
/**************/
{
  std::cout << "Option of original version: " << std::endl;
  std::cout << "-t: Run in test mode. I.e. do not disable the ROLs after the test " << std::endl;
  std::cout << "Options for non-interactive performance measurements: " << std::endl;
  printf("-a  name of script for running dolar_slink_src [%s]\n",  gConfInfo.dolarScriptName);
  printf("-b  name of computer with DOLAR card [%s] \n", gConfInfo.dolarComputerName);
  printf("-d: documentation\n");
  printf("-e: external data source\n");
  printf("-f  fraction of events used for setup phase [%f]\n", gConfInfo.setupNeventFraction);
  printf("-h: this screen\n");
  printf("-i: check data and print the size of each fragment\n");
  printf("-j: non-interactive dump of data\n");
  printf("-k  size of delete group [%d]\n", gConfInfo.deleteGroupSize);
  printf("-n  number of LVL1 accepts per cycle [%d] \n", gConfInfo.numberOfLVL1AcceptsPerCycle);
  printf("-m  link mask [%d]\n", gConfInfo.linkMask);
  printf("-o: one cycle only\n");
  printf("-p: non-interactive perf. measurement\n");
  printf("-q  initial value of reduction factor [%f]\n", gConfInfo.initialValueReducFactor);
  printf("-r  request fraction in percent [%f]\n", gConfInfo.requestFraction);
  printf("-s  step size of reduction factor [%f]\n", gConfInfo.initialStepReducFactor);
  printf("-v: verbose output\n");
  printf("-w  words per fragment [%d]\n", gConfInfo.numberOfWordsPerFragment);
  printf("-x  ROBIN page size [%d]\n", gConfInfo.pageSize);
  printf("-y  ROBIN number of pages [%d]\n", gConfInfo.numPages);
  printf("-z  name of script for killing dolar_slink_src [%s] \n", gConfInfo.nameOfKillScript);
  printf("-P  PCI slot number [%d]\n", gConfInfo.pciSlotNumber);
  printf("-S  ROBIN software version [0x%x]\n", gConfInfo.softwareVersion);
  printf("-F  ROBIN FPGA configuration version [0x%x] \n", gConfInfo.firmwareVersion);
  printf("-T  Target delete rate [%f kHz]\n", gConfInfo.targetDeleteRate);
  printf("-V  Data check mode. 0=no check, 1=full check, 2=stop after first error. [%d]\n", gConfInfo.check_verbosity);
  printf("-C: Combine clears for all ROLs in 1 command sent to ROBIN\n");
  printf("-D: Do not send clears, use the  same Id (0) for all requests\n");
  printf("-E  PCIe ROBIN -> default FPGA configuration version = 0x%x\n", gConfInfo.firmwareVersionPCIe);
  printf("-M  MaxRXPages: maximum number of pages per fragment [%d]\n", gConfInfo.maxRXPages);
  printf("-N  MemoryPoolNumPages: maximum number of pages in ROS memory pool [%d]\n", gConfInfo.memoryPoolNumPages);
  printf("-O  MemoryPoolPageSize: size of page in ROS memory pool [%d]\n", gConfInfo.memoryPoolPageSize);
  printf("-J  PreScaleFrag: ROBIN expert parameter [%d]\n", gConfInfo.preScaleFrag);
  printf("-K  PreScaleMsg: ROBIN expert parameter [%d]\n", gConfInfo.preScaleMsg);
  printf("-L  PreScaleFpf: ROBIN expert parameter [%d]\n", gConfInfo.preScaleFpf);
  printf("-R  maximum number of outstanding requests [%d]\n", gConfInfo.numberOfOutstandingReq);
}

/*****************************/
int main(int argc, char **argv)
/*****************************/ 
{
  int c, testmode = 0, count, num, value, il1id, l1id, option, quit_flag = 0;
  u_int lastticket = 0, dataChannel;
  std::vector<DataChannel *> *channels;
  std::vector <u_int> idgroup, idgroup2, idgroup3;
  struct sigaction sa;
  ReadoutModule* ReadoutModule[FM_POOL_NUMB] = {0};
  ROBFragment* robfragment;
  DFCountedPointer<Config> configuration = Config::New();
  char *theEnd;
daq::rc::TransitionCmd transCmd(daq::rc::FSM_COMMAND::INIT_FSM); // Dummy run control transition command not used by any RobinNPReadoutModule methods


  while ((c = getopt(argc, argv, "ta:b:c:def:hijk:n:m:ops:q:r:vw:x:y:z:P:S:F:T:V:CDEM:N:O:J:K:L:R:")) != -1)
  {
    if (testmode == 1) 
    {
      std::cout << "-t not allowed together with other options" << std::endl; 
      exit(0);
    }
    switch (c)
    {
    case 'a': gConfInfo.SetRemoteStartCmd(optarg); break;
    case 'b': gConfInfo.SetRemoteComputer(optarg); break;
    case 'd': documentation(); exit(0); break;
    case 'e': gConfInfo.externalDataGeneratorFlag = true; break;
    case 'f': gConfInfo.setupNeventFraction = strtod(optarg, &theEnd); break;
    case 'h': usage(); exit(0); break;
    case 'i': gConfInfo.dataCheckFlag = true; break;
    case 'j': gConfInfo.dataDumpFlag = true; break;
    case 'k': gConfInfo.deleteGroupSize = strtol(optarg, &theEnd, 0); break;
    case 'm': gConfInfo.SetLinkMask(strtol(optarg, &theEnd, 0)); break;
    case 'n': gConfInfo.numberOfLVL1AcceptsPerCycle = strtol(optarg, &theEnd, 0); break;
    case 'o': gConfInfo.oneCycleFlag = true; break;
    case 'p': gConfInfo.nonIntPerformMeasFlag = true; break;
    case 's': gConfInfo.initialStepReducFactor = strtod(optarg, &theEnd); break;
    case 'q': gConfInfo.initialValueReducFactor = strtod(optarg, &theEnd); break;
    case 'r': gConfInfo.requestFraction = strtod(optarg, &theEnd); break;
    case 't': testmode = 1; break;
    case 'v': gConfInfo.verboseFlag = true; break;
    case 'w': gConfInfo.numberOfWordsPerFragment = strtol(optarg, &theEnd, 0); break;
    case 'x': gConfInfo.pageSize = strtol(optarg, &theEnd, 0); break;
    case 'y': gConfInfo.numPages = strtol(optarg, &theEnd, 0); break;
    case 'z': gConfInfo.SetRemoteKillCmd(optarg); break;
    case 'P': gConfInfo.pciSlotNumber = strtol(optarg, &theEnd, 0); break;
    case 'S': gConfInfo.softwareVersion = strtol(optarg, &theEnd, 0); break;
    case 'F': gConfInfo.firmwareVersion = strtol(optarg, &theEnd, 0); break;
    case 'T': gConfInfo.targetDeleteRate = strtod(optarg, &theEnd); gConfInfo.iterateFlag=true;break;
    case 'V': gConfInfo.check_verbosity = strtol(optarg, &theEnd, 0); break;
    case 'C': gConfInfo.clearAllFlag = true; break;
    case 'D': gConfInfo.noClearsFlag = true; break;
    case 'E': gConfInfo.busIsPCIFlag = false; break;
    case 'M': gConfInfo.maxRXPages = strtol(optarg, &theEnd, 0); break;
    case 'N': gConfInfo.memoryPoolNumPages = strtol(optarg, &theEnd, 0); break;
    case 'O': gConfInfo.memoryPoolPageSize = strtol(optarg, &theEnd, 0); break;
    case 'J': gConfInfo.preScaleFrag = strtol(optarg, &theEnd, 0); break;
    case 'K': gConfInfo.preScaleMsg = strtol(optarg, &theEnd, 0); break;
    case 'L': gConfInfo.preScaleFpf = strtol(optarg, &theEnd, 0); break;
    case 'R': gConfInfo.numberOfOutstandingReq = strtol(optarg, &theEnd, 0); break;
    default:
        std::cout << "Invalid option " << c << std::endl;
        std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
        usage();
        exit (0);
    }
}

  if (!gConfInfo.busIsPCIFlag)
  {
      if (gConfInfo.firmwareVersion == gConfInfo.firmwareVersionPCI)
      {
          // default firmware version was not changed -> a PCIe ROBIN is used, therefore set version to PCIe default version
          gConfInfo.firmwareVersion = gConfInfo.firmwareVersionPCIe;
      }
  }
  
  ts_open(1, TS_DUMMY); 

  TS_OPEN(100000, TS_H2);	// for Robin S/W profiling
  TS_START(TS_H2);
  TS_RECORD(TS_H2,1);
   
  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0)
  {
    std::cout << "main: sigaction() FAILED with ";
    int code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (-1);
  }
  
  try 
  {
    IPCCore::init(argc, argv);
  }
  catch(daq::ipc::Exception & ex) 
  {
    ers::fatal(ex);
  }

  try
  {
    //compute CRC table
    {
      const u_long crchighbit = (u_long) (1 << 15);
      const u_long polynom =  0x1021;
      for (int i = 0; i < 65536; i++) 
      {
	u_long crc = i;

	for (int j = 0; j < 16; j++) 
	{
          u_long bit = crc & crchighbit;
          crc <<= 1;
          if (bit) 
            crc ^= polynom;
	}			
        crctab16[i] = crc & 0xffff; 
      }
    }

      // uncomment next line for debugging output
      // DF::GlobalDebugSettings::setup(20, 1);

    if (gConfInfo.externalDataGeneratorFlag == true)
	{
		gConfInfo.SendRemoteKill();
		gConfInfo.SendRemoteCommand();
	}

    if (gConfInfo.nonIntPerformMeasFlag  || gConfInfo.dataCheckFlag || gConfInfo.dataDumpFlag)
    {
      gConfInfo.SetParams(configuration);
      if (ReadoutModule[0] != 0)
      {
        std::cout << "Array element 0 has already been filled"  << std::endl;
        return 1;
      }
      if (gConfInfo.verboseFlag == false && gConfInfo.dataCheckFlag == false && gConfInfo.dataDumpFlag == false)
        freopen ("/dev/null", "a+", stdout);
 
      ReadoutModule[0] = new RobinReadoutModule();
      ReadoutModule[0]->setup(configuration);
      ReadoutModule[0]->configure(transCmd);
      ReadoutModule[0]->prepareForRun(transCmd);
      if (gConfInfo.verboseFlag == false && gConfInfo.dataCheckFlag == false && gConfInfo.dataDumpFlag == false)
      {
          int fd;

          fclose(stdout);
          // found this at: http://www.thescripts.com/forum/thread222840.html
          fd = open("/dev/tty", O_WRONLY);
          stdout = fdopen(fd, "w");
      }

      DoPerformanceMeasurement(ReadoutModule);

      ReadoutModule[0]->stopGathering(transCmd);
      ReadoutModule[0]->unconfigure(transCmd);
      delete ReadoutModule[0];
      ReadoutModule[0] = 0;
      if (gConfInfo.externalDataGeneratorFlag == true)
        gConfInfo.SendRemoteKill();
    }
    else
    {
      do 
      {
	std::cout << std::dec << std::endl << std::endl;
	std::cout << " Automatic tests:  " << std::endl;
	std::cout << " Run synchronous benchmark (request all)   : " << TIMING4 << std::endl; 
	std::cout << " Run synchronous benchmark (request some)  : " << TIMING5 << std::endl; 
	std::cout << " Run synchronous benchmark (up to 3 ROLs)  : " << TIMING6 << std::endl; 
	std::cout << " Run integrated release test (up to 3 ROLs): " << TIMING7 << std::endl;
	std::cout << " Run slow Robin test                       : " << TIMING8 << std::endl;
	std::cout << "==============================================" << std::endl;
	std::cout << " Create a RobinReadoutModule               : " << CREATE << std::endl; 
	std::cout << " Configure a RobinReadoutModule            : " << CONFIGURE << std::endl;  
	std::cout << " Start a RobinReadoutModule                : " << START << std::endl;
	std::cout << " Request a ROB fragment                    : " << REQUEST << std::endl; 
	std::cout << " Get a ROB fragment                        : " << GET << std::endl; 
	std::cout << " Run garbage collection                    : " << GC << std::endl; 
	std::cout << " Release many ROB fragments                : " << RELEASEMANY << std::endl;
        std::cout << " Send a user command                       : " << USERCOMMAND << std::endl;
	std::cout << " Stop a RobinReadoutModule                 : " << STOP << std::endl;
	std::cout << " Unconfigure a Readout Module              : " << UNCONFIGURE << std::endl; 
	std::cout << " Delete a Readout Module                   : " << DELETE << std::endl; 
	std::cout << " Request many Fragments and dump           : " << DUMPMANY << std::endl; 
	std::cout << " Get ROBIN statistics (getInfo)            : " << GETINFO << std::endl;
	std::cout << " Set a configuration variable              : " << SETCONFIG << std::endl;
	std::cout << " Reset ROBIN                               : " << RESET << std::endl;
	std::cout << " Set debug parameters                      : " << SETDEBUG << std::endl;
	std::cout << " HELP                                      : " << HELP << std::endl;
	std::cout << " QUIT                                      : " << QUIT << std::endl;
	std::cout << " ? : ";

	option = getdec();    
	switch(option)   
	{
	  case CREATE:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] != 0)
	    {
              std::cout << "This array element has already been filled"  << std::endl;
              break;
	    }
	    ReadoutModule[num] = new RobinReadoutModule();
	    std::cout << "Readout Module created" << std::endl;
	    break;

	  case CONFIGURE:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }     

            getparams(configuration, 0);
	    ReadoutModule[num]->setup(configuration);
	    ReadoutModule[num]->configure(transCmd);
	    break;

	  case START:
            std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
            num = getdecd(0);

            if (ReadoutModule[num] == 0)
            {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
            }

            ReadoutModule[num]->prepareForRun(transCmd);
            break;


	  case REQUEST:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2)
	    {
	      std::cout << "Wrong channel number." << std::endl;
              break;
	    }

	    std::cout << "Enter the Level 1 ID" << std::endl;
	    l1id = gethexd(0); 

	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());

	    value = (*channels)[dataChannel]->requestFragment(l1id); 
	    std::cout << "Request returns the ticket " << value << std::endl;
	    lastticket = value;   
	    break;

	  case GET:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2)
	    {
	      std::cout << "Wrong channel number." << std::endl;
              break;
	    }

	    std::cout << "Enter the ticket received from request()" << std::endl;
	    l1id = getdecd(lastticket); 

	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());
	    robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(l1id)); 
	    if (robfragment == 0) // may come or lost
              std::cout << " getFragment returns : " << robfragment << " i.e. fragment may come or is lost " << std::endl;
	    else 
	    {
              std::cout << "Dumping ROB fragment" << std::endl;
              std::cout << "ROB fragment pointer = " << std::hex << robfragment << std::dec << std::endl;
              std::cout << *robfragment;
              delete robfragment;
	    }
	    break;

	  case GC:
	  {
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2)
	    {
	      std::cout << "Wrong channel number." << std::endl;
              break;
	    }

	    std::cout << "Enter the oldest valid L1ID" << std::endl;
	    l1id = getdecd(0); 

	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());
	    u_int success = (*channels)[dataChannel]->collectGarbage(l1id);
	    std::cout << "The result is " << success << std::endl;
	  }	 
	  break;

	  case RELEASEMANY: 
	  {
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);      

	    if (ReadoutModule[num] == 0)
	    {
	      std::cout << "This array element has not yet been filled"  << std::endl;
	      break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2) 
	    {
	      std::cout << "Wrong channel number." << std::endl;
	      break;
	    }

	    std::cout << "How many events do you want to release" << std::endl;
	    value = getdecd(100);

	    std::cout << "Enter the L1ID to start with" << std::endl;
	    il1id = getdecd(0);

	    idgroup.clear();

	    for(int loop = il1id; loop < (il1id + value); loop++)
	      idgroup.push_back(loop);    

	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());
            (*channels)[dataChannel]->releaseFragment(&idgroup);        
	  }
          break;





         case USERCOMMAND:
           std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
           num = getdecd(0);
            if (ReadoutModule[num] == 0)
            {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
            }

            {
               std::string commandName;
               std::cout << "Enter the command" << std::endl;
               std::cin >> commandName;

               std::cout << "Enter the list of ROLS" << std::endl;
               //getstrd(user_command1, "1,3,99");
               std::vector<std::string> commandParams;
               std::string user_s2;
               do {
                  std::cin >> user_s2;
                  if (!user_s2.empty()) {
                     commandParams.push_back(user_s2);
                  }
               } while (!user_s2.empty());
               std::cout << "Sending command " << commandName << "-"  << " to the ReadoutModule" << std::endl;

               daq::rc::UserCmd userCommand(commandName,commandParams);
               ReadoutModule[num]->user(userCommand);
            }
            break;


	  case STOP:
            std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
            num = getdecd(0);

            if (ReadoutModule[num] == 0)
            {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
            }

            ReadoutModule[num]->stopGathering(transCmd);
            break;

	  case UNCONFIGURE:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
	    num = getdecd(0);      

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }
	    ReadoutModule[num]->unconfigure(transCmd);
	    break;

	  case DELETE:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
	    num = getdecd(0);      

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled" << std::endl;
              break;
	    }

	    delete ReadoutModule[num];
	    ReadoutModule[num] = 0;
	    break;

	  case TIMING4: 
	  {
	    u_int nevents, nevents2, nextticket, nextl1id, tickets[4][2];
	    u_int isempty, loop;
	    u_int ddd = 0, datay = 0, datan = 0, ndel = 0;
	    ROBFragment *fragment;
	    tstamp ts1, ts2;
	    float delta1, delta2;

	    if (ReadoutModule[0] != 0)
	    {
              std::cout << "Array element 0 has already been filled"  << std::endl;
              return 1;
	    }
	    ReadoutModule[0] = new RobinReadoutModule();

            getparams(configuration, 1);

	    dataChannel = 0;  // index into channels

	    ReadoutModule[0]->setup(configuration);
	    ReadoutModule[0]->configure(transCmd);
	    ReadoutModule[0]->prepareForRun(transCmd);

	    std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
	    ddd = getdecd(ddd);     

	    std::cout << "How many events do you want to process " << std::endl;
	    nevents = getdecd(1000);     
	    nevents2 = nevents;
	    nextl1id = 1;
	    getstat = 0;
	    channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());
	    std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

	    ts_clock(&ts1);
	    //Fill the message Q
	    for (loop = 0; loop < 4; loop++)
	    {
              if (ddd)
        	std::cout << "Posting request for L1ID " << nextl1id << std::endl;
              tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
              tickets[loop][1] = nextl1id;
              nextl1id++;
	    }
	    nextticket = 0;

	    idgroup.clear();

	    while(nevents)
	    {
      	      //Wait for a complete ROB fragment
              isempty = 1;       
              while(isempty)
              {
		if (getstat)
		{
		  getstat = 0;
		  std::cout << "Number of empty events             = " << datan << std::endl;
        	  std::cout << "Number of complete events          = " << datay << std::endl;
        	  std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
		  std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " << tickets[1][1] << ", "  << tickets[2][1] << " and " << tickets[3][1] << std::endl;
		}  

        	if (ddd)
        	  std::cout << "waiting for L1ID " << tickets[nextticket][1] << std::endl;

        	fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

        	if (fragment) 
		{ // good fragment (with data)
        	  if (ddd) 
        	    std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;

        	  delete fragment;

        	  idgroup.push_back(tickets[nextticket][1]);
		  if (ddd)
        	    std::cout << "L1ID = " << tickets[nextticket][1] << " received and scheduled for release (datay = " << datay << ")" << std::endl;
		  isempty = 0;
        	  datay++;
        	}
        	else  //Requeue the L1ID: lost fragments are not handled correctly ...
        	{
        	  if (ddd)
        	    std::cout << "Requeueing L1ID " <<  tickets[nextticket][1] << std::endl;

        	  tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
        	  nextticket++;
        	  if (nextticket == 4)
        	    nextticket = 0;
        	  datan++;
        	}
              }  

              //Queue a new L1ID
 	      if (ddd)
        	std::cout << "Requesting new L1ID = " << nextl1id << std::endl;
              tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
              tickets[nextticket][1] = nextl1id;
              nextl1id++;
              nextticket++;
              if (nextticket == 4)
        	nextticket = 0;

              //Send a delete request if we have received 100 events
              if ((datay % 100) == 0)
              {
        	if (ddd)
        	  std::cout << "100 events released" << std::endl;

        	(*channels)[dataChannel]->releaseFragment(&idgroup);  
        	idgroup.clear();
		ndel++;
              } 
              nevents--;
	    }

	    ts_clock(&ts2);
	    delta1 = 1000000.0 * ts_duration(ts1, ts2);

	    std::cout << "Number of getFragment without data = " << datan << std::endl;
	    std::cout << "Number of getFragment with data    = " << datay << std::endl;
	    std::cout << "Total time     =  " << delta1 << " us" << std::endl;
	    delta2 = delta1 / nevents2;
	    std::cout << "Time per event =  " << delta2 << " us" << std::endl;

	    ReadoutModule[0]->stopGathering(transCmd);
	    ReadoutModule[0]->unconfigure(transCmd);
	    delete ReadoutModule[0];
	    ReadoutModule[0] = 0;
	  }
	  break;

	  case TIMING5: 
	  {
	    u_int clearfirst, nevents, nevents2, nextticket, nextl1id, tickets[4][2];
	    u_int isempty, loop;
	    u_int min1, min2, ddd = 0, datay = 0, datan = 0, ndel = 0;
	    ROBFragment *fragment;
	    tstamp ts1, ts2;
	    float limit, percent = 10.0, delta1, delta2;

	    if (ReadoutModule[0] != 0)
	    {
              std::cout << "Array element 0 has already been filled"  << std::endl;
              return 1;
	    }
	    ReadoutModule[0] = new RobinReadoutModule();

            getparams(configuration, 1);

	    dataChannel = 0;	// only ONE channel

	    ReadoutModule[0]->setup(configuration);
	    ReadoutModule[0]->configure(transCmd);
	    ReadoutModule[0]->prepareForRun(transCmd);

	    std::cout << "Enter the (float) request percantage" << std::endl;
	    percent = getfloatd(percent);    

	    std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
	    ddd = getdecd(ddd); 

	    std::cout << "How many events do you want to process " << std::endl;
	    nevents = getdecd(1000);     
	    nevents2 = nevents;
	    nextl1id = 0;
	    clearfirst = 1;
	    limit = 0.0;
	    getstat = 0;
	    channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());
	    std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

	    ts_clock(&ts1);
	    //Fill the message Q
	    tickets[0][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
	    tickets[0][1] = nextl1id;      
	    for (loop = 1; loop < 4; loop++)
	    {
              nextl1id = getnext(percent, limit, nextl1id);
	      tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
              tickets[loop][1] = nextl1id;
	    }
	    nextticket = 0;

	    while(nevents)
	    {
      	      //Wait for a complete ROB fragment
              isempty = 1;       
              while(isempty)
              {
		if (getstat)
		{
		  getstat = 0;
		  std::cout << "Number of empty events             = " << datan << std::endl;
        	  std::cout << "Number of complete events          = " << datay << std::endl;
        	  std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
		  std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " << tickets[1][1] << ", " << tickets[2][1] << " and " << tickets[3][1] << std::endl;
		}  

        	if (ddd)
        	  std::cout << "waiting for L1ID " << tickets[nextticket][1] << std::endl;

        	fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

        	if (fragment) 
		{ // good fragment (with data)
        	  if (ddd) 
        	    std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;

        	  delete fragment;

        	  idgroup.push_back(tickets[nextticket][1]);
        	  if (ddd)
                    std::cout << "L1ID = " << tickets[nextticket][1] << " received and scheduled for release (datay = " << datay << ")" << std::endl;
		  isempty = 0;
        	  datay++;
        	}
        	else  //Requeue the L1ID; lost fragments are not handled correctly
        	{
        	  tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
        	  nextticket++;
        	  if (nextticket == 4)
        	    nextticket = 0;
        	  datan++;
        	}
              }  

              //Queue a new L1ID
              nextl1id = getnext(percent, limit, nextl1id);
              tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
              tickets[nextticket][1] = nextl1id;
              nextticket++;
              if (nextticket == 4)
        	nextticket = 0;

              //Find the lowest L1ID that we are still waiting for
              if (tickets[0][1] < tickets[1][1])
		min1 = tickets[0][1];
	      else
		min1 = tickets[1][1];

              if (tickets[2][1] < tickets[3][1])
		min2 = tickets[2][1];
	      else
		min2 = tickets[3][1];

	      if (min1 < min2)
		min2 = min1; 	  

              if (ddd)
               std::cout << "Lowest outstanding L1ID = " << min2 << std::endl;

              //Send a delete request if we have received 100 events
              if ((clearfirst + 100) < min2)
	      {  
        	idgroup.clear();
        	for(int loop = 0; loop < 100; loop++)
        	  idgroup.push_back(clearfirst++);
		(*channels)[dataChannel]->releaseFragment(&idgroup);
		ndel++;  
        	if (ddd)
		  std::cout << "events released up to L1ID " << clearfirst - 1 << std::endl;
              } 
              nevents--;
	    }

	    ts_clock(&ts2);
	    delta1 = 1000000.0 * ts_duration(ts1, ts2);

	    std::cout << "Number of getFragment without data = " << datan << std::endl;
	    std::cout << "Number of getFragment with data    = " << datay << std::endl;
	    std::cout << "Total time     =  " << delta1 << " us" << std::endl;
	    delta2 = delta1 / nevents2;
	    std::cout << "Time per event out =  " << delta2 << " us" << std::endl;
	    delta2 = delta2 * percent / 100.0;
	    std::cout << "Time per event in =  " << delta2 << " us" << std::endl;

	    ReadoutModule[0]->stopGathering(transCmd);
	    ReadoutModule[0]->unconfigure(transCmd);
	    delete ReadoutModule[0];
	    ReadoutModule[0] = 0;
	  }
	  break;

	  case TIMING6: 
	  {
	    u_int nevents[3], nevents2[3], nextticket[3], nextl1id[3], tickets[3][4][2];
	    u_int rdump = 0, cdata = 0, isempty, loop, looprol;
	    u_int rol, ddd = 0, datal[3] = {0, 0, 0}, datay[3] = {0, 0, 0}, datan[3] = {0, 0, 0}, ndel[3] = {0, 0, 0};
	    u_int nrols, numberOfBadEvents = 0;
	    bool eventOk;
	    ROBFragment *fragment;
	    tstamp ts1, ts2;
	    float delta1, delta2;

            TS_PAUSE(TS_H2);
            nrols = getparams(configuration, 0);
	    if (ReadoutModule[0] != 0)
	    {
              std::cout << "Array element 0 has already been filled"  << std::endl;
              return 1;
	    }
	    ReadoutModule[0] = new RobinReadoutModule();
	    ReadoutModule[0]->setup(configuration);
	    ReadoutModule[0]->configure(transCmd);
	    ReadoutModule[0]->prepareForRun(transCmd);

	    std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
	    ddd = getdecd(ddd);  

	    std::cout << "Enable data checking (0 = No  1 = Yes  2 = Reduced output)" << std::endl;
	    cdata = getdecd(cdata);

	    std::cout << "Dump raw data (0 = No  1 = Yes)" << std::endl;
	    rdump = getdecd(rdump);

	    std::cout << "How many events do you want to process " << std::endl;
	    nevents[0] = getdecd(200000);     
	    nevents[1] = nevents[0];
	    nevents[2] = nevents[0];
	    nevents2[0] = nevents[0];
	    nevents2[1] = nevents[1];
	    nevents2[2] = nevents[2];

	    if (nrols < 3)
	    {
	      nevents2[2] = 0;
	      nevents[2] = 0;
	    }
	    if (nrols < 2)
	    {
	      nevents2[1] = 0;
	      nevents[1] = 0;
	    } 

	    nextl1id[0] = 1;
	    nextl1id[1] = 1;
	    nextl1id[2] = 1;
	    getstat = 0;
	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[0]->channels());
            TS_RESUME(TS_H2);
	    std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

	    ts_clock(&ts1);
	    //Fill the message Q
	    for (looprol = 0; looprol < nrols; looprol++)
	    {
	      for (loop = 0; loop < 4; loop++)
	      {
        	tickets[looprol][loop][0] = (*channels)[looprol]->requestFragment(nextl1id[looprol]);
        	tickets[looprol][loop][1] = nextl1id[looprol];
        	nextl1id[looprol]++;
	      }
	      nextticket[looprol] = 0;
            }
	    idgroup.clear();
	    idgroup2.clear();
	    idgroup3.clear();
	    rol = 0;

	    while((nevents[0] != 0) || (nevents[1] != 0) || (nevents[2] != 0))
	    {
              isempty = 1;       
              while(isempty)  //Wait for a complete ROB fragment
              {
		if (getstat)
		{
		  getstat = 0;
		  for (looprol = 0; looprol < nrols; looprol++)
		  {
		    std::cout << "ROL " << looprol << ":" << std::endl;
        	    std::cout << "Number of complete events          = " << datay[looprol] << std::endl;
		    std::cout << "Number of missing events           = " << datan[looprol] << std::endl;
        	    std::cout << "Number of lost events              = " << datal[looprol] << std::endl;
        	    std::cout << "Number of events to be requested   = " << nevents[looprol] << std::endl;
        	    std::cout << "Number of release(100) groups sent = " << ndel[looprol] << std::endl;
		    std::cout << "Currently waiting for L1IDs " << tickets[looprol][0][1] << ", " << tickets[looprol][1][1] << ", " << tickets[looprol][2][1] << " and " << tickets[looprol][3][1] << std::endl;
		  }
		}  

        	while(nevents[rol] == 0)
		{
        	  rol++;                 //try another ROL
		  if (rol == nrols)
	            rol = 0;
        	}

        	if (ddd)
        	  std::cout << "waiting for L1ID " << tickets[rol][nextticket[rol]][1] << " from ROL " << rol << std::endl;

        	fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));

        	if (fragment) 
		{ // good fragment (with data)
        	  if (ddd) 
        	    std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;

        	  if (cdata)  
		  {
		    if (ddd)
		    {
	              std::cout << "Calling checkData with" << std::endl;
	              std::cout << "fragment at 0x " << &fragment << std::endl;
	              std::cout << "tickets[rol][nextticket[rol]][1] = " << tickets[rol][nextticket[rol]][1] << std::endl;
	              std::cout << "rol                              = " << rol << std::endl;
	              std::cout << "rdump                            = " << rdump << std::endl;
		    }
        	    eventOk = checkData(fragment, tickets[rol][nextticket[rol]][1], rol, cdata);
        	    if (eventOk != 0)
                      numberOfBadEvents++;
        	  }
		  
		  if(rdump)
		    dumpData(fragment, rol);

		  if (ddd)
	            std::cout << "masked status = " << std::hex << (fragment->status() & 0xf0000000) << std::dec << std::endl;

        	  if((fragment->status() & 0xf0000000) == 0x90000000)
		  {
		    if (ddd)
	              std::cout << "Fragment lost" << std::endl;
	            datal[rol]++;
		  }
		  else
        	  {
		    if (ddd)
	              std::cout << "Fragment OK" << std::endl;
	            datay[rol]++;
        	  }

        	  delete fragment;

        	  if (ddd)
        	     std::cout << "Event with L1ID " << tickets[rol][nextticket[rol]][1] << " received from ROL "  << rol << std::endl;

		  if (rol == 0)
        	    idgroup.push_back(tickets[rol][nextticket[rol]][1]);
		  else if (rol == 1)
		    idgroup2.push_back(tickets[rol][nextticket[rol]][1]);
		  else 
		    idgroup3.push_back(tickets[rol][nextticket[rol]][1]);

		  isempty = 0;
        	}
        	else  //Requeue the L1ID 
		{
        	  tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
        	  nextticket[rol]++;
        	  if (nextticket[rol] == 4)
        	    nextticket[rol] = 0;
        	  /////if(datay[rol])
		  datan[rol]++;
        	  rol++;  //try another ROL
		  if (rol == nrols)
	            rol = 0;
        	}
              }  

              //Queue a new L1ID
	      tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(nextl1id[rol]);
              tickets[rol][nextticket[rol]][1] = nextl1id[rol];
              nextl1id[rol]++;
              nextticket[rol]++;
              if (nextticket[rol] == 4)
        	nextticket[rol] = 0;

              //Send a delete request if we have received 100 events
              if (((datay[rol] + datal[rol]) % 100) == 0)
              {
        	if (ddd)
        	  std::cout << "100 events released on ROL " << rol << std::endl;
		if (rol == 0)
		{
        	  (*channels)[rol]->releaseFragment(&idgroup);  
        	  idgroup.clear();
		}
		else if (rol == 1)
        	{
        	  (*channels)[rol]->releaseFragment(&idgroup2);  
        	  idgroup2.clear();
		}	
		else
        	{
        	  (*channels)[rol]->releaseFragment(&idgroup3);  
        	  idgroup3.clear();
		}		    

		ndel[rol]++;
              } 
              if (nevents[rol])
		nevents[rol]--;
              rol++;         //try another ROL
	      if (rol == nrols)
		rol = 0;
	    }

	    ts_clock(&ts2);
	    delta1 = 1000000.0 * ts_duration(ts1, ts2);

            for (looprol = 0; looprol < nrols; looprol++)
	    {
	      std::cout << "ROL " << looprol << " : Number of getFragment without data = " << datan[looprol] << std::endl;
	      std::cout << "ROL " << looprol << " : Number of getFragment with data    = " << datay[looprol] << std::endl;
	      std::cout << "ROL " << looprol << " : Number of lost events              = " << datal[looprol] << std::endl;
	    }
	    std::cout << "Total time     =  " << delta1 << " us" << std::endl;
	    delta2 = delta1 / (nevents2[0] + nevents2[1]);
	    std::cout << "Time per event =  " << delta2 << " us" << std::endl;

	    if (cdata)
              std::cout << "Number of events where data check failed = " << numberOfBadEvents << std::endl;

	    if (!testmode)
	      ReadoutModule[0]->stopGathering(transCmd);
	    ReadoutModule[0]->unconfigure(transCmd);
	    delete ReadoutModule[0];
	    ReadoutModule[0] = 0;
	  }
            break;

	  case TIMING7: 
	  {
	    tstamp ts1, ts2;
	    float delta;
	    int todo, last;
	    u_int nrols, ddd = 0;
	    static int rol1 = 1, rol2 = 1, rol3 = 1, first = 1, total = 1000, gsize = 100;

            nrols = getparams(configuration, 0);
	    if (ReadoutModule[0] != 0)
	    {
              std::cout << "Array element 0 has already been filled"  << std::endl;
              return 1;
	    }
	    ReadoutModule[0] = new RobinReadoutModule();
	    ReadoutModule[0]->setup(configuration);
            ReadoutModule[0]->configure(transCmd);
	    ReadoutModule[0]->prepareForRun(transCmd);
	    channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());

	    std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
	    ddd = getdecd(ddd);     

	    std::cout << "Enter the L1ID of the first event to be released" << std::endl;
	    first = getdecd(first);
	    std::cout << "How many events do you want to release" << std::endl;
	    total = getdecd(total);
	    std::cout << "Enter the release group size (max 100)" << std::endl;
	    gsize = getdecd(gsize);

	    if (nrols > 0)
	    {
	      std::cout << "Delete events from ROL 0 (1=yes  0=no)" << std::endl;
	      rol1 = getdecd(rol1);
	    }
	    else
	      rol1 = 0;

	    if (nrols > 1)
	    {	  
	      std::cout << "Delete events from ROL 1 (1=yes  0=no)" << std::endl;
	      rol2 = getdecd(rol2);
	    }
	    else
	      rol2 = 0;

	    if (nrols > 2)
	    {	  
	      std::cout << "Delete events from ROL 2 (1=yes  0=no)" << std::endl;
	      rol3 = getdecd(rol3);
	    }
	    else
	      rol3 = 0;

	    ts_clock(&ts1);

	    last = first + total - 1;
	    while (first < last)
	    {
              if ((first + gsize) < last)
        	todo = gsize;
              else
        	todo = last - first + 1;

              if (ddd)
        	std::cout << "Deleting group of " << todo << " events with IDs " << first << " to " << first + todo - 1 << std::endl;

              idgroup.clear();
              for(int loop = 0; loop < todo; loop++)
        	idgroup.push_back(first++);

              if (rol1) (*channels)[0]->releaseFragment(&idgroup);
              if (rol2) (*channels)[1]->releaseFragment(&idgroup);
              if (rol3) (*channels)[2]->releaseFragment(&idgroup);
	    }

	    ts_clock(&ts2);

	    delta = ts_duration(ts1, ts2);
	    std::cout << "Total time for " << total << " events = " << delta * 1000000 << " us" << std::endl;
	    int norols = 0;
	    if (rol1) norols++;
	    if (rol2) norols++;
	    if (rol3) norols++;
	    std::cout << "Time for one event (fragment) from one ROL = " << delta * 1000000 / total / norols << " us" << std::endl;

	    ReadoutModule[0]->stopGathering(transCmd);
	    ReadoutModule[0]->unconfigure(transCmd);
	    delete ReadoutModule[0];
	    ReadoutModule[0] = 0;
	  }
	    break;

	  case TIMING8: 
	  {
	    u_int nevents[3], nevents2[3], nextticket[3], nextl1id[3], tickets[3][4][2];
	    u_int isempty, loop, looprol;
	    u_int rol, datal[3] = {0, 0, 0}, datay[3] = {0, 0, 0}, datan[3] = {0, 0, 0}, ndel[3] = {0, 0, 0};
	    u_int robin_number, robin_max, nrols = 3;
	    ROBFragment *fragment;
	    tstamp ts1, ts2;
	    float delta1[5], delta2[5];

            TS_PAUSE(TS_H2);

            std::cout << "Enter the number of Robins in the PC" << std::endl;
            robin_max = getdecd(4);  

            for (robin_number = 0; robin_number < robin_max; robin_number ++)
            {
              getparams2(configuration, robin_number);
	      if (ReadoutModule[0] != 0)
	      {
        	std::cout << "Array element 0 has already been filled"  << std::endl;
        	return 1;
	      }
	      ReadoutModule[0] = new RobinReadoutModule();
	      ReadoutModule[0]->setup(configuration);
	      ReadoutModule[0]->configure(transCmd);
	      ReadoutModule[0]->prepareForRun(transCmd);
	      nevents[0] = 200000;     
	      nevents[1] = nevents[0];
	      nevents[2] = nevents[0];
	      nevents2[0] = nevents[0];
	      nevents2[1] = nevents[1];
	      nevents2[2] = nevents[2];
	      nextl1id[0] = 1;
	      nextl1id[1] = 1;
	      nextl1id[2] = 1;
	      getstat = 0;
	      channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[0]->channels());
              TS_RESUME(TS_H2);
	      std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

	      ts_clock(&ts1);
	      //Fill the message Q
	      for (looprol = 0; looprol < nrols; looprol++)
	      {
		for (loop = 0; loop < 4; loop++)
		{
        	  tickets[looprol][loop][0] = (*channels)[looprol]->requestFragment(nextl1id[looprol]);
        	  tickets[looprol][loop][1] = nextl1id[looprol];
        	  nextl1id[looprol]++;
		}
		nextticket[looprol] = 0;
              }
	      idgroup.clear();
	      idgroup2.clear();
	      idgroup3.clear();
	      rol = 0;

	      while((nevents[0] != 0) || (nevents[1] != 0) || (nevents[2] != 0))
	      {
        	isempty = 1;       
        	while(isempty)  //Wait for a complete ROB fragment
        	{
		  if (getstat)
		  {
		    getstat = 0;
		    for (looprol = 0; looprol < nrols; looprol++)
		    {
		      std::cout << "ROL " << looprol << ":" << std::endl;
        	      std::cout << "Number of complete events          = " << datay[looprol] << std::endl;
		      std::cout << "Number of missing events           = " << datan[looprol] << std::endl;
        	      std::cout << "Number of lost events              = " << datal[looprol] << std::endl;
        	      std::cout << "Number of events to be requested   = " << nevents[looprol] << std::endl;
        	      std::cout << "Number of release(100) groups sent = " << ndel[looprol] << std::endl;
		      std::cout << "Currently waiting for L1IDs " << tickets[looprol][0][1] << ", " << tickets[looprol][1][1] << ", " << tickets[looprol][2][1] << " and " << tickets[looprol][3][1] << std::endl;
		    }
		  }  

        	  while(nevents[rol] == 0)
		  {
        	    rol++;                 //try another ROL
		    if (rol == nrols)
	              rol = 0;
        	  }

        	  fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));

        	  if (fragment) 
		  { // good fragment (with data)
        	    if((fragment->status() & 0xf0000000) == 0x90000000)
	              datal[rol]++;
		    else
	              datay[rol]++;

        	    delete fragment;

		    if (rol == 0)
        	      idgroup.push_back(tickets[rol][nextticket[rol]][1]);
		    else if (rol == 1)
		      idgroup2.push_back(tickets[rol][nextticket[rol]][1]);
		    else 
		      idgroup3.push_back(tickets[rol][nextticket[rol]][1]);

		    isempty = 0;
        	  }
        	  else  //Requeue the L1ID 
		  {
        	    tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
        	    nextticket[rol]++;
        	    if (nextticket[rol] == 4)
        	      nextticket[rol] = 0;
		    datan[rol]++;
        	    rol++;  //try another ROL
		    if (rol == nrols)
	              rol = 0;
        	  }
        	}  

        	//Queue a new L1ID
		tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(nextl1id[rol]);
        	tickets[rol][nextticket[rol]][1] = nextl1id[rol];
        	nextl1id[rol]++;
        	nextticket[rol]++;
        	if (nextticket[rol] == 4)
        	  nextticket[rol] = 0;

        	//Send a delete request if we have received 100 events
        	if (((datay[rol] + datal[rol]) % 100) == 0)
        	{
		  if (rol == 0)
		  {
        	    (*channels)[rol]->releaseFragment(&idgroup);  
        	    idgroup.clear();
		  }
		  else if (rol == 1)
        	  {
        	    (*channels)[rol]->releaseFragment(&idgroup2);  
        	    idgroup2.clear();
		  }	
		  else
        	  {
        	    (*channels)[rol]->releaseFragment(&idgroup3);  
        	    idgroup3.clear();
		  }		    

		  ndel[rol]++;
        	} 
        	if (nevents[rol])
		  nevents[rol]--;
        	rol++;         //try another ROL
		if (rol == nrols)
		  rol = 0;
	      }

	      ts_clock(&ts2);
	      delta1[robin_number] = 1000000.0 * ts_duration(ts1, ts2);

              for (looprol = 0; looprol < nrols; looprol++)
	      {
		if (datan[looprol] > 0 || datal[looprol] > 0)
		{
	          std::cout << "ROL " << looprol << " : Number of getFragment without data = " << datan[looprol] << std::endl;
	          std::cout << "ROL " << looprol << " : Number of getFragment with data    = " << datay[looprol] << std::endl;
	          std::cout << "ROL " << looprol << " : Number of lost events              = " << datal[looprol] << std::endl;
		}
	      }
	      delta2[robin_number] = delta1[robin_number] / (nevents2[0] + nevents2[1]);

	      if (!testmode)
		ReadoutModule[0]->stopGathering(transCmd);
	      ReadoutModule[0]->unconfigure(transCmd);
	      delete ReadoutModule[0];
	      ReadoutModule[0] = 0;
	    }
	    
	    for (robin_number = 0; robin_number < robin_max; robin_number ++)
	    {
	      std::cout << "Robin #" << robin_number << ": Total time     =  " << delta1[robin_number] << " us" << std::endl;
	      std::cout << "Robin #" << robin_number << ": Time per event =  " << delta2[robin_number] << " us" << std::endl;
            }    
	  }
          break;
	    
	  case DUMPMANY:
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
              std::cout << "This array element has not yet been filled"  << std::endl;
              break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2) 
	    {
	      std::cout << "Wrong channel number." << std::endl;
	      break;
	    }

	    std::cout << "How many events do you want to request" << std::endl;
	    count = getdecd(100);

	    std::cout << "Enter the L1ID to start with" << std::endl;
	    il1id = getdecd(0);

	    channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());

	    for (l1id = il1id; l1id < (count + il1id); l1id++) 
	    {
	      std::cout << "Requesting LVL1 ID: " << l1id << std::endl;
	      value = (*channels)[dataChannel]->requestFragment(l1id); 
	      robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(value)); 
	      std::cout << "Dumping ROB fragment" << std::endl;
              std::cout << "ROB fragment pointer = " << std::hex << robfragment << std::dec << std::endl;
              robfragment->print();
	      delete robfragment;
	    }
	    break;

	  case GETINFO: 
	  {
	    std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
	    num = getdecd(0);

	    if (ReadoutModule[num] == 0)
	    {
	      std::cout << "This array element has not yet been filled"  << std::endl;
	      break;
	    }

	    std::cout << "Enter ROL (0, 1 or 2)" << std::endl;
	    dataChannel = getdecd(0);
	    if (dataChannel > 2) 
	    {
	      std::cout << "Wrong channel number." << std::endl;
	      break;
	    }

	    channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[num]->channels());

	    DFCountedPointer<Config> info;
	    info = (*channels)[dataChannel]->getInfo(); 

	    std::cout << "RobIn reply:" << std::endl;
	    std::cout << "--------------------------------------------------" << std::endl;
	    info->dump();
	    std::cout << "--------------------------------------------------" << std::endl;
	  }
            break;

	  case SETCONFIG: 
	    std::cout << "Not yet available" << std::endl;
	    break;

	  case RESET: 
	    std::cout << "Not yet available" << std::endl;
	    break;

	  case SETDEBUG:
	    setdebug();
	    break;

	  case HELP:
	    std::cout <<  " Exerciser program for the BusBasedRobinReadoutModule Class." << std::endl;
	    std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
	    std::cout <<  " the basic BusBasedRobinReadoutModule methods: this is a bit for specialists ..." << std::endl;
	    break;

	  case QUIT:
            quit_flag = 1;
            break;

	  default:
            std::cout <<  "not implemented yet" << std::endl;
	  } //main switch
      } while (quit_flag == 0);
    }
  }
  catch(std::exception &ex) 
  {
    std::cout << ex.what() << std::endl;
  }

  TS_SAVE(TS_H2, "Robin_timing");
  TS_CLOSE(TS_H2);
  ts_close(TS_DUMMY);
  return 0;
}


/***************************************************/
int getnext(float percent, float limit, int nextl1id)
/***************************************************/
{
  while (limit < 100.0)
  {
    limit += percent;
    nextl1id++;
  }
  limit -= 100.0;
  return nextl1id;
}


/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}



/*******************************************/
void dumpData(ROBFragment *fragment, int rol)
/*******************************************/
{
  u_int i, fsize;
  Buffer *fragBuffer = fragment->buffer();
  MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
  u_int *fragBufferAddress = (u_int *)memPage->address();

  fsize = (u_int)fragment->size();       //Total fragment size
    
  std::cout << "ROL " << rol << ": Fragment size = " << fsize << std::endl;
  for (i = 0; i < fsize; i++)
  {
    if((i % 8) == 0) 
      std::cout << std::hex << "0x" << std::setw(3) << i << std::dec << ": ";

    std::cout << std::hex << " 0x" << std::setw(8) << fragBufferAddress[i] << std::dec;

    if((i % 8) == 7)
      std::cout << std::endl; 
   }
   std::cout << std::endl; 
  
/*
  if (rdump)
  {
   std::cout << std::endl;
   std::cout << "ROL " << rol << ": Fragment size = " << fsize << std::endl;
   std::cout << "dstart = " << dstart << std::endl;
   std::cout << "sstart = " << sstart << std::endl;
   std::cout << "tstart = " << tstart << std::endl;
   std::cout << "dsize = " << dsize << std::endl;   
   for (i = 0; i < fsize; i++)
   {
     std::cout << "word(" << i << "): 0x" << std::hex << fragBufferAddress[i] << std::dec; 
     if (i == 0) std::cout << "  ROB: header marker" << std::endl;
     else if (i == 1) std::cout << "  ROB: total fragment size" << std::endl;
     else if (i == 2) std::cout << "  ROB: header size" << std::endl;
     else if (i == 3) std::cout << "  ROB: format version number" << std::endl;
     else if (i == 4) std::cout << "  ROB: source identifier" << std::endl;
     else if (i == 5) std::cout << "  ROB: number of status elements" << std::endl;
     else if (i == 6) std::cout << "  ROB: status element 1" << std::endl;
     else if (i == 7) std::cout << "  ROB: status element 2 (most recent ID; do not check)" << std::endl;
     else if (i == 8) std::cout << "  ROB: CRC type" << std::endl;
     else if (i == 9) std::cout << "  ROD: header marker" << std::endl;
     else if (i == 10) std::cout << "  ROD: header size" << std::endl;
     else if (i == 11) std::cout << "  ROD: format version number" << std::endl;
     else if (i == 12) std::cout << "  ROD: source identifier" << std::endl;
     else if (i == 13) std::cout << "  ROD: run number" << std::endl;
     else if (i == 14) std::cout << "  ROD: Level 1 ID" << std::endl;
     else if (i == 15) std::cout << "  ROD: Bunch crossing ID" << std::endl;
     else if (i == 16) std::cout << "  ROD: Level 1 trigger type" << std::endl;
     else if (i == 17) std::cout << "  ROD: Detector event type" << std::endl;
     else if (i == (fsize - 4)) std::cout << "  ROD: number of status elements" << std::endl;
     else if (i == (fsize - 3)) std::cout << "  ROD: number of data elements" << std::endl;
     else if (i == (fsize - 2)) std::cout << "  ROD: status block position (follows data)" << std::endl;
     else if (i == (fsize - 1)) 
     {
       std::cout << "  ROB: CRC word" << std::endl;
       std::cout << "Calculated CRC: 0x" << std::hex << calculatedCRC << std::dec << std::endl;
     }
     else std::cout << std::endl; 
    }
  }
*/
  
  return;
}


/*************************************************************/
int checkData(ROBFragment *fragment, int id, int rol, int mode)
/*************************************************************/
{
  static u_int evok = 0, evlim = 1;
  int notok = 0;
  Buffer *fragBuffer = fragment->buffer();
  MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
  u_int ref_data, ui, *fragBufferAddress = (u_int *)memPage->address();
  u_int i, ssize, fsize, dsize, hsize, tsize, dstart, sstart, tstart;
  u_int headerData[25];
  u_int headerDataGen[25];
  u_int trailerData[4];
  uint32_t crc1, crc2, calculatedCRC;

  if (mode < 1)
    return(0);   //Mode 0 = no check!

  //We need some constants....
  fsize = (u_int)fragment->size();       //Total fragment size
  hsize = 18;                            //ROD header + ROB header size
  tsize = 4;                             //ROB trailer + ROD trailer size
  ssize = 1;                             //Number of status words in the ROD fragment
  dsize = fsize - hsize - tsize - ssize; //ROD data size
  dstart = hsize;
  sstart = fsize - (tsize + ssize);      //status elements follow data
  tstart = fsize - tsize;
  
  std::cout << "Total fragmet size = " << fsize << std::endl;
  std::cout << "ROD data size      = " << dsize << std::endl;

  headerData[0]  = 0xdd1234dd;                    //ROB: header marker
  headerData[1]  = fsize;                         //ROB: total fragment size
  headerData[2]  = 0x00000009;                    //ROB: header size
  headerData[3]  = 0x04000000;                    //ROB: format version number
  headerData[4]  = 0x510000 + rol;                //ROB: source identifier
  headerData[5]  = 0x00000002;                    //ROB: number of status elements
  headerData[6]  = 0x00000000;                    //ROB: status element 1
  headerData[7]  = 0x00000000;                    //ROB: status element 2 (most recent L1ID; do not check)
  headerData[8]  = 0x00000001;                    //ROB: check sum type
  headerData[9]  = 0xee1234ee;                    //ROD: header marker
  headerData[10]  = 0x00000009;                   //ROD: header size
  headerData[11]  = 0x03010000;                   //ROD: format version number
  headerData[12]  = 0x00a00000;                   //ROD: source identifier
  headerData[13]  = 0x00000001;                   //ROD: run number
  headerData[14]  = id;                           //ROD: Level 1 ID
  headerData[15]  = 3 * id;                       //ROD: Bunch crossing ID
  headerData[16]  = 0x00000007;                   //ROD: Level 1 trigger type
  headerData[17]  = 0x0000000a;                   //ROD: Detector event type
  
  headerDataGen[0]  = 0xdd1234dd;                 //ROB: header marker
  headerDataGen[1]  = fsize;                      //ROB: total fragment size
  headerDataGen[2]  = 0x00000009;                 //ROB: header size
  headerDataGen[3]  = 0x04000000;                 //ROB: format version number
  headerDataGen[4]  = 0x510000 + rol;             //ROB: source identifier
  headerDataGen[5]  = 0x00000002;                 //ROB: number of status elements
  headerDataGen[6]  = 0x00000000;                 //ROB: status element 1
  headerDataGen[7]  = 0x00000000;                 //ROB: status element 2 (most recent L1ID; do not check)
  headerDataGen[8]  = 0x00000001;                 //ROB: check sum type
  headerDataGen[9]  = 0xee1234ee;                 //ROD: header marker
  headerDataGen[10]  = 0x00000009;                //ROD: header size
  headerDataGen[11]  = 0x03010000;                //ROD: format version number
  headerDataGen[12]  = 0xabcdacbd;                //ROD: source identifier
  headerDataGen[13]  = 0x00000000;                //ROD: run number
  headerDataGen[14]  = id;                        //ROD: Level 1 ID
  headerDataGen[15]  = 0x11111111;                //ROD: Bunch crossing ID
  headerDataGen[16]  = 0xb0b0b0b0;                //ROD: Level 1 trigger type
  headerDataGen[17]  = 0x0d0d0d0d;                //ROD: Detector event type

  trailerData[0] = 0x00000001;                    //ROD: number of status elements
  trailerData[1] = dsize;                         //ROD: number of data elements
  trailerData[2] = 0x00000001;                    //ROD: status block position (follows data)
  trailerData[3] = 0x00000000;                    //ROB: CRC value (do not check)
 
  // start CRC calculation
  crc1 = 0xffff;
  crc2 = 0xffff;
  
  //Header
  for (i = 0; i < hsize; i++)
  {
    if ((fragBufferAddress[i] != headerData[i]) && (fragBufferAddress[i] != headerDataGen[i]) && i != 7)
    {
      std::cout << "Header " << i << ": 0x" << std::hex << fragBufferAddress[i] << " [expected 0x" << headerData[i];
      if (headerData[i] != headerDataGen[i])   
        std::cout << " or 0x" << headerDataGen[i];
      std::cout << "]" << std::dec << std::endl;
      notok = 1;
      if (mode == 2)  
      {
        std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; 
        return(notok);
      }
    }
    if (i > 8)
    {
      // ROD header, need to be taken into account for CRC calculation
      crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[i] >> 16)];
      crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[i] & 0xffff)];
    }
  }

  //Data
  ref_data = fragBufferAddress[dstart] + 1;
  crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[dstart] >> 16)];
  crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[dstart] & 0xffff)];
  for (ui = 1; ui < dsize; ui++)
  {
    if (fragBufferAddress[ui + dstart] != ref_data)
    {
      std::cout << "Data " << ui + dstart << ": 0x" << std::hex << fragBufferAddress[ui + dstart] << " [expected 0x" << ref_data << " ] " << std::dec << std::endl;
      notok = 1;
      if (mode == 2)  
      {
        std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; 
	return(notok);
      }
    }
    crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[ui + dstart] >> 16)];
    crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[ui + dstart] & 0xffff)];
    ref_data = fragBufferAddress[ui + dstart] + 1;
  }
  
  //Status
  if (fragBufferAddress[sstart] != 0)
  {
    std::cout << "Status " << sstart << ": 0x" << std::hex << fragBufferAddress[sstart] << " [expected 0x0 ] " << std::dec << std::endl;
    notok = 1;
    if (mode == 2)  
    {
      std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; 
      return(notok);
    }
  }
  crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[sstart] >> 16)];
  crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[sstart] & 0xffff)];

  //Trailer
  for (i = 0; i < tsize - 1; i++)  //Do not check the CRC value in the last word of the trailer
  {
    if (fragBufferAddress[i + tstart] != trailerData[i])
    {
      std::cout << "Trailer " << i + tstart << ": 0x" << std::hex << fragBufferAddress[i + tstart] << " [expected 0x" << trailerData[i] << " ] " << std::dec << std::endl;
      notok = 1;
      if (mode == 2)  
      {
        std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; 
	return(notok);
      }
    }
    crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[i + tstart] >> 16)];
    crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[i + tstart] & 0xffff)];
  }  
          
  if(notok)	  
    std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; 

  crc1 &= 0xffff;
  crc2 &= 0xffff;
  calculatedCRC = (crc1 << 16) + crc2;
  if (fragBufferAddress[fsize - 1] != calculatedCRC) 
    notok = 1;
  
  if (calculatedCRC != fragBufferAddress[fsize - 1])
  {
    std::cout << "ERROR: The CRC does not match!" << std::endl; 
    std::cout << "ROB trailer CRC = 0x" << std::hex << fragBufferAddress[fsize - 1] << ". Calculated CRC = 0x" << calculatedCRC << std::dec << std::endl;
  }
  
  if (!notok)
  {
    evok++;
    if (!(evok % evlim))
      std::cout << evok << " events OK. Last L1ID was " << id << std::endl;
    if (evok == (10 * evlim))
      evlim = evok; 
  }
  
  return notok;
}


/***************************************************************/
int getparams(DFCountedPointer<Config> configuration, u_int mode)
/***************************************************************/
{
  // Definitions:
  // PhysicalAddress        = The PCI slot number of the Robin card
  // timeout                = The message time-out in seconds
  // numberOfChannels       = The number of ROLs on one Robin card
  // memoryPoolNumPages     = The number of memory pages for ROB fragments
  // memoryPoolPageSize     = The size of a memory page for ROB fragments
  // numberOfOutstandingReq = The number of entries to be reserved in the message FIFO of a Robin card
  // msgInputMemorySize     = The amount of message DPM to be reserved in one Robin card
  // miscSize               = The amount of PC memory to be reserved for misc. messages
  // ChannelId              = The logical ID of a ROL
  // ROLPhysicalAddress     = The physical number of a ROL on a Robin card
  // cfgParms[l_index].name = The Robin coonfiguration parameters
  
  u_int yesno, value, rolva, noChannels, p1, p2, p3;

  std::cout << "Enter the value for <PhysicalAddress> (this is the PCI logical board number: 0 .. n)" << std::endl;
  value = getdecd(0);  
  configuration->set("PhysicalAddress", value); 
  
  std::cout << "Enter the value for <EbistEnabled>" << std::endl;
  value = getdecd(0);  
  configuration->set("EbistEnabled", value); 

  std::cout << "Enter the value for <RevisionNumber> " << std::endl;
  value = gethexd(0xa0018);  
  configuration->set("RevisionNumber", value); 

  std::cout << "Enter the value for <FPGAVersion> " << std::endl;
  value = gethexd(0x05033001);  
  configuration->set("FPGAVersion", value);
  
  std::cout << "Enter the value for <Timeout> (in seconds) " << std::endl;
  value = getdecd(100);  
  configuration->set("Timeout", value);

  std::cout << "Enter the value for <ResetRobin> (1=yes  0=no) " << std::endl;
  value = getdecd(0);  
  configuration->set("ResetRobin", value);

  if (mode == 0)
  {
    std::cout << "Enter the value for <numberOfChannels> " << std::endl;
    noChannels = getdecd(1);  
    configuration->set("numberOfChannels", noChannels);
  }
  else
  {
    configuration->set("numberOfChannels", 1);
    noChannels = 1;  
  }

  std::cout << "Enter the value for <NumberOfOutstandingReq>" << std::endl;
  value = getdecd(20);    
  configuration->set("NumberOfOutstandingReq", value);

  /*
    Some of the benchmarking functions do not run if there is insufficient DPM memory
    These functions are designed such that they generate a certain number of outstanding 
    requests for fragments. These messages block parts of the available DPM space. At one moment 
    the S/W will try to release a group of events. The message with the L1IDs is large 
    (~400 bytes) and the release function is blocking (i.e. it will not return if it cannot send 
    the message due to a lack of DPM memory). If the release function fails to get 
    DPM memory because one of the outstanding fragment requests is in the way the release function 
    cannot do its job and blocks the whole process.   
  */

  std::cout << "Enter the value for <MsgInputMemorySize> in bytes" << std::endl;
  value = gethexd(0x2000);    
  configuration->set("MsgInputMemorySize", value);

  std::cout << "Enter the value for <MiscSize> in bytes" << std::endl;
  value = gethexd(0x2000);    
  configuration->set("MiscSize", value);

  std::cout << "Enter the value for <Interactive> (1=yes  0=no) " << std::endl;
  value = getdecd(0);    
  configuration->set("Interactive", value);

  std::cout << "Enter the value for <Prescalefrag> " << std::endl;
  value = getdecd(3); 
  configuration->set("Prescalefrag", value);

  std::cout << "Enter the value for <SubDetectorId> " << std::endl;
  value = gethexd(0x51);
  configuration->set("SubDetectorId", value);

  std::cout << "Enter the value for <Pagesize>" << std::endl;
  p2 = getdecd(512);    
  configuration->set("Pagesize", p2);

  std::cout << "Enter the value for <Numpages>" << std::endl;
  value = getdecd(32768);    
  configuration->set("Numpages", value);

  std::cout << "Enter the value for <MaxRxPages>" << std::endl;
  p1 = getdecd(2);    
  configuration->set("MaxRxPages", p1);

  configuration->set("triggerQueue", 0);

  for (u_int loop = 0; loop < noChannels; loop++)
  {
    std::cout << "Enter the value for <ChannelId#" << loop << "> " << std::endl;
    rolva = getdecd(loop);    
    configuration->set("ChannelId", rolva, loop);

    std::cout << "Enter the value for <ROLPhysicalAddress#" << loop << "> " << std::endl;
    value = getdecd(loop);
    configuration->set("ROLPhysicalAddress", value, loop);
      
    std::cout << "Enter the value for <memoryPoolNumPages#" << loop << ">" << std::endl;
    value = getdecd(10);    
    configuration->set("memoryPoolNumPages", value, loop);

    std::cout << "Enter the value for <memoryPoolPageSize#" << loop << "> in bytes" << std::endl;
    p3 = getdecd(8192);    
    configuration->set("memoryPoolPageSize", p3, loop);

    if ((p2 * 4 * p1) > p3)
    {
      std::cout << "With the parameters you have selected the ROBIN can send fragments of up to " << p2 * 4 * p1 << " bytes" << std::endl;
      std::cout << "But the local buffer manager can only handle fragments of at most " << p3 << " bytes" << std::endl;
      std::cout << "Don't say you have not be warned" << std::endl;
    }

    std::cout << "Do you want to enter more ROL specific ROBIN configuration parameters (1=yes  0=no)" << std::endl;
    yesno = getdecd(0);    

    configuration->set("CrcCheckInterval", 100, loop);
    configuration->set("UID", "my_rol", loop);
    configuration->set("ForceL1ID", 0, loop);

    if (yesno)
    {  
      std::cout << "Enter the value for <RolDataGen#" << loop << ">" << std::endl;
      value = getdecd(0);    
      configuration->set("RolDataGen", value, loop);  

      std::cout << "Enter the value for <Rolemu#" << loop << ">" << std::endl;
      value = getdecd(0);        
      configuration->set("Rolemu", value, loop);
  
      std::cout << "Enter the value for <TestSize#" << loop << ">" << std::endl;
      value = getdecd(0);       
      configuration->set("TestSize", value, loop);
    
      std::cout << "Enter the value for <Keepfrags#" << loop << ">" << std::endl;
      value = getdecd(0);       
      configuration->set("Keepfrags", value, loop);  
    
      std::cout << "Enter the value for <ForceL1ID#" << loop << ">" << std::endl;
      value = getdecd(0);       
      configuration->set("ForceL1ID", value, loop);
    
      std::cout << "Enter the value for <CrcCheckInterval#" << loop << ">" << std::endl;
      value = getdecd(100);       
      configuration->set("CrcCheckInterval", value, loop);   
    }
  }
  
  return(noChannels);
}


/********************************************************************/
int getparams2(DFCountedPointer<Config> configuration, u_int robin_nr)
/********************************************************************/
{
  configuration->set("PhysicalAddress", robin_nr); 
  configuration->set("EbistEnabled", 0); 
  configuration->set("RevisionNumber", 0xa0018); 
  configuration->set("FPGAVersion", 0x05033001);
  configuration->set("Timeout", 100);
  configuration->set("ResetRobin", 0);
  configuration->set("numberOfChannels", 3);
  configuration->set("NumberOfOutstandingReq", 20);
  configuration->set("MsgInputMemorySize", 0x2000);
  configuration->set("MiscSize", 0x2000);
  configuration->set("Interactive", 0);
  configuration->set("Prescalefrag", 1);
  configuration->set("SubDetectorId", 0x51);
  configuration->set("Pagesize", 512);
  configuration->set("Numpages", 32768);
  configuration->set("MaxRxPages", 2);
  configuration->set("triggerQueue", 0);

  for (u_int loop = 0; loop < 3; loop++)
  {
    configuration->set("ChannelId", loop, loop);
    configuration->set("ROLPhysicalAddress", loop, loop);
    configuration->set("memoryPoolNumPages", 10, loop);
    configuration->set("memoryPoolPageSize", 8192, loop);
    configuration->set("CrcCheckInterval", 100, loop);
    configuration->set("UID", "my_rol", loop);
    configuration->set("ForceL1ID", 0, loop);
    configuration->set("RolDataGen", 1, loop);  
    configuration->set("Rolemu", 0, loop);
    configuration->set("TestSize", 200, loop);
    configuration->set("Keepfrags", 0, loop);  
    configuration->set("ForceL1ID", 0, loop);
    configuration->set("CrcCheckInterval", 100, loop);   
  }
  
  return(3);
}
