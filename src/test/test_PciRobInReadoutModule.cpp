/****************************************************************/
/*								*/
/*  file test_emulated_fm.cpp					*/
/*								*/
/*  exerciser for the PciRobinReadoutModule class		*/
/*								*/
/*  The program quickly test PciRobin classes            	*/
/* It is not intended for performance measuerments              */
/*								*/
/*								*/
/***C 2004 ******************************************************/

#include <iostream.h>
#include <iomanip.h>
#include <vector>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModules/PciRobinReadoutModule.h"
#include "ROSModules/DataChannel.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFSubSystemItem/Config.h"

#include "DFDebug/GlobalDebugSettings.h"

using namespace ROS;

#define FM_POOL_NUMB     10      //Max. number of fragment managers

/************/
int main(void)
/************/ 
{

  try {
    
    std::cout << "-------------------------------" << std::endl;
    std::cout << " Pci ROBIN readout module test " << std::endl;
    std::cout << "                  mmueller 2004" << std::endl;
    std::cout << "-------------------------------" << std::endl;
    
    std::cout << "Creating a readout module" << std::endl;
    ReadoutModule* readoutModule = new PciRobinReadoutModule();
    std::cout << "done" << std::endl;
    
    
    DFCountedPointer<Config> configuration = Config::New();
    
    configuration->set("memoryPoolNumPages", 400);
    configuration->set("memoryPoolPageSize", 4096);
    configuration->set("robInID", 0);
    
    std::cout << "Configure it with:" << std::endl;
    configuration->dump();
    
    DF::GlobalDebugSettings::setup(300,0);
    
    readoutModule->setup(configuration);
    std::cout << "done" << std::endl;
    
    std::vector<DataChannel *> *channels = readoutModule->channels();
    
    std::cout << "Request a fragment on channel 1" << std::endl;
    unsigned int ticket = (*channels)[0]->requestFragment(10);
    std::cout << "done" << std::endl;
    
    std::cout << "get a fragment on channel 1" << std::endl;
    ROBFragment *fragment = dynamic_cast<ROBFragment *> ((*channels)[0]->getFragment(ticket));
    std::cout << "done" << std::endl;
    
    fragment->print();
    
    unsigned int * fragmentAddress = (unsigned int *)fragment->buffer()->current();
    
    unsigned int i;
    for ( i = 0; i < fragment->size() ; i++) {
      std::cout << std::hex 
		<< i 
		<< " :  " 
		<< fragmentAddress[i] 
		<< std::dec
		<< std::endl;
      
    }
    
  
    delete fragment;
    
    std::cout << "Request a fragment on channel 2" << std::endl;
    ticket = (*channels)[1]->requestFragment(10);
    std::cout << "done" << std::endl;
    
    std::cout << "get a fragment on channel 2" << std::endl;
    fragment = dynamic_cast<ROBFragment *>((*channels)[1]->getFragment(ticket));
    std::cout << "done" << std::endl;
    
    fragment->print();
    
    fragmentAddress = (unsigned int *)fragment->buffer()->current();
    
    for ( i = 0; i < fragment->size() ; i++) {
      std::cout << std::hex 
		<< i 
		<< " :  " 
		<< fragmentAddress[i] 
		<< std::dec
		<< std::endl;
      
    }
    
    delete fragment;
    
    std::vector<unsigned int> deletedIds;
    for (i = 0; i < 50; i++)
      deletedIds.push_back(i);
    
    std::cout << "Deleting fragments 1..50 on channel 1" << std::endl;
    (*channels)[0]->releaseFragment(&deletedIds);
    std::cout << "done" << std::endl;
    
    std::cout << "Request a fragment on channel 1" << std::endl;
    ticket = (*channels)[0]->requestFragment(10);
    std::cout << "done" << std::endl;
    
    std::cout << "get a fragment on channel 1" << std::endl;
    fragment = dynamic_cast<ROBFragment *> ((*channels)[0]->getFragment(ticket));
    std::cout << "done" << std::endl;
    
    fragment->print();
    
    fragmentAddress = (unsigned int *)fragment->buffer()->current();
    
    for ( i = 0; i < fragment->size() ; i++) {
      std::cout << std::hex 
		<< i 
		<< " :  " 
		<< fragmentAddress[i] 
		<< std::dec
		<< std::endl;
      
    }
    
    
    DFCountedPointer < Config > infoCh0 = (*channels)[0]->getInfo();
    infoCh0->dump();
    
    std::cout << "Unconfigure it..." << std::endl;
    readoutModule->unconfigure();
    std::cout << "done" << std::endl;
    
    delete readoutModule;

  }
  catch(ROSException &err) {
    std::cout << "ROS Exception: " << std::endl;
    std::cout << "\t" << "Package: " << err.getPackage() << std::endl;
    std::cout << "\t" << "Error  : " << err.getErrorMessage() << std::endl;    
  }
  catch(exception &err) {
    std::cout << "STL Exception: " << err.what()<< std::endl;    
  }
  catch(...) {
    std::cout << "Unknown Exception occured!!!" << std::endl;
  }    
  return 0;
}
