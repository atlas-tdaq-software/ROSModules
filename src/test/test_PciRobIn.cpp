// $Id$
/****************************************************************/
/*								*/
/*  file test_PciRobIn						*/
/*								*/
/*  exerciser for the ROS Modules & Channels Class		*/
/*								*/
/*  The program allows to:					*/
/*  . exercise the methods of the ROS Modules & Channels Class	*/
/*  . measure the performance					*/
/*								*/
/*								*/
/***C 2002 - A nickel program worth a dime***********************/

#include <unistd.h>
#include <iostream.h>
#include <signal.h>
#include <iomanip.h>
#include <errno.h>
#include <exception>
#include <vector>
#include <string>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModules/PciRobinReadoutModule.h"
#include "ROSModules/DataChannel.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFSubSystemItem/Config.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"

#ifndef HOST
#define HOST
#endif
//#include "robin_ppc/robin.h"

using namespace ROS;

enum 
{ 
  TIMING4 = 1,
  TIMING5,
  TIMING6,
  TIMING7,
  CREATE,
  CONFIGURE,
  REQUEST,
  GET,
  RELEASEMANY,
  UNCONFIGURE,
  DELETE,
  TIMING,
  TIMING2,
  TIMING3,
  DUMPONE,
  DUMPMANY,
  GETINFO,
  SETCONFIG,
  RESET,  
  SETDEBUG,
  HELP,
  QUIT = 100
};

unsigned int trailerData[3] = {
  0x00000001,
  0,
  0
};

#define FM_POOL_NUMB     10      //Max. number of fragment managers

// Globals
int getstat;
unsigned int dblevel = 0, dbpackage = DFDB_ROSFM;

// Prototypes
int checkData(ROBFragment *fragment, int id, int rol, int rdump, int mode);
int getnext(float percent, float limit, int nextl1id);
void sigquit_handler(int signum);
int setdebug(void);

/******************************/
void sigquit_handler(int signum)
/******************************/
{
  getstat = 1;
}


/************/
int main(void)
/************/ 
{
  int count, num, value, l1id, option, quit_flag = 0;
  int noChannels;
  ReadoutModule* pciReadoutModule[FM_POOL_NUMB] = {0};
  ROBFragment* robfragment;
  std::vector<DataChannel *> *channels;
  vector <unsigned int> idgroup, idgroup2;
  DFCountedPointer<Config> configuration = Config::New();
  unsigned int *fragBufferAddress;
  struct sigaction sa;
  unsigned int dataChannel;

  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0)
  {
    std::cout << "main: sigaction() FAILED with ";
    int code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (-1);
  }

  try
  {
  do 
  {
  std::cout << std::endl << std::endl;
  std::cout << " Automatic tests:  " << dec << std::endl;
  std::cout << " Run synchronous benchmark (request all) : " << dec << TIMING4 << std::endl; 
  std::cout << " Run synchronous benchmark (request some): " << dec << TIMING5 << std::endl; 
  std::cout << " Run synchronous benchmark (2 ROLs)      : " << dec << TIMING6 << std::endl; 
  std::cout << " Run integrated release test (2 ROLs)    : " << dec << TIMING7 << std::endl;
  std::cout << "==============================================" << std::endl;
  std::cout << " Create a PCI ROBIN Readout Module       : " << dec << CREATE << std::endl; 
  std::cout << " Configure a PCI ROBIN Readout Module    : " << dec << CONFIGURE << std::endl;  
  std::cout << " Request a ROB fragment                  : " << dec << REQUEST << std::endl; 
  std::cout << " Get a ROB fragment                      : " << dec << GET << std::endl; 
  std::cout << " Release many ROB fragments              : " << dec << RELEASEMANY << std::endl; 
  std::cout << " Unconfigure a Readout Module            : " << dec << UNCONFIGURE << std::endl; 
  std::cout << " Delete a Readout Module                 : " << dec << DELETE << std::endl; 
  std::cout << " Run timing benchmarks                   : " << dec << TIMING << std::endl; 
  std::cout << " Run request Q benchmark                 : " << dec << TIMING2 << std::endl; 
  std::cout << " Run release benchmark                   : " << dec << TIMING3 << std::endl; 
  std::cout << " Request one Fragment and dump           : " << dec << DUMPONE << std::endl; 
  std::cout << " Request many Fragments and dump         : " << dec << DUMPMANY << std::endl; 
  std::cout << " Get ROBIN statistics                    : " << dec << GETINFO << std::endl;
  std::cout << " Reset ROBIN                             : " << dec << RESET << std::endl;
  std::cout << " Set a configuration variable            : " << dec << SETCONFIG << std::endl;
  std::cout << " Set debug parameters                    : " << dec << SETDEBUG << std::endl;
  std::cout << " HELP                                    : " << dec << HELP << std::endl;
  std::cout << " QUIT                                    : " << dec << QUIT << std::endl;
  std::cout << " ? : ";

  option = getdec();    
  switch(option)   
  {
    case CREATE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }
      pciReadoutModule[num] = new PciRobinReadoutModule();
      std::cout << "Readout Module created" << std::endl;
      break;
      
    case CONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     

      std::cout << "Enter RobIn ID (currently PCI logical board number: 0 .. n)" << std::endl;
      value = getdecd(0);
      configuration->set("robInID", value);     

// support for ONLY one at present 
      std::cout << "Enter Number of Channels " << std::endl;
      noChannels = getdecd(1);
      configuration->set("numberOfChannels", noChannels);

      std::cout << "Enter the value for <timeout>(seconds) " << std::endl;
      value = getdecd(10);
      configuration->set("timeout", value);
      
      std::cout << "Enter the value for <memoryPoolNumPages#0>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 0);
 
      std::cout << "Enter the value for <memoryPoolPageSize#0>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 0);     

      std::cout << "Enter the value for <channelId#0> " << std::endl;
      value = getdecd(12345);
      configuration->set("channelId", value, 0);

      std::cout << "Enter the value for <channelPhysicalAddress#0> " << std::endl;
      value = getdecd(0);
      if (value > 1 ) {
        std::cout << " wrong value for the Rol Physical address " << std::endl;
      }
      configuration->set("channelPhysicalAddress", value, 0);

      if ( noChannels > 1) {
      
        std::cout << "Enter the value for <memoryPoolNumPages#1>" << std::endl;
        value = getdecd(100);
        configuration->set("memoryPoolNumPages", value, 1);
 
        std::cout << "Enter the value for <memoryPoolPageSize#1>" << std::endl;
        value = getdecd(4096);
        configuration->set("memoryPoolPageSize", value, 1);     

        std::cout << "Enter the value for <channelId#1> " << std::endl;
        value = getdecd(12345);
        configuration->set("channelId", value, 1);

        std::cout << "Enter the value for <channelPhysicalAddress#1> " << std::endl;
        value = getdecd(0);
        if (value > 1 ) {
          std::cout << " wrong value for the Rol Physical address " << std::endl;
        }
        configuration->set("channelPhysicalAddress", value, 1);

      }

      pciReadoutModule[num]->setup(configuration);
      break;
      
    case REQUEST:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();

      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }

      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }
                 
      std::cout << "Enter the Level 1 ID" << std::endl;
      l1id = getdecd(1); 
         
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      value = (*channels)[dataChannel]->requestFragment(l1id); 
      std::cout << "Request returns the ticket " << value << std::endl;   
      break;
      
    case GET:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }

      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }
      
      std::cout << "Enter the ticket received from request()" << std::endl;
      l1id = getdecd(1); 
         
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(l1id)); 
      if (robfragment == 0) {	// may come or lost
        std::cout << " getFragment returns : " << robfragment 
                  << " i.e. fragment may come or is lost " << std::endl;
      }
      else {
        std::cout << "Dumping ROB fragment" << std::endl;
        std::cout << "ROB fragment pointer = " << std::hex << robfragment << std::endl;
        std::cout << *robfragment;
        delete robfragment;
      }
      break;
      
  case RELEASEMANY: {
      tstamp ts1, ts2;
      float delta;
      int i;
      
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();      
      
      if (pciReadoutModule[num] == 0)
	{
	  std::cout << "This array element has not yet been filled"  << std::endl;
	  break;
	}
      
      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      std::cout << "How many events do you want to release" << std::endl;
      value = getdecd(100);
      
      idgroup.clear();
      
      for(int loop = 1; loop <= value; loop++)
	idgroup.push_back(loop);    // L1ID is dummy
            
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      ts_open(1, TS_DUMMY);
      
      ts_clock(&ts1);
      
      for (i = 0; i < 100; i++) {
	
	(*channels)[dataChannel]->releaseFragment(&idgroup);  // L1ID is dummy      
      }
      
      ts_clock(&ts2);
      
      delta = ts_duration(ts1, ts2);
      std::cout << "Time for " << 100
		<< " delete " 
		<< delta * 1000 << " ms" << std::endl;
      std::cout << "Request frequency: " << (1/(delta / (float )100))/1000 << " kHz" << std::endl;
      ts_close(TS_DUMMY);
    }
      break;
      
    case UNCONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();      
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      pciReadoutModule[num]->unconfigure();
      break;
      
    case DELETE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();      
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      
      delete pciReadoutModule[num];
      pciReadoutModule[num] = 0;
      break;
      
    case TIMING: {
      unsigned int runs, offset;
      unsigned int tickets[16];
      unsigned int nextRequestTicket = 0;
      unsigned int nextGetTicket = 0;
      unsigned int nrOfTickets = 0;
      unsigned int nrOfOutstandingRequests;
      ROBFragment *fragment;
      tstamp ts1, ts2;
      float delta;

      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      
      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      std::cout << "Enter the number of outstanding requests (1..4)" << std::endl;
      nrOfOutstandingRequests = getdecd(4);

      if (nrOfOutstandingRequests > 4) {
	std::cout << "Not more then 4 outstanding requests currently!" << std::endl;
      }
      
      std::cout << "Number of runs" << std::endl;
      runs = getdecd(10000);
      
      ts_open(1, TS_DUMMY);

      ts_clock(&ts1);

      channels = const_cast< std::vector<DataChannel *> * > (pciReadoutModule[num]->channels());

      unsigned int requested = 0, received = 0;
      offset = 0;
      while ( received < runs ) {
	while ((nrOfTickets != nrOfOutstandingRequests) && 
	       (requested < runs)) {
	  tickets[nextRequestTicket++] = (*channels)[dataChannel]->requestFragment( 0x12 + offset);
	  offset = 1 - offset;
	  requested++;
	  if (nextRequestTicket == nrOfOutstandingRequests)
	    nextRequestTicket = 0;
	  nrOfTickets++;
	}
	if (nrOfTickets != 0) {
	  fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextGetTicket++]));

	  /*
	  fragBufferAddress = (unsigned int *)fragment->buffer()->current();
	  for ( i = 0; i < fragment->size(); i++) {
	    std::cout << std::hex 
		      << i 
		      << " :  " 
		      << fragBufferAddress[i] 
		      << std::dec
		      << std::endl;
	    
	  }
	  */


	  if (nextGetTicket == nrOfOutstandingRequests) {
	    nextGetTicket = 0;
	  }
	  nrOfTickets--;
	  delete fragment;
	  received++;
	}
      }

      ts_clock(&ts2);

      delta = ts_duration(ts1, ts2);
      std::cout << "Time for " << runs 
	   << " request() + get () + delete(robfragment) " 
	   << delta << " s" << std::endl;
      std::cout << "Request frequency: " << (1/(delta / (float )runs))/1000 << " kHz" << std::endl;
      ts_close(TS_DUMMY);

    }
      break;

    case TIMING2: 
    {
      //Assumption: The ROBin has L2IDs 1 to 1000 in memory
      unsigned int loop, reqs, tickets[1000];
      ROBFragment *fragment;
      tstamp ts1, ts2, ts3;
      float delta1, delta2;

      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdecd(0);
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      
      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      std::cout << "Number of requests (max. 1000) " << std::endl;
      reqs = getdecd(4);
      
      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      //Request the fragments
      for(loop = 0; loop < reqs; loop++)
      {
        tickets[loop] = (*channels)[dataChannel]->requestFragment(loop + 1);
      }
      ts_clock(&ts2);

      //Get the data
      for(loop = 0; loop < reqs; loop++)
      {
	fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[loop]));
        delete fragment;
      }
      ts_clock(&ts3);

      delta1 = 1000000.0 * ts_duration(ts1, ts2);
      delta2 = 1000000.0 * ts_duration(ts2, ts3);
      std::cout << "Time for " << reqs << " request() " << delta1 << " us" << std::endl;
      std::cout << "Time for " << reqs << " get () + delete(robfragment) " << delta2 << " us" << std::endl;
      ts_close(TS_DUMMY);
    }
    break;
    
  case TIMING3: 
    {    
      int loop;
      tstamp ts1, ts2;
      float delta1;     
      
      ts_open(1, TS_DUMMY);
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();      
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
     }
      
      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      for (loop = 10; loop < 310; loop += 10)
      {
        idgroup.clear();
        for(int loop2 = 1; loop2 < (loop + 1); loop2++)
          idgroup.push_back(loop2);    // L1ID is dummy
        
        ts_clock(&ts1);
        (*channels)[dataChannel]->releaseFragment(&idgroup);  
        ts_clock(&ts2);
        delta1 = 1000000.0 * ts_duration(ts1, ts2);
        float for12 = delta1 / loop * 12;
        std::cout << "Time for release("<< loop << ") = "<< delta1 << " us  (for 12 ROLs: " << for12 << " us)" << std::endl;

      }
      ts_close(TS_DUMMY);
    }   
    break;    

    case TIMING4: 
    {
      unsigned int nevents, nevents2, nextticket, nextl1id, tickets[4][2];
      unsigned int isempty, loop;
      unsigned int ddd = 0, datay = 0, datan = 0, ndel = 0;
      ROBFragment *fragment;
      tstamp ts1, ts2;
      float delta1, delta2;

      if (pciReadoutModule[0] != 0)
      {
        std::cout << "Array element 0 has already been filled"  << std::endl;
        return 1;
      }
      pciReadoutModule[0] = new PciRobinReadoutModule();

      std::cout << "Enter RobIn ID (currently PCI logical board number: 0 .. n)" << std::endl;
      value = getdecd(0);
      configuration->set("robInID", value);     

// SHOULD IT BE FIXED TO 1 ????????
      std::cout << "Enter Number of Channels " << std::endl;
      value = getdecd(1);
      configuration->set("numberOfChannels", value);

      std::cout << "Enter the value for <timeout>(seconds) " << std::endl;
      value = getdecd(10);
      configuration->set("timeout", value);

      std::cout << "Enter the value for <memoryPoolNumPages#0>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 0);
 
      std::cout << "Enter the value for <memoryPoolPageSize#0>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 0);     

      std::cout << "Enter the value for <channelId#0> " << std::endl;
      value = getdecd(12345);
      configuration->set("channelId", value, 0);

      std::cout << "Enter the value for <channelPhysicalAddress#0> " << std::endl;
      value = getdecd(0);
      if (value > 1 ) {
        std::cout << " wrong value for the Rol Physical address " << std::endl;
      }

      dataChannel = 0;  // index into channels

      configuration->set("channelPhysicalAddress", value, 0);

      pciReadoutModule[0]->setup(configuration);

      std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
      ddd = getdecd(ddd);     

      std::cout << "How many events do you want to process " << std::endl;
      nevents = getdecd(1000);     
      nevents2 = nevents;
      
      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      nextl1id = 1;
      
      std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;
      getstat = 0;
      
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[0]->channels());

      //Fill the message Q
      for (loop = 0; loop < 4; loop++)
      {
        if (ddd)
           std::cout << "Posting request for L1ID " << nextl1id << std::endl;
        tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
        tickets[loop][1] = nextl1id;
        nextl1id++;
      }
      nextticket = 0;

      idgroup.clear();

      while(nevents)
      {
      	//Wait for a complete ROB fragment
        isempty = 1;       
        while(isempty)
        {
	  if (getstat)
	  {
	    getstat = 0;
	    std::cout << "Number of empty events             = " << std::dec << datan << std::endl;
            std::cout << "Number of complete events          = " << datay << std::endl;
            std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
	    std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " 
                                                        << tickets[1][1] << ", " 
	                                                << tickets[2][1] << " and " 
                                                        << tickets[3][1] << std::endl;
	  }  
	
          if (ddd)
             std::cout << "waiting for L1ID " << std::dec << tickets[nextticket][1] << std::endl;

          fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

          if (fragment) { // good fragment (with data)
            if (ddd) {
              std::cout << " after getFragment: fragment status = " << std::hex << fragment->status()
                        << " L1id = " << std::dec << fragment->level1Id() << std::endl;
            }
	    //if (ret_id != tickets[nextticket][1])
	    //  std::cout << "Level1 ID wrong! Requested" << tickets[nextticket][1] 
            //              << "  received: " << ret_id << std::endl; 
            delete fragment;

            idgroup.push_back(tickets[nextticket][1]);
	    if (ddd)
               std::cout << "L1ID = " << tickets[nextticket][1] 
                         << " received and scheduled for release (datay = " << datay << ")" << std::endl;
	    isempty = 0;
            datay++;
          }
          else  //Requeue the L1ID: lost fragments are not handled correctly ...
          {
            if (ddd) {
               std::cout << "Requeueing L1ID " <<  tickets[nextticket][1] << std::endl;
            }
               
            tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
            nextticket++;
            if (nextticket == 4)
              nextticket = 0;
            datan++;
          }
        }  
        
        //Queue a new L1ID
 	if (ddd)
             std::cout << "Requesting new L1ID = " << nextl1id << std::endl;
        tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
        tickets[nextticket][1] = nextl1id;
        nextl1id++;
        nextticket++;
        if (nextticket == 4)
          nextticket = 0;
              
        //Send a delete request if we have received 100 events
        if ((datay % 100) == 0)
        {
          if (ddd)
          {
            std::cout << "100 events released" << std::endl;
            //std::cout << idgroup << std::endl;
          }
          (*channels)[dataChannel]->releaseFragment(&idgroup);  
          idgroup.clear();
	  ndel++;
        } 
        nevents--;
      }

      ts_clock(&ts2);
      delta1 = 1000000.0 * ts_duration(ts1, ts2);

      std::cout << "Number of getFragment without data = " << std::dec << datan << std::endl;
      std::cout << "Number of getFragment with data    = " << datay << std::endl;
      std::cout << "Total time     =  " << delta1 << " us" << std::endl;
      delta2 = delta1 / nevents2;
      std::cout << "Time per event =  " << delta2 << " us" << std::endl;

      ts_close(TS_DUMMY);
      pciReadoutModule[0]->unconfigure();
      delete pciReadoutModule[0];
      pciReadoutModule[0] = 0;
    }
    break;

    case TIMING5: 
    {
      unsigned int clearfirst, nevents, nevents2, nextticket, nextl1id, tickets[4][2];
      unsigned int isempty, loop;
      unsigned int min1, min2, ddd = 0, datay = 0, datan = 0, ndel = 0;
      ROBFragment *fragment;
      tstamp ts1, ts2;
      float limit, percent = 10.0, delta1, delta2;

      if (pciReadoutModule[0] != 0)
      {
        std::cout << "Array element 0 has already been filled"  << std::endl;
        return 1;
      }
      pciReadoutModule[0] = new PciRobinReadoutModule();

      std::cout << "Enter RobIn ID (currently PCI logical board number: 0 .. n)" << std::endl;
      value = getdecd(0);
      configuration->set("robInID", value);     

// SHOULD IT BE FIXED TO 1 ????????
      std::cout << "Enter Number of Channels " << std::endl;
      value = getdecd(1);
      configuration->set("numberOfChannels", value);

      std::cout << "Enter the value for <timeout>(seconds) " << std::endl;
      value = getdecd(10);
      configuration->set("timeout", value);

      std::cout << "Enter the value for <memoryPoolNumPages#0>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 0);
 
      std::cout << "Enter the value for <memoryPoolPageSize#0>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 0);     

      std::cout << "Enter the value for <channelId#0> " << std::endl;
      value = getdecd(12345);
      configuration->set("channelId", value, 0);

      std::cout << "Enter the value for <channelPhysicalAddress#0> " << std::endl;
      value = getdecd(0);
      if (value > 1 ) {
        std::cout << " wrong value for the Rol Physical address " << std::endl;
      }

      dataChannel = 0;	// only ONE channel

      configuration->set("channelPhysicalAddress", value, 0);

      pciReadoutModule[0]->setup(configuration);
      
      std::cout << "Enter the (float) request percantage" << std::endl;
      percent = getfloatd(percent);    
      
      std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
      ddd = getdecd(ddd); 

      std::cout << "How many events do you want to process " << std::endl;
      nevents = getdecd(1000);     
      nevents2 = nevents;
      
      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      nextl1id = 1;
      clearfirst = 1;
      limit = 0.0;
      
      std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;
      getstat = 0;
      
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[0]->channels());

      //Fill the message Q
      tickets[0][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
      tickets[0][1] = nextl1id;      
      for (loop = 1; loop < 4; loop++)
      {
        nextl1id = getnext(percent, limit, nextl1id);
	tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
        tickets[loop][1] = nextl1id;
      }
      nextticket = 0;
      
      while(nevents)
      {
      	//Wait for a complete ROB fragment
        isempty = 1;       
        while(isempty)
        {
	  if (getstat)
	  {
	    getstat = 0;
	    std::cout << "Number of empty events             = " << std::dec << datan << std::endl;
            std::cout << "Number of complete events          = " << datay << std::endl;
            std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
	    std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " << tickets[1][1] << ", " 
	              << tickets[2][1] << " and " << tickets[3][1] << std::endl;
	  }  
	  
          if (ddd)
             std::cout << "waiting for L1ID " << tickets[nextticket][1] << std::endl;

          fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

          if (fragment) { // good fragment (with data)
            if (ddd) {
              std::cout << " after getFragment: fragment status = " << std::hex << fragment->status()
                        << " L1id = " << std::dec << fragment->level1Id() << std::endl;
            }
	    //if (ret_id != tickets[nextticket][1])
	    //  std::cout << "Level1 ID wrong! Requested" << tickets[nextticket][1] 
            //              << "  received: " << ret_id << std::endl; 
            delete fragment;

            idgroup.push_back(tickets[nextticket][1]);
            if (ddd)
               std::cout << "L1ID = " << tickets[nextticket][1]
                         << " received and scheduled for release (datay = " << datay << ")" << std::endl;
	    isempty = 0;
            datay++;
          }
          else  //Requeue the L1ID; lost fragments are not handled correctly
          {
            tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
            nextticket++;
            if (nextticket == 4)
              nextticket = 0;
            datan++;
          }
        }  
        
        //Queue a new L1ID
        nextl1id = getnext(percent, limit, nextl1id);
        tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
        tickets[nextticket][1] = nextl1id;
        nextticket++;
        if (nextticket == 4)
          nextticket = 0;

        //Find the lowest L1ID that we are still waiting for
        if (tickets[0][1] < tickets[1][1])
	  min1 = tickets[0][1];
	else
	  min1 = tickets[1][1];

        if (tickets[2][1] < tickets[3][1])
	  min2 = tickets[2][1];
	else
	  min2 = tickets[3][1];
	
	if (min1 < min2)
	  min2 = min1; 	  

        if (ddd)
         std::cout << "Lowest outstanding L1ID = " << min2 << std::endl;
	 
        //Send a delete request if we have received 100 events
        if ((clearfirst + 100) < min2)
	{  
          idgroup.clear();
          for(int loop = 0; loop < 100; loop++)
            idgroup.push_back(clearfirst++);
	  (*channels)[dataChannel]->releaseFragment(&idgroup);
	  ndel++;  
          if (ddd)
	    std::cout << "events released up to L1ID " << clearfirst - 1 << std::endl;
        } 
        nevents--;
      }

      ts_clock(&ts2);
      delta1 = 1000000.0 * ts_duration(ts1, ts2);

      std::cout << "Number of getFragment without data = " << datan << std::endl;
      std::cout << "Number of getFragment with data    = " << datay << std::endl;
      std::cout << "Total time     =  " << delta1 << " us" << std::endl;
      delta2 = delta1 / nevents2;
      std::cout << "Time per event out =  " << delta2 << " us" << std::endl;
      delta2 = delta2 * percent / 100.0;
      std::cout << "Time per event in =  " << delta2 << " us" << std::endl;

      ts_close(TS_DUMMY);
      pciReadoutModule[0]->unconfigure();
      delete pciReadoutModule[0];
      pciReadoutModule[0] = 0;
    }
    break;
    
    case TIMING6: 
    {
      unsigned int nevents[2], nevents2[2], nextticket[2], nextl1id[2], tickets[2][4][2];
      unsigned int rdump = 0, cdata = 0, isempty, loop;
      unsigned int rol, value, ddd = 0,  datay[2] = {0, 0}, datan[2] = {0, 0}, ndel[2] = {0, 0};
      int numberOfBadEvents = 0;
      bool eventOk;
      ROBFragment *fragment;
      tstamp ts1, ts2;
      float delta1, delta2;

      if (pciReadoutModule[0] != 0)
      {
        std::cout << "Array element 0 has already been filled"  << std::endl;
        return 1;
      }
      pciReadoutModule[0] = new PciRobinReadoutModule();

      std::cout << "Enter RobIn ID (currently PCI logical board number: 0 .. n)" << std::endl;
      value = getdecd(0);
      configuration->set("robInID", value);     

// SHOULD IT BE FIXED TO 2 ????????
      std::cout << "Enter Number of Channels " << std::endl;
      value = getdecd(2);
      configuration->set("numberOfChannels", value);

      std::cout << "Enter the value for <timeout>(seconds) " << std::endl;
      value = getdecd(10);
      configuration->set("timeout", value);

      std::cout << "Enter the value for <memoryPoolNumPages#0>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 0);
 
      std::cout << "Enter the value for <memoryPoolPageSize#0>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 0);     

      std::cout << "Enter the value for <channelId#0> " << std::endl;
      value = getdecd(12345);
      configuration->set("channelId", value, 0);

      std::cout << "Enter the value for <channelPhysicalAddress#0> " << std::endl;
      value = getdecd(0);
      if (value > 1 )
        std::cout << " wrong value for the Rol Physical address " << std::endl;

      configuration->set("channelPhysicalAddress", value, 0);

      std::cout << "Enter the value for <memoryPoolNumPages#1>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 1);
 
      std::cout << "Enter the value for <memoryPoolPageSize#1>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 1);     

      std::cout << "Enter the value for <channelId#1> " << std::endl;
      value = getdecd(67890);
      configuration->set("channelId", value, 1);

      std::cout << "Enter the value for <channelPhysicalAddress#1> " << std::endl;
      value = getdecd(1);
      if (value > 1 )
        std::cout << " wrong value for the Rol Physical address " << std::endl;

      configuration->set("channelPhysicalAddress", value, 1);

      pciReadoutModule[0]->setup(configuration);
      
      std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
      ddd = getdecd(ddd);  
      
      std::cout << "Enable data checking (0 = No  1 = Yes  2 = Reduced output)" << std::endl;
      cdata = getdecd(cdata);

      if (cdata)
      {
        std::cout << "Dump raw data (0 = No  1 = Yes)" << std::endl;
        rdump = getdecd(rdump);
      }

      std::cout << "How many events do you want to process " << std::endl;
      nevents[0] = getdecd(200000);     
      nevents2[0] = nevents[0];
      nevents[1] = nevents[0];
      nevents2[1] = nevents[1];
      
      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      nextl1id[0] = 1;
      nextl1id[1] = 1;

      std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;
      getstat = 0;
      
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[0]->channels());

      //Fill the message Q
      for (loop = 0; loop < 4; loop++)
      {
        tickets[0][loop][0] = (*channels)[0]->requestFragment(nextl1id[0]);
        tickets[0][loop][1] = nextl1id[0];
        nextl1id[0]++;
        tickets[1][loop][0] = (*channels)[1]->requestFragment(nextl1id[1]);
        tickets[1][loop][1] = nextl1id[1];
        nextl1id[1]++;	
      }
      nextticket[0] = 0;
      nextticket[1] = 0;

      idgroup.clear();
      idgroup2.clear();
      rol = 0;

      while(nevents[0] + nevents[1])
      {
        isempty = 1;       
        while(isempty)  //Wait for a complete ROB fragment
        {
	  if (getstat)
	  {
	    getstat = 0;
	    std::cout << "ROL 0:" << std::endl;
	    std::cout << "Number of empty events             = " << std::dec << datan[0] << std::endl;
            std::cout << "Number of complete events          = " << datay[0] << std::endl;
            std::cout << "Number of release(100) groups sent = " << ndel[0] << std::endl;
	    std::cout << "Currently waiting for L1IDs " << tickets[0][0][1] << ", " 
                      << tickets[0][1][1] << ", " 
                      << tickets[0][2][1] << " and " 
                      << tickets[0][3][1] << std::endl;
	    std::cout << "ROL 1:" << std::endl;
	    std::cout << "Number of empty events             = " << datan[1] << std::endl;
            std::cout << "Number of complete events          = " << datay[1] << std::endl;
            std::cout << "Number of release(100) groups sent = " << ndel[1] << std::endl;
	    std::cout << "Currently waiting for L1IDs " << tickets[1][0][1] << ", " 
                      << tickets[1][1][1] << ", " 
                      << tickets[1][2][1] << " and " 
                      << tickets[1][3][1] << std::endl;	      
	  }  
	
	  if (nevents[rol] == 0)   //this channel has already done its job
            rol = 1 - rol;         //try the other ROL

          if (ddd)
             std::cout << "waiting for L1ID " << tickets[rol][nextticket[rol]][1] << " from ROL " << rol << std::endl;

          fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));

          if (fragment) 
	  { // good fragment (with data)
            if (ddd) 
              std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << " L1id = " << std::dec << fragment->level1Id() << std::endl;

            if (cdata)  
	    {
	      if (ddd)
	      {
	        std::cout << "Calling checkData with" << std::endl;
	        std::cout << "fragment at 0x " << &fragment << std::endl;
	        std::cout << "tickets[rol][nextticket[rol]][1] = " << tickets[rol][nextticket[rol]][1] << std::endl;
	        std::cout << "rol                              = " << rol << std::endl;
	        std::cout << "rdump                            = " << rdump << std::endl;
	      }
              eventOk = checkData(fragment, tickets[rol][nextticket[rol]][1], rol, rdump, cdata);
              if (eventOk != 0)
                numberOfBadEvents++;
            }
	    
            delete fragment;

            if (ddd)
               std::cout << "Event with L1ID " << tickets[rol][nextticket[rol]][1] << " received from ROL "  << rol << std::endl;

	    if (rol == 0)
              idgroup.push_back(tickets[rol][nextticket[rol]][1]);
	    else
	      idgroup2.push_back(tickets[rol][nextticket[rol]][1]);

	    isempty = 0;
            datay[rol]++;
          }
          else  //Requeue the L1ID; lost fragments are not handled correctly.
          {
            tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
            nextticket[rol]++;
            if (nextticket[rol] == 4)
              nextticket[rol] = 0;
            datan[rol]++;
            rol = 1 - rol;  //try the other ROL
          }
        }  

        //Queue a new L1ID
	tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(nextl1id[rol]);
        tickets[rol][nextticket[rol]][1] = nextl1id[rol];
        nextl1id[rol]++;
        nextticket[rol]++;
        if (nextticket[rol] == 4)
          nextticket[rol] = 0;

        //Send a delete request if we have received 100 events
        if ((datay[rol] % 100) == 0)
        {
          if (ddd)
            std::cout << "100 events released on ROL " << rol << std::endl;
	  if (rol == 0)
	  {
            (*channels)[rol]->releaseFragment(&idgroup);  
            idgroup.clear();
	  }
	  else
          {
            (*channels)[rol]->releaseFragment(&idgroup2);  
            idgroup2.clear();
	  }	    
	  ndel[rol]++;
        } 
        if (nevents[rol])
	  nevents[rol]--;
        rol = 1 - rol;  //try the other ROL
      }

      ts_clock(&ts2);
      delta1 = 1000000.0 * ts_duration(ts1, ts2);

      std::cout << "ROL 0 : Number of getFragment without data = " << dec << datan[0] << std::endl;
      std::cout << "ROL 0 : Number of getFragment with data    = " << datay[0] << std::endl;
      std::cout << "ROL 1 : Number of getFragment without data = " << datan[1] << std::endl;
      std::cout << "ROL 1 : Number of getFragment with data    = " << datay[1] << std::endl;
      std::cout << "Total time     =  " << delta1 << " us" << std::endl;
      delta2 = delta1 / (nevents2[0] + nevents2[1]);
      std::cout << "Time per event =  " << delta2 << " us" << std::endl;

      if (cdata) {
        std::cout << "Number of events where data check failed = " << numberOfBadEvents << std::endl;
      }

      ts_close(TS_DUMMY);
      
      pciReadoutModule[0]->unconfigure();
      delete pciReadoutModule[0];
      pciReadoutModule[0] = 0;
    }
    break;

  case TIMING7: {
      tstamp ts1, ts2;
      float delta;
      int todo, last;
      unsigned int ddd = 0;
      static int rol1 = 1, rol2 = 1, first = 1, total = 1000, gsize = 100;

      if (pciReadoutModule[0] != 0)
      {
        std::cout << "Array element 0 has already been filled"  << std::endl;
        return 1;
      }
      pciReadoutModule[0] = new PciRobinReadoutModule();

      std::cout << "Enter RobIn ID (currently PCI logical board number: 0 .. n)" << std::endl;
      value = getdecd(0);
      configuration->set("robInID", value);     

// SHOULD IT BE FIXED TO 2 ????????
      std::cout << "Enter Number of Channels " << std::endl;
      value = getdecd(2);
      configuration->set("numberOfChannels", value);

      std::cout << "Enter the value for <timeout>(seconds) " << std::endl;
      value = getdecd(10);
      configuration->set("timeout", value);

      std::cout << "Enter the value for <memoryPool0NumPages#0>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 0);
 
      std::cout << "Enter the value for <memoryPool0PageSize#0>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 0);     

      std::cout << "Enter the value for <channel0Id#0> " << std::endl;
      value = getdecd(12345);
      configuration->set("channelId", value, 0);

      std::cout << "Enter the value for <channel0PhysicalAddress#0> " << std::endl;
      value = getdecd(0);
      if (value > 1 ) {
        std::cout << " wrong value for the Rol Physical address " << std::endl;
      }

      configuration->set("channelPhysicalAddress", value, 0);

      std::cout << "Enter the value for <memoryPool1NumPages#1>" << std::endl;
      value = getdecd(100);
      configuration->set("memoryPoolNumPages", value, 1);
 
      std::cout << "Enter the value for <memoryPool1PageSize#1>" << std::endl;
      value = getdecd(4096);
      configuration->set("memoryPoolPageSize", value, 1);     

      std::cout << "Enter the value for <channel1Id#1> " << std::endl;
      value = getdecd(67890);
      configuration->set("channelId", value, 1);

      std::cout << "Enter the value for <channel1PhysicalAddress#1> " << std::endl;
      value = getdecd(1);
      if (value > 1 ) {
        std::cout << " wrong value for the Rol Physical address " << std::endl;
      }

      configuration->set("channelPhysicalAddress", value, 1);

      pciReadoutModule[0]->setup(configuration);
      
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[0]->channels());

      std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
      ddd = getdecd(ddd);     

      std::cout << "Enter the L1ID of the first event to be released" << std::endl;
      first = getdecd(first);
      std::cout << "How many events do you want to release" << std::endl;
      total = getdecd(total);
      std::cout << "Enter the release group size (max 100)" << std::endl;
      gsize = getdecd(gsize);
      std::cout << "Delete events from ROL 0" << std::endl;
      rol1 = getdecd(rol1);
      std::cout << "Delete events from ROL 1" << std::endl;
      rol2 = getdecd(rol2);

      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      last = first + total - 1;
      while (first < last)
      {
        if ((first + gsize) < last)
          todo = gsize;
        else
          todo = last - first + 1;

        if (ddd)
          std::cout << "Deleting group of " << dec << todo << " events with IDs " 
                    << first << " to " << first + todo - 1 << std::endl;

        idgroup.clear();
        for(int loop = 0; loop < todo; loop++)
          idgroup.push_back(first++);

        if (rol1) (*channels)[0]->releaseFragment(&idgroup);
        if (rol2) (*channels)[1]->releaseFragment(&idgroup);
      }

      ts_clock(&ts2);

      delta = ts_duration(ts1, ts2);
      std::cout << "Total time for " << dec << total << " events = " << delta * 1000000 << " us" << std::endl;
      std::cout << "Time for one event (fragment) from one ROL = " << delta * 1000000 / total / 2 << " us" << std::endl;
      ts_close(TS_DUMMY);

      pciReadoutModule[0]->unconfigure();
      delete pciReadoutModule[0];
      pciReadoutModule[0] = 0;
    }
      break;

  case DUMPONE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }

      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      std::cout << "Enter the Level 1 ID" << std::endl;
      l1id = getdecd(1); 
         
      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      value = (*channels)[dataChannel]->requestFragment(l1id); 

      robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(value)); 
      std::cout << "Dumping ROB fragment" << std::endl;

      fragBufferAddress = (unsigned int *)robfragment->buffer()->current();

      unsigned int i;
      for ( i = 0; i < robfragment->size() ; i++) {
	std::cout << std::hex 
		  << i 
		  << " :  " 
		  << fragBufferAddress[i] 
		  << std::dec
		  << std::endl;
	
      }
      delete robfragment;

    break;

  case DUMPMANY:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }

      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }
         
      std::cout << "How many events do you want to request" << std::endl;
      count = getdecd(100);

      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      for (l1id = 1; l1id <= count; l1id++) {
	
	std::cout << "Requesting LVL1 ID: " << l1id << std::endl;

	value = (*channels)[dataChannel]->requestFragment(l1id); 
	
	robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(value)); 
	std::cout << "Dumping ROB fragment" << std::endl;
	
	fragBufferAddress = (unsigned int *)robfragment->buffer()->current();
	
	unsigned int i;
	for ( i = 0; i < robfragment->size(); i++) {
	  std::cout << std::hex 
		    << i 
		    << " :  " 
		    << fragBufferAddress[i] 
		    << std::dec
		    << std::endl;
	  
	}
	
	delete robfragment;
      }

      break;

  case GETINFO: {
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (pciReadoutModule[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }

      std::cout << "Enter ROL (0 or 1)" << std::endl;
      dataChannel = getdecd(0);
      if (dataChannel > 1) {
	std::cout << "Wrong channel number." << std::endl;
      }

      channels = const_cast< std::vector<DataChannel *> * >(pciReadoutModule[num]->channels());

      DFCountedPointer<Config> info;
      info = (*channels)[dataChannel]->getInfo(); 
      
      std::cout << "RobIn reply:" << std::endl;
      unsigned int mostRecentId;
      mostRecentId = info->getInt("mostRecentId");
      std::cout << "\t" << "mostRecentId:  " << std::hex << mostRecentId << std::dec << std::endl;
      std::cout << "--------------------------------------------------" << std::endl;
      info->dump();
      std::cout << "--------------------------------------------------" << std::endl;
  }
      break;

  case SETCONFIG: {
    std::cout << "Not yet available" << std::endl;
  }
    break;

  case RESET: {
    std::cout << "Not yet available" << std::endl;
  }
    break;

  case SETDEBUG:

    setdebug();

    break;

  case HELP:
    std::cout <<  " Exerciser program for the PciFragmentManager Class." << std::endl;
    std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
    std::cout <<  " the basic PciFragmentManager methods: this is a bit for specialists ..." << std::endl;
    break;

      case QUIT:
        quit_flag = 1;
        break;

      default:
        std::cout <<  "not implemented yet" << std::endl;
 
      } //main switch
    } while (quit_flag == 0);

  }
  catch(exception &ex) 
  {
    std::cout << ex.what() << std::endl;
  }
  return 0;
}


/***************************************************/
int getnext(float percent, float limit, int nextl1id)
/***************************************************/
{
  while (limit < 100.0)
  {
    limit += percent;
    nextl1id++;
  }
  limit -= 100.0;
  return nextl1id;
}

/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}



/************************************************************************/
int checkData(ROBFragment *fragment, int id, int rol, int rdump, int mode)
/************************************************************************/
{
  int notok = 0;
  Buffer *fragBuffer = fragment->buffer();
  MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
  unsigned int dumpit, *fragBufferAddress = (unsigned int *)memPage->address();
  int i, fsize, dsize, hsize, tsize, dstart, sstart, tstart;
  unsigned int headerData[25];
  unsigned int trailerData[3];
  
  //We need some constants....
  fsize = (int)fragment->size();         //Total fragment size
  hsize = 25;                            //ROD header + ROB header size
  tsize = 3;                             //ROD trailer
  dsize = fsize - hsize - tsize - 1;     //ROD data size
  dstart = hsize;
  sstart = fsize - 4; //status elements follow data
  tstart = fsize - 3;

  headerData[0]  = 0xdd1234dd;                    //ROB: header marker
  headerData[1]  = fsize;                         //ROB: total fragment size
  headerData[2]  = 0x00000010;                    //ROB: header size
  headerData[3]  = 0x02040000;                    //ROB: format version number
  headerData[4]  = 0x0101002f + rol;              //ROB: source identifier
  headerData[5]  = 0x0000000a;                    //ROB: run number
  headerData[6]  = 0x00000002;                    //ROB: number of status elements
  headerData[7]  = 0x00000000;                    //ROB: status element 1
  headerData[8]  = 0x00000000;                    //ROB: status element 2 (most revent L1ID received by the ROBIN; do not check)
  headerData[9]  = 0x00000001;                    //ROB: number of offset elements
  headerData[10]  = ((0x2f + rol ) << 24) + 0x10; //ROB: offset element 1
  headerData[11]  = 0x00000004;                   //ROB: number of fragment specific words
  headerData[12]  = id;                           //ROB: Level 1 ID
  headerData[13]  = 0x00000000;                   //ROB: Bunch crossing ID
  headerData[14]  = 0x00000000;                   //ROB: Level 1 trigger type
  headerData[15]  = 0x00000000;                   //ROB: Detector event type
  headerData[16]  = 0xee1234ee;                   //ROD: header marker
  headerData[17]  = 0x00000009;                   //ROD: header size
  headerData[18]  = 0x02040000;                   //ROD: format version number
  headerData[19]  = 0x00000000;                   //ROD: source identifier
  headerData[20]  = 0x00000001;                   //ROD: run number
  headerData[21]  = id;                           //ROD: Level 1 ID
  headerData[22]  = 3 * id;                       //ROD: Bunch crossing ID
  headerData[23]  = 0x00000000;                   //ROD: Level 1 trigger type
  headerData[24]  = 0x00000000;                   //ROD: Detector event type

  trailerData[0] = 0x00000001;                    //ROD: number of status elements
  trailerData[1] = dsize;                         //ROD: number of data elements
  trailerData[2] = 0x00000001;                    //ROD: status block position (follows data)

  dumpit = 0;
  //Header
  for (i = 0; i < hsize; i++)
  {
    if (fragBufferAddress[i] != headerData[i] && i!=8 && i!=4 && i!=10 && i!=19 && i!=23 && i!=24 )
    {
      std::cout << "Header " << std::dec << i << ": 0x" << std::hex << fragBufferAddress[i] << " [ 0x" << headerData[i] << " ] " << std::dec << std::endl;
      dumpit = 1;
      notok = 1;
      if (mode == 2)  {std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; return(notok);}
    }
  }

  //Data
  for (i = 0; i < dsize; i++)
  {
    if ((int)fragBufferAddress[i + dstart] != i)
    {
      std::cout << "Data " << std::dec << i + dstart << ": 0x" << std::hex << fragBufferAddress[i + dstart] << " [ 0x" << i << " ] " << std::dec << std::endl;
      dumpit = 1;
      notok = 1;
      if (mode == 2)  {std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; return(notok);}
    }
  }

  //Status
  if (fragBufferAddress[sstart] != 0)
  {
    std::cout << "Status " << std::dec << sstart << ": 0x" << std::hex << fragBufferAddress[sstart] << " [ 0x0 ] " << std::dec << std::endl;
    dumpit = 1;
    notok = 1;
    if (mode == 2)  {std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; return(notok);}
  }

  //Trailer
  for (i = 0; i < tsize; i++)
  {
    if (fragBufferAddress[i + tstart] != trailerData[i])
    {
      std::cout << "Trailer " << std::dec << i + tstart << ": 0x" << std::hex << fragBufferAddress[i + tstart] << " [ 0x" << trailerData[i] << " ] " << std::dec << std::endl;
      dumpit = 1;
      notok = 1;
      if (mode == 2)  {std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl; return(notok);}
    }
  }  
  
  if (dumpit || rdump)
  {
   std::cout << std::dec << std::endl;
   std::cout << "ROL " << rol << ": Fragment size = " << fsize << std::endl;
   std::cout << "dstart = " << dstart << std::endl;
   std::cout << "sstart = " << sstart << std::endl;
   std::cout << "tstart = " << tstart << std::endl;
   std::cout << "dsize = " << dsize << std::endl;   
   for (i = 0; i < fsize; i++)
   {
     std::cout << "word(" << i << "): 0x" << std::hex << fragBufferAddress[i] << std::dec; 
     if (i == 0) std::cout << "  ROB: header marker" << std::endl;
     else if (i == 1) std::cout << "  ROB: total fragment size" << std::endl;
     else if (i == 2) std::cout << "  ROB: header size" << std::endl;
     else if (i == 3) std::cout << "  ROB: format version number" << std::endl;
     else if (i == 4) std::cout << "  ROB: source identifier" << std::endl;
     else if (i == 5) std::cout << "  ROB: run number" << std::endl;
     else if (i == 6) std::cout << "  ROB: number of status elements" << std::endl;
     else if (i == 7) std::cout << "  ROB: status element 1" << std::endl;
     else if (i == 8) std::cout << "  ROB: status element 2 (most revent L1ID received by the ROBIN; do not check)" << std::endl;
     else if (i == 9) std::cout << "  ROB: number of offset elements" << std::endl;
     else if (i == 10) std::cout << "  ROB: offset element 1" << std::endl;
     else if (i == 11) std::cout << "  ROB: number of fragment specific words" << std::endl;
     else if (i == 12) std::cout << "  ROB: Level 1 ID" << std::endl;
     else if (i == 13) std::cout << "  ROB: Bunch crossing ID" << std::endl;
     else if (i == 14) std::cout << "  ROB: Level 1 trigger type" << std::endl;
     else if (i == 15) std::cout << "  ROB: Detector event type" << std::endl;
     else if (i == 16) std::cout << "  ROD: header marker" << std::endl;
     else if (i == 17) std::cout << "  ROD: header size" << std::endl;
     else if (i == 18) std::cout << "  ROD: format version number" << std::endl;
     else if (i == 19) std::cout << "  ROD: source identifier" << std::endl;
     else if (i == 20) std::cout << "  ROD: run number" << std::endl;
     else if (i == 21) std::cout << "  ROD: Level 1 ID" << std::endl;
     else if (i == 22) std::cout << "  ROD: Bunch crossing ID" << std::endl;
     else if (i == 23) std::cout << "  ROD: Level 1 trigger type" << std::endl;
     else if (i == 24) std::cout << "  ROD: Detector event type" << std::endl;
     else if (i == (fsize - 3)) std::cout << "  ROD: number of status elements" << std::endl;
     else if (i == (fsize - 2)) std::cout << "  ROD: number of data elements" << std::endl;
     else if (i == (fsize - 1)) std::cout << "  ROD: status block position (follows data)" << std::endl;
     else std::cout << std::endl; 
    }
  }
  
  return notok;
}
