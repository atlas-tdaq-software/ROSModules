/****************************************************************/
/*								*/
/*  file test_emulated_fm.cpp					*/
/*								*/
/*  exerciser for the EmulatedReadoutModule class		*/
/*								*/
/*  The program allows to:					*/
/*  . exercise the methods of the EmulatedReadoutModule Class	*/
/*  . measure the performance					*/
/*								*/
/*								*/
/***C 2002 - A nickel program worth a dime***********************/

#include <iostream.h>
#include <iomanip.h>
#include <vector>
#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModules/EmulatedReadoutModule.h"
#include "ROSModules/EmulatedDataChannel.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;

enum 
{ 
  CREATE = 1,
  CONFIGURE,
  DUMP_CHANNELS,
  REQUEST,
  GET,
  RELEASEMANY,
  UNCONFIGURE,
  DELETE,
  TIMING,
  HELP,
  QUIT = 100
};

#define FM_POOL_NUMB     10      //Max. number of fragment managers

/************/
int main(void)
/************/ 
{
int num, value, l1id, option, quit_flag = 0;
ReadoutModule* fm[FM_POOL_NUMB] = {0};
ROBFragment* robfragment;
float fvalue;
vector <unsigned int> idgroup;
int nChannels=0;
vector<DataChannel *>* vChannels=0; 
DataChannel *channel=0;

DFCountedPointer<Config> configuration = Config::New();

try 
{
  IPCCore::init( argc, argv );
}
catch( daq::ipc::Exception & ex ) 
{
  ers::fatal( ex );
}

do 
  {
  std::cout << std::endl << std::endl;
  std::cout << " Create a Readout Module        : " << dec << CREATE << std::endl; 
  std::cout << " Setup a Readout Module         : " << dec << CONFIGURE << std::endl; 
  std::cout << " Dump channel IDs               : " << dec << DUMP_CHANNELS << std::endl; 
  std::cout << " Request a ROB fragment         : " << dec << REQUEST << std::endl; 
  std::cout << " Get a ROB fragment             : " << dec << GET << std::endl; 
  std::cout << " Release many ROB fragments     : " << dec << RELEASEMANY << std::endl; 
  std::cout << " Unconfigure a Readout Module   : " << dec << UNCONFIGURE << std::endl; 
  std::cout << " Delete a Readout Module        : " << dec << DELETE << std::endl; 
  std::cout << " Run timing benchmarks          : " << dec << TIMING << std::endl; 
  std::cout << " HELP                           : " << dec << HELP << std::endl;
  std::cout << " QUIT                           : " << dec << QUIT << std::endl;
  std::cout << " option> ";
  option = getdec();    
  switch(option)   
  {
    case CREATE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }
      fm[num] = new EmulatedReadoutModule();
      std::cout << "Readout Module created" << std::endl;
      break;
      
    case CONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     

      std::cout << "Enter the value for the number of Data Channels" << std::endl;
      nChannels = getdecd(1);
      configuration->set("numberOfChannels", nChannels);

      for (int i=0; i<nChannels; i++) {	
	std::cout << "Enter the ID of channel " << i << std::endl;
	value = getdecd(i);
	ostringstream str1;
	str1 << "channel" << i << "Id" ;
	configuration->set(str1.str(), value);

	std::cout << "Enter the physical address of channel " << i<< std::endl;
	value = getdecd(i);
	ostringstream str2;
	str2 << "channel" << i << "PhysicalAddress" ;
	configuration->set(str2.str(), value);

	ostringstream str3;
	str3 << "memoryPool" << i << "NumPages" ;
	std::cout << "Enter the value for <" << str3.str() << ">" << std::endl;
	value = getdecd(100);
	configuration->set(str3.str(), value);
	
	ostringstream str4;
	str4 << "memoryPool" << i << "PageSize" ;
	std::cout << "Enter the value for <" << str4.str() << ">" << std::endl;
	value = getdecd(1024);
	configuration->set(str4.str(), value);     
	
	ostringstream str5;
	str5 << "pool" << i << "Type" ;
	std::cout << "Enter the value for <" << str5.str() << "> (1=malloc 2=CMEM)" << std::endl;
	value = getdecd(1);
	configuration->set(str5.str(), value);     

      }

      std::cout << "Enter the value for <trace> (1=yes 0=no)" << std::endl;
      value = getdecd(0);
      configuration->set("trace", value);     

      std::cout << "Enter the value for <getDelay> in us" << std::endl;
      value = getdecd(5);
      configuration->set("getDelay", value);     

      std::cout << "Enter the value for <releaseDelay> in us" << std::endl;
      value = getdecd(1);
      configuration->set("releaseDelay", value);     

      std::cout << "Enter the value for <releaseGroupDelay> in us" << std::endl;
      value = getdecd(1);
      configuration->set("releaseGroupDelay", value);     

      std::cout << "Enter the value for <messageDelay> in us" << std::endl;
      value = getdecd(3);
      configuration->set("messageDelay", value);     

      std::cout << "Enter the value for <level1TriggerType>" << std::endl;
      value = getdecd(0);
      configuration->set("level1TriggerType", value);     

      std::cout << "Enter the value for <detectorEventType>" << std::endl;
      value = getdecd(0);
      configuration->set("detectorEventType", value);     

      std::cout << "Enter the value for <numberOfStatusElements>" << std::endl;
      value = getdecd(1);
      configuration->set("numberOfStatusElements", value);     

      std::cout << "Enter the value for <numberOfDataElements>" << std::endl;
      value = getdecd(1);
      configuration->set("numberOfDataElements", value);     

      std::cout << "Enter the value for <statusBlockPosition>" << std::endl;
      value = getdecd(0);
      configuration->set("statusBlockPosition", value);     

      std::cout << "Enter the value for <getFragmentMiss>" << std::endl;
      fvalue = getfloatd(0.0);
      configuration->set("getFragmentMiss", fvalue);
       
      try 
      {
        fm[num]->setup(configuration);
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl ;
	break;
      }
      break;      
      
    case DUMP_CHANNELS:
      nChannels = DataChannel::numberOfChannels() ;
      vChannels = DataChannel::channels();
      for (int i=0; i<nChannels; i++) {
	std::cout << i << ": Channel ID " << (*vChannels)[i]->id() 
		  << " Physical Address " << (*vChannels)[i]->physicalAddress() 
		  << std::endl ;	  
      }
      break;

    case REQUEST:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try {
	channel = DataChannel::channel(num) ;
      }
      catch (ModulesException &e) {
	std::cout << e << std::endl ;
	break;
      }
      
      std::cout << "Enter the Level 1 ID" << std::endl;
      l1id = getdecd(1); 
         
      value = channel->requestFragment(l1id); 
      std::cout << "Request returns the ticket " << value << std::endl;   
      break;
      
    case GET:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try {
	channel = DataChannel::channel(num) ;
      }
      catch (ModulesException &e) {
	std::cout << e << std::endl;
	break;
      }
      
      std::cout << "Enter the ticket received from request()" << std::endl;
      l1id = getdec(); 
         
      try 
      {
        robfragment = dynamic_cast<ROS::ROBFragment*>(channel->getFragment(l1id)); 
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "Dumping ROB fragment" << std::endl;
      std::cout << *robfragment;
      delete robfragment;
      break;
      
    case RELEASEMANY:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try {
	channel = DataChannel::channel(num) ;
      }
      catch (ModulesException &e) {
	std::cout << e << std::endl;
      }
      
      std::cout << "How many events do you want to release" << std::endl;
      value = getdecd(100);
      
      for(int loop = 0; loop < value; loop++)
        idgroup.push_back(loop);    // L1ID is dummy
      
      channel->releaseFragment(&idgroup);  // L1ID is dummy      
      break;
      
    case UNCONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     

      fm[num]->unconfigure();
      break;
      
    case DELETE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     
      delete fm[num];
      
      break;
    case TIMING:
//T.B.I.      tstamp ts1, ts2;
//T.B.I.      float delta;
//T.B.I.      ReadoutModule* fm;
//T.B.I.      int ticket;
//T.B.I.      
//T.B.I.      fm = new EmulatedReadoutModule();
//T.B.I.      configuration->set("memoryPoolNumPages", 100);
//T.B.I.      configuration->set("memoryPoolPageSize", 1024);     
//T.B.I.      configuration->set("pooltype", 1);     
//T.B.I.      configuration->set("trace", 0);
//T.B.I.      configuration->set("internalROLNumber", 1);
//T.B.I.      configuration->set("getDelay", 0);     
//T.B.I.      configuration->set("releaseDelay", 0);     
//T.B.I.      configuration->set("messageDelay", 0);     
//T.B.I.      configuration->set("robId", 0);     
//T.B.I.      configuration->set("level1TriggerType", 0);     
//T.B.I.      configuration->set("detectorEventType", 0);     
//T.B.I.      configuration->set("numberOfStatusElements", 1);     
//T.B.I.      configuration->set("numberOfDataElements", 100);     
//T.B.I.      configuration->set("statusBlockPosition", 0);
//T.B.I.      fm->configure(configuration);
//T.B.I.   
//T.B.I.      ts_open(1, TS_DUMMY);
//T.B.I.
//T.B.I.      ts_clock(&ts1);
//T.B.I.      ticket = fm->requestFragment(0);
//T.B.I.      robfragment = dynamic_cast<ROS::ROBFragment*>(fm->getFragment(ticket));
//T.B.I.      delete robfragment;     
//T.B.I.      ts_clock(&ts2);
//T.B.I.
//T.B.I.      delta = ts_duration(ts1, ts2);
//T.B.I.      std::cout << "Time for one request() + get () + delete(robfragment) " << delta << " s" << std::endl;
//T.B.I.      ts_close(TS_DUMMY);
//T.B.I.      
//T.B.I.      break;

    case HELP:
      std::cout <<  " Exerciser program for the EmulatedEventManager Class." << std::endl;
      std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
      std::cout <<  " the basic EEmulatedEventManager methods: this is a bit for specialists ..." << std::endl;
      break;

    case QUIT:
      quit_flag = 1;
      break;

    default:
      std::cout <<  "not implemented yet" << std::endl;
 
    } //main switch
  } while (quit_flag == 0);
  return 0;
}
