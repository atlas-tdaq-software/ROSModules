//$Id$
/************************************************************************/
/*									*/
/* File             : EmulatedSequentialDataChannel.cpp			*/
/*									*/
/* Authors          : B.Gorini, M. Joos, J.Petersen CERN		*/
/*									*/
/********* C 2008 - ROS/RCD *********************************************/


#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/EmulatedSequentialDataChannel.h"


using namespace ROS;


/********************************************************************************************/
EmulatedSequentialDataChannel::EmulatedSequentialDataChannel(u_int id,
				   u_int configId,
				   u_int rolPhysicalAddress,
				   bool usePolling,
				   DFCountedPointer<Config> configuration,
				   EmulatedSequentialDataChannelInfo* info) :
  SequentialDataChannel(id, configId, rolPhysicalAddress, usePolling, configuration,info)
/********************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::constructor: Entered");

  m_statistics = info;
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialDataChannel::constructor: m_statistics = " << m_statistics);

  int poolPageSize = configuration->getInt("memoryPoolPageSize", configId);
  m_numberOfDataElements = ((poolPageSize - sizeof(ROBFragment::ROBHeader) -
                             sizeof(RODFragment::RODHeader) - sizeof(RODFragment::RODTrailer)) >> 2 ) // bytes to words
                             - 1;                                                                     // # status elements
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialDataChannel: numberOfDataElements = " << m_numberOfDataElements);

  m_l1id = 0;
  m_triggerReady = false;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::constructor: Done");
}


/*************************************************************/
EmulatedSequentialDataChannel::~EmulatedSequentialDataChannel() 
/*************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::destructor: Entered (empty function).");
}


/*******************************************************************************************************/
int EmulatedSequentialDataChannel::getNextFragment(u_int* buffer, int /*max_size*/, u_long /*pciAddress*/)
/*******************************************************************************************************/
{
  RODFragment::RODHeader *rodheader;
  RODFragment::RODTrailer *rodtrailer;
  u_int loop, *ptr;

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::getNextFragment: Entered.");
  u_int fsize = m_numberOfDataElements + 1;             // # data elements + # status elements
  u_long base = (u_long) buffer;
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialDataChannel::getNextFragment: base = " << HEX(base));

  rodheader = (RODFragment::RODHeader*)base;

  // Build the ROD header
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialDataChannel::getNextFragment: rodheader->startOfHeaderMarker is at " << &rodheader->startOfHeaderMarker);
  rodheader->startOfHeaderMarker = RODFragment::s_rodMarker;
  rodheader->headerSize          = sizeof(RODFragment::RODHeader) / sizeof(int);
  rodheader->formatVersionNumber = RODFragment::s_rodformatVersionNumber;
  rodheader->runNumber           = 0xffffffff;
  rodheader->sourceIdentifier    = 0x12;
  rodheader->level1Id            = m_l1id;
  rodheader->bunchCrossingId     = 0x34;
  rodheader->level1TriggerType   = 0x56;
  rodheader->detectorEventType   = 0x78;

  // Build the ROD body: status element + data
  ptr = (u_int *)(base + sizeof(RODFragment::RODHeader));
  for (loop = 0; loop < fsize; loop++)
    *ptr++ = loop;

  // Build the ROD trailer
  rodtrailer = (RODFragment::RODTrailer*)(base + sizeof(RODFragment::RODHeader) + fsize * sizeof(int));
  rodtrailer->numberOfStatusElements = 1;
  rodtrailer->numberOfDataElements   = m_numberOfDataElements;
  rodtrailer->statusBlockPosition    = 0;

  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialDataChannel::getNextFragment: fsize = " << fsize);

  m_l1id++;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::getNextFragment: Done.");
  return(fsize * sizeof(int) + sizeof(RODFragment::RODHeader) + sizeof(RODFragment::RODTrailer));
}


/*******************************************/
int EmulatedSequentialDataChannel::poll(void)
/*******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::poll: Entered.");
  if (m_triggerReady) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::poll: Done (return 1).");
    return(1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::poll: Done (return 0).");
  return(0);
}


/*****************************************************/
void EmulatedSequentialDataChannel::prepareForRun(void)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::prepareForRun: Entered.");
  m_l1id = 0;
  m_triggerReady = true;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::prepareForRun: Done.");
}


/**********************************************/
void EmulatedSequentialDataChannel::stopEB(void)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::stopEB: Entered.");
  m_triggerReady = false;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::stopEB: Done.");
}

/*****************************************/
void EmulatedSequentialDataChannel::probe()
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::probe: called");
  SequentialDataChannel::probe();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::probe: done");
}

/*************************************************/
ISInfo*  EmulatedSequentialDataChannel::getISInfo()
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::getIsInfo: called");
  return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::getIsInfo: done");
}


/*************************************************/
void EmulatedSequentialDataChannel::clearInfo(void)
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::clearInfo: Entered");
  SequentialDataChannel::clearInfo();
  m_statistics->dummy = 0;                 //Just an example
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialDataChannel::clearInfo: Done");
}
