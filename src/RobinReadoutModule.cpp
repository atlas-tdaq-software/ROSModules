/*********************************************************/
/*  ATLAS ROS Software 			 		 */
/*							 */
/*  Class: RobinReadoutModule 			 	 */
/*  Author: Markus Joos, CERN PH/ESE 			 */
/*							 */
/*** C 2007 - The software with that certain something ***/

#include <new>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModules/ModulesException.h"
#include "ROSModules/RobinReadoutModule.h"
#include "ROSModules/DDTScheduledUserAction.h"
//#include "robin_ppc/robin.h"
#include "ROSInterruptScheduler/RobinInterruptCatcher.h"

using namespace ROS;

bool RobinReadoutModule::s_firstModuleFound = false;
bool RobinReadoutModule::s_DDTCreated = false;

/******************************************/
RobinReadoutModule::RobinReadoutModule(void)
/******************************************/
  : m_memoryPool(0),
    m_robIn(0),
    m_DDTscheduledUserAction(0)
{ 
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::Constructor: called");
}


/***************************************/
RobinReadoutModule::~RobinReadoutModule() noexcept
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::Destructor: called");
  if (m_robIn != 0) 
  {
    std::vector <DataChannel *>::iterator rols;
    for (rols = m_dataChannels.begin(); rols != m_dataChannels.end(); rols++) 
      delete (*rols);

    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::Destructor: m_robIn is at " << HEX(m_robIn));
    m_robIn->freeMsgResources();
    delete m_robIn;
    m_robIn = 0;
  }
  
  if (m_memoryPool != 0)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::Destructor: m_memoryPool is at " << HEX(m_memoryPool));
    delete m_memoryPool; 
    m_memoryPool = 0;
  }  
  
  if (m_DDTscheduledUserAction != 0)
  { 
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::Destructor: m_DDTscheduledUserAction is at " << HEX(m_DDTscheduledUserAction));
    delete m_DDTscheduledUserAction;
    m_DDTscheduledUserAction = 0;
    s_firstModuleFound = false;
    s_DDTCreated = false;
  }

  if (m_configuration != 0)
  {
    RobinInterruptCatcher* interruptCatcher=RobinInterruptCatcher::Instance();
    interruptCatcher->deregister(m_configuration);
  }     
}


/********************************************************************/
void RobinReadoutModule::setup(DFCountedPointer<Config> configuration)
/********************************************************************/
{
  // Get the configuration parameters here
  m_configuration = configuration;
  RobinInterruptCatcher* interruptCatcher=RobinInterruptCatcher::Instance();
  interruptCatcher->setup(m_configuration);
}


/******************************************/
void RobinReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/******************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::prepareForRun: called");

  if (m_numberOfDataChannels)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::prepareForRun: Now resetting m_memoryPool");
    m_memoryPool->reset();
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::prepareForRun: Reset of m_memoryPool OK");
  }

  //Nothing to be done at this level but the data channels should enable the S-Links
  for (u_int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<RobinDataChannel *>(m_dataChannels[chan])->prepareForRun();
 
  // start ECR scanning on enabled links 
  if (m_DDTscheduledUserAction !=0) 
    dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction)->prepareForRun();
}


/***********************************/
void RobinReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/***********************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::stopGathering: called");

  // stop ECR scanning on enabled links 
  if (m_DDTscheduledUserAction !=0) 
    dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction)->stopEB();

  //Nothing to be done at this level but the data channels should disable the S-Links
  for (u_int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<RobinDataChannel *>(m_dataChannels[chan])->stopFE();
}


/**************************************/
void RobinReadoutModule::configure(const daq::rc::TransitionCmd&)
/**************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::configure: called");
  u_int npages = 0;
  u_int pageSize = 0;
  u_int numberOfOutstandingReq = 0;
  u_int msgInputMemorySize = 0;
  u_int miscSize = 0;
  u_int resetRobin = 0;
  u_int db_pagesize;
  std::vector <CfgParm> cfgParms;
 
  m_configuration->dump();

  try 
  {
    //Get the configuration variables for the whole board
    m_numberOfDataChannels = m_configuration->getInt("numberOfChannels");
    if (m_numberOfDataChannels == 0)
    {
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: m_numberOfDataChannels = " << m_numberOfDataChannels);
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Well, in this case I have done my job!");
      return;
    }

    m_physicalAddress      = m_configuration->getUInt("PhysicalAddress");  //This identifies a CARD not a ROL!!!
    float timeout          = m_configuration->getFloat("Timeout");
    numberOfOutstandingReq = m_configuration->getUInt("NumberOfOutstandingReq");  
    msgInputMemorySize     = m_configuration->getUInt("MsgInputMemorySize"); 
    miscSize               = m_configuration->getUInt("MiscSize"); 
    
    try
    {
      resetRobin = m_configuration->getUInt("ResetRobin"); //Just for test_Robin
    } 
    catch(std::exception) {}     

    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: m_numberOfDataChannels = " << m_numberOfDataChannels);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: timeout                = " << timeout);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: m_physicalAddress      = " << m_physicalAddress);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: numberOfOutstandingReq = " << numberOfOutstandingReq);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: msgInputMemorySize     = " << msgInputMemorySize);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: miscSize               = " << miscSize);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: resetRobin             = " << resetRobin);

    //This table links the physical addresses of the ROLs on a ROBIN to the channel numbers. The value "9" means "invalid"   
    m_rolPhysicalAddressTable[0] = 9;
    m_rolPhysicalAddressTable[1] = 9;
    m_rolPhysicalAddressTable[2] = 9;

    for (u_int chan = 0; chan < m_numberOfDataChannels; chan++) 
    {
      npages += m_configuration->getInt("memoryPoolNumPages", chan);

      u_int channelPageSize = m_configuration->getInt("memoryPoolPageSize", chan); 
      if ((chan > 0) && (pageSize != channelPageSize)) 
      {
        CREATE_ROS_EXCEPTION(ex102, ModulesException, PCIROBIN_PSIZE, "RobinReadoutModule::configure: Page size was not the same for all channels in configuration, using the maximum"); 
        ers::warning(ex102);
      }
      if (channelPageSize > pageSize) 
        pageSize = channelPageSize;
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: npages                 = " << npages);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: pageSize               = " << pageSize);

    m_robIn = new Robin(m_physicalAddress, timeout);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: m_robIn is at " << HEX(m_robIn));
    m_robIn->open();  
    if (resetRobin)   //Just for test_Robin
      m_robIn->reset();  
    m_robIn->reserveMsgResources(numberOfOutstandingReq, msgInputMemorySize, miscSize, pageSize * npages);

    // Create a memoryPool structure around the event buffer
    u_long eventBase       = m_robIn->getVirtEventBase();
    u_int physicalAddress = m_robIn->getPhysEventBase();
    m_memoryPool = new WrapperMemoryPool(npages, pageSize, eventBase, physicalAddress);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: m_memoryPool is at " << HEX(m_memoryPool));

    // Now it is time to create the Data Channels
    for (u_int i = 0; i < m_numberOfDataChannels; i++) 
    {
      u_int chId = m_configuration->getInt("ChannelId", i);   
      u_int sdId = m_configuration->getInt("SubDetectorId");   
      u_int rolId = chId + (sdId << 16);   
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Assembled rolId = " << HEX(rolId));

      u_int rolPhysicalAddress = m_configuration->getInt("ROLPhysicalAddress", i);
      m_rolPhysicalAddressTable[i] = rolPhysicalAddress;
  
      // first data channel in the configuration
      if (!s_firstModuleFound && i == 0) 
      {
        m_firstRolPhysicalAddress = rolPhysicalAddress;
        s_firstModuleFound = true;
      }
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: rolId of Channel " << i << " = " << rolId);
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: rolPhysicalAddress of Channel " << i << " = " << rolPhysicalAddress);
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Creating channel");
      
      RobinDataChannel *channel = new RobinDataChannel(rolId, i, rolPhysicalAddress, m_robIn->getRol(rolPhysicalAddress), m_physicalAddress, m_memoryPool);
    
      //Get the UID of the DataChannel. We need it if we want to enable / disable a ROL on the fly
      std::string uid = m_configuration->getString("UID", i);
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: UID of the current channel = " << uid);
      m_uidMap[uid] = channel;
    
      //CRC stuff      
      u_int crc_interval = m_configuration->getInt("CrcCheckInterval", i);   
      m_robIn->getRol(rolPhysicalAddress).setCRCInterval(crc_interval);      
      
      m_dataChannels.push_back(channel);
    } 
       
    //MJ: Dump the content of m_uidMap. It should now have all ROLs
    //std::cout << "Configure: Number of entries in m_uidMap: " << m_uidMap.size() << std::endl; 
    //std::map<std::string, RobinDataChannel *>::iterator it;
    //for (it = m_uidMap.begin(); it != m_uidMap.end(); it++) 
    //{
    //  std::cout << "m_uidMap element = " << (*it).first << ":" << (*it).second << std::endl;
    //}   
       
    // Check the BIST result and the PPC application
    StatisticsBlock status = m_robIn->getRol(0).getStatistics();    
  
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: ROBIN serial number: " << status.serNum);
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: ROBIN software version: 0x" << HEX(status.swVersion));
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: ROBIN FPGA design version: 0x" << HEX(status.designVersion));

    // Do we accept the FPGA F/W?
    u_int fwrevision_number = m_configuration->getUInt("FPGAVersion");  
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: revision_number: 0x" << HEX(fwrevision_number));
    if (fwrevision_number != (u_int)status.designVersion)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: The FPGA F/W of the Robin does not have the required version");
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Expected = 0x" << HEX(fwrevision_number) << "  Found = 0x" << HEX((u_int)status.designVersion));
      CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_OLDFPGAFW, "ERROR: The PPC application of the Robin (physical Address = " 
                           << m_physicalAddress << ") is out of date: Expected = 0x" << HEX(fwrevision_number) << "  Found = 0x" 
			   << HEX((u_int)status.designVersion));
      throw(ex1);
    }

    // Do we accept the PPC code?
    u_int revision_number = m_configuration->getUInt("RevisionNumber");  
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: revision_number: 0x" << HEX(revision_number));
    if (revision_number != (u_int)status.swVersion)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: The PPC application of the Robin does not have the required version");
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Expected = 0x" << HEX(revision_number) << "  Found = 0x" << HEX((u_int)status.swVersion));
      CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_OLDPPCFW, "ERROR: The PPC application of the Robin (physical Address = " 
                           << m_physicalAddress << ") is out of date: Expected = 0x" << HEX(revision_number) << "  Found = 0x" 
			   << HEX((u_int)status.swVersion));
      throw(ex1);
    }
    
    // Checking BIST result!
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Checking BIST result");
    if (status.bistResult != 0) 
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: status.bistResult = 0x" << HEX(status.bistResult));
      if ((status.bistResult & enum_bistError) != 0) 
      {
	DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: ROBIN hardware reported severe error(s) during the builtin self test! ");
        CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_HWERROR, "ERROR: ROBIN hardware reported error during the built-in self test (physical Address = " << m_physicalAddress << ") ! These are: " << getBistErrorString(status.bistResult));
        throw(ex1);
      }
      else 
      {
	DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: WARNING: ROBIN has reported warnings during the builtin self test! These are: " << getBistErrorString(status.bistResult));
	DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Trying to continue");
      }  
    }
    
    // read the parameter names and flags from ROL 0
    cfgParms = m_robIn->getRol(0).getConfig();

    // loop over all entries in the CfgItems list. 
    // check if a Robin or ROL specific value is given in the configuration entry and set the parameter accordingly.
    // First process the static variables
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Processing cfgParms");    
    u_int do_reset = 0;
    for(u_int l_index = 1; l_index < enum_cfgNumItems; l_index++)
    {
      if (!cfgParms[l_index].dynamic)
      {
        if (!cfgParms[l_index].global)
	{
	  DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Unexpected ROL specific static variable " << cfgParms[l_index].name);
          CREATE_ROS_EXCEPTION(ex1,ModulesException,PCIROBIN_NONDYNAMIC,"");
          throw(ex1);
	}
	else
	{
	  try
	  {
	    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Reading value of Robin static parameter " << cfgParms[l_index].name);
	    u_int l_tmpValue = m_configuration->getUInt(cfgParms[l_index].name);  
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: l_tmpValue = " << l_tmpValue);

	    if (l_tmpValue != cfgParms[l_index].value) 
	    {
	       DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Setting Robin static parameter " << cfgParms[l_index].name << " from value " << cfgParms[l_index].value << " to value " << l_tmpValue);
	       m_robIn->getRol(0).setConfig(l_index, l_tmpValue);
	       do_reset = 1;
	    }
	  } 
	  catch(std::exception)
	  { 
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Exception received for parameter " << cfgParms[l_index].name);
            //MJ: Remove this once the NV-RAM has been eliminated
	    if ((cfgParms[l_index].value != cfgParms[l_index].defVal) && (l_index != enum_cfgTempAlarmValue))   
	    {
              CREATE_ROS_EXCEPTION(ex100, ModulesException, PCIROBIN_SETCPDEF, "RobinReadoutModule::configure: Setting parameter " << cfgParms[l_index].name << " from " << cfgParms[l_index].value << " to default = " << cfgParms[l_index].defVal); 	      
	      ers::warning(ex100);
	      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Setting parameter " << cfgParms[l_index].name << " from " << cfgParms[l_index].value << " to default = " << cfgParms[l_index].defVal);
	      m_robIn->getRol(0).setConfig(l_index, cfgParms[l_index].defVal);
	      do_reset = 1;
	    }
	  } 
	}	
      }
    }  
    
    //Reset the Robin to activate the new values of the static parameters 
    if (do_reset)
    {
      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Resetting PPC application after modification of static parameter");
      m_robIn->reset();  
    }
 
 
    //Now process the dynamic parameters  
    for(u_int l_index = 1; l_index < enum_cfgNumItems; l_index++)
    {
      if (cfgParms[l_index].dynamic)
      {
	if (cfgParms[l_index].global)
	{	
	  try
	  {
	    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Reading value of Robin dynamic parameter " << cfgParms[l_index].name);
	    if (l_index == enum_cfgPageSize)
	      db_pagesize = 0;          //Clear page size as we do not want to use an old value
	    u_int l_tmpValue = m_configuration->getUInt(cfgParms[l_index].name);  
	    if (l_index == enum_cfgPageSize)
	      db_pagesize = l_tmpValue; //We have got a valid page size
	    
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Setting Robin dynamic parameter " << cfgParms[l_index].name << " to value " << l_tmpValue);
            m_robIn->getRol(0).setConfig(l_index, l_tmpValue);
	  } 
	  catch(std::exception)
	  {
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Exception seen for dynamic parameter " << cfgParms[l_index].name);
	    //MJ: Remove this once the NV-RAM has been eliminated
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: l_index = " << l_index);
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Current value = " << cfgParms[l_index].value);
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Default value = " << cfgParms[l_index].defVal);
	    if (cfgParms[l_index].value != cfgParms[l_index].defVal)
	    {
	      if (l_index == enum_cfgNumPages)
	      {
	        int npmax = 0;
		if (db_pagesize)  //we have got a page size from the configuration DB
		  npmax = (16 * 1024 * 1024) / db_pagesize;		
		else              //Use the page size that is currently programmed in the Robin
		  npmax = (16 * 1024 * 1024) / cfgParms[enum_cfgPageSize].value;		
		DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Setting parameter " << cfgParms[l_index].name << " from " << cfgParms[l_index].value << " to max. value = " << npmax);
	        m_robIn->getRol(0).setConfig(l_index, npmax);
	      }
	      else
	      {
	        DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Setting parameter " << cfgParms[l_index].name << " from " << cfgParms[l_index].value << " to default = " << cfgParms[l_index].defVal);
	        m_robIn->getRol(0).setConfig(l_index, cfgParms[l_index].defVal);
	      }
	    }
	  } 
	}
	else
	{
          for (u_int i = 0; i < m_numberOfDataChannels; i++) 
	  {
            if (m_rolPhysicalAddressTable[i] > 2)  //Well, this should never happen.....
	    {
	      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: I don't have a physical address for data channel # " << i);
              CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_NOROLPA, "ERROR: There is no ROL physical address for data channel # " 
	      << i << " on ROBIN " << m_physicalAddress);
              throw(ex1);
	    }
	    	    
            try
	    {
	      DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Reading value of ROL#" << i << " dynamic parameter " << cfgParms[l_index].name);
	      u_int l_tmpValue = m_configuration->getUInt(cfgParms[l_index].name, i);  

              DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: Setting ROL dynamic parameter " << cfgParms[l_index].name << " to value " << l_tmpValue << " on the ROL with physical address " << m_rolPhysicalAddressTable[i]);
              m_robIn->getRol(m_rolPhysicalAddressTable[i]).setConfig(l_index, l_tmpValue);
	    } 
	    catch(std::exception)
	    {
   	      //MJ: Remove this once the NV-RAM has been eliminated
	      if ((cfgParms[l_index].value != cfgParms[l_index].defVal) && (l_index != enum_cfgDiscardMode))
	      {
	        DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Setting parameter " << cfgParms[l_index].name << " from " << cfgParms[l_index].value << " to default = " << cfgParms[l_index].defVal << " on the ROL with physical address " << m_rolPhysicalAddressTable[i]);
		m_robIn->getRol(m_rolPhysicalAddressTable[i]).setConfig(l_index, cfgParms[l_index].defVal);
	      }
	    } 
	  }
	}
      }	
    }
    
    //Check parameters for consistency
    cfgParms = m_robIn->getRol(0).getConfig();
    u_int maxfragsize = sizeof(ROBFragment::ROBHeader) + 4 * cfgParms[enum_cfgPageSize].value * cfgParms[enum_cfgMaxRxPages].value;  //in bytes
    if (maxfragsize > pageSize)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: The ROBIN can produce events of up to " << maxfragsize << " bytes");
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: But the ROS has a limit of " << pageSize << " bytes");
      CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_PSIZE, "The ROBIN can produce events of up to " << maxfragsize 
                           << " bytes. But the ROS has a limit of " << pageSize << " bytes");
      throw(ex1);
    }
    
    u_int memused = 4 * cfgParms[enum_cfgNumPages].value * cfgParms[enum_cfgPageSize].value;
    if (memused < 0x4000000) 
    {
      CREATE_ROS_EXCEPTION(ex101, ModulesException, PCIROBIN_MEMINEF, "RobinReadoutModule::configure: The ROBIN has 64 MB per channel but you are only using " << memused << " bytes "); 
      ers::warning(ex101);
    }
  }  
  catch (std::exception & mess) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: Exception received: " << mess.what());
    throw;  
  }        

  //This code is for the special case of running in emulated mode (i.e. the Robin generates the events)
  //in combination with an unsynchronised trigger (e.g. TTC2LAN) and ECRs or 24-bit wrap round
  for (u_int i = 0; i < m_numberOfDataChannels; i++) 
  {     
    u_int forcel1id = m_configuration->getInt("ForceL1ID", i); 
    if (forcel1id)
    {
      //Now that the Robin is fully configured we request the configuration parameters and compute how many
      //fragments it will generate. From that we derive a hash mask and configure the individual channels.
      cfgParms = m_robIn->getRol(m_rolPhysicalAddressTable[i]).getConfig();

      if (cfgParms[enum_cfgRolEmu].value != 1)
      {
        DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::configure: If ForceL1ID is true RolEmu must be set to 1");
	CREATE_ROS_EXCEPTION(ex21, ModulesException, PCIROBIN_INCON, "If ForceL1ID is true RolEmu must also be set to 1");
        throw(ex21);
      }
      dynamic_cast<RobinDataChannel *>(m_dataChannels[i])->setExtraParameters(1);
      m_robIn->getRol(m_rolPhysicalAddressTable[i]).formatBuffer();
    }
  }

  // create the DDT User Action 
  bool triggerQueueFlag = m_configuration->getBool("triggerQueue");
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: triggerQueue = " << triggerQueueFlag);
  if (triggerQueueFlag && s_firstModuleFound && !s_DDTCreated)
  {
    u_int triggerQueueSize = m_configuration->getInt("triggerQueueSize");
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: triggerQueueSize = " << triggerQueueSize);

    u_int triggerQueueUnblockOffset = m_configuration->getInt("triggerQueueUnblockOffset");
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: triggerQueueUnblockOffset = " << triggerQueueUnblockOffset);

    int deltaTimeMs = m_configuration->getInt("RobinProbeInterval");
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::configure: deltaTimeMs = " << deltaTimeMs);
    m_DDTscheduledUserAction = new DDTScheduledUserAction(m_robIn, m_firstRolPhysicalAddress, deltaTimeMs, triggerQueueSize, triggerQueueUnblockOffset);
    std::cout << "RobinReadoutModule::configure: Created DDTScheduledUserAction" << " for Module # " << m_physicalAddress << " channel = " << m_firstRolPhysicalAddress << std::endl;
    s_DDTCreated = true;
  }
}


/****************************************/
void RobinReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::unconfigure: Called");

  //Release the memory pool
  if (m_memoryPool != 0)
  { 
    delete m_memoryPool;
    m_memoryPool = 0;
  }
  
  //Release the Robin
  if (m_robIn != 0) 
  {
    std::vector <DataChannel *>::iterator rols;
    for (rols = m_dataChannels.begin(); rols != m_dataChannels.end(); rols++) 
      delete (*rols);

    m_dataChannels.clear();
    m_robIn->freeMsgResources();
    m_robIn->close();  
    delete m_robIn;
    m_robIn = 0;
  }

  //Delete the DDT User Action Scheduler
  if (m_DDTscheduledUserAction != 0) 
  {
    delete m_DDTscheduledUserAction;
    m_DDTscheduledUserAction = 0;
    s_firstModuleFound = false;
    s_DDTCreated = false;
  }
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::unconfigure: Done");
}


/***********************************************************************************/
void RobinReadoutModule::user(const daq::rc::UserCmd& command)
/***********************************************************************************/
{
  int old_pos = 0, pos = 0;
  std::string rol_id;

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::userCommand: Called with commandName = " << command.commandName());

  if((command.commandName() == "clear") || (command.commandName() == "CLEAR")) {
     for (auto rol_id : command.commandParameters()) {
        std::map<std::string, RobinDataChannel *>::iterator it;
        it = m_uidMap.find(rol_id);
        if (it == m_uidMap.end())
           {
              DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::userCommand: Cannot find data channel for UID = " << rol_id);
           }
        else
           {
              RobinDataChannel *dchannel = (*it).second;
              dchannel->reset();  //Clear all events and reset the S-Link
              DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::userCommand: Channel " << rol_id << " has been reset");
           }
     }
  }
  else {
    DEBUG_TEXT(DFDB_ROSFM, 10, "RobinReadoutModule::userCommand:Illegal command received");
    return;
  }


  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::userCommand: Done");
}


//Note: "disable" in this context means "disable channel" and therefore "enable discard mode"
/************************************************/
void RobinReadoutModule::disable(const std::vector<std::string>& argVec)
/************************************************/
{
   for (auto uid : argVec) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::disable: Called with UID = " << uid);
  
      std::map<std::string, RobinDataChannel *>::iterator it;
      it = m_uidMap.find(uid);
      if (it == m_uidMap.end())
         {
            DEBUG_TEXT(DFDB_ROSFM, 20, "RobinReadoutModule::disable: Cannot find data chanel for UID = " << uid);
         }
      else
         {
            RobinDataChannel *dchannel = (*it).second;
            dchannel->enable();  //Enable discard mode
         }
   }
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::disable: Done");
}


//Note: "enable" in this context means "enable channel" and therefore "disable discard mode"
/***********************************************/
void RobinReadoutModule::enable(const std::vector<std::string>& argVec)
/***********************************************/
{
   for (auto uid : argVec) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::enable: Called with UID = " << uid);

      std::map<std::string, RobinDataChannel *>::iterator it;
      it = m_uidMap.find(uid);
      if (it == m_uidMap.end())
         { 
            DEBUG_TEXT(DFDB_ROSFM, 5, "RobinReadoutModule::enable: Cannot find data chanel for UID = " << uid);
         }
      else
         {
            RobinDataChannel *dchannel = (*it).second;
            dchannel->disable();  //disable discard mode
         }
   }
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::enable: Done");
}


/***************************************************************/
const std::vector<DataChannel *> * RobinReadoutModule::channels()
/***************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::channels: Called");
  return &m_dataChannels;
}


/**********************************/
void RobinReadoutModule::clearInfo() 
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::clearInfo: Entered");

  if (m_DDTscheduledUserAction != 0) 
    dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction)->clearInfo();
}


/**************************************************/
void RobinReadoutModule::publishFullStatistics(void)
/**************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::publishFullStatistics: Entered");

  if (m_DDTscheduledUserAction != 0) 
    dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction)->printTimeHistogram();

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinReadoutModule::publishFullStatistics: Done");
}


/******************************************************************/
std::string RobinReadoutModule::getBistErrorString(u_int bistStatus) 
/******************************************************************/
{
  std::ostringstream errorMessages;

  if ((bistStatus & enum_bistDesIdWrong) != 0) 
    errorMessages << "FPGA design ID wrong";

  if ((bistStatus & enum_bistRolTlkPrbs) != 0) 
    errorMessages << "Error Code: bistRolTlkPrbs";

  if ((bistStatus & enum_bistRolTlkLoop) != 0) 
    errorMessages << "Error Code: bistRolTlkLoop";

  if ((bistStatus & enum_bistBufAddrError) != 0) 
    errorMessages << "Error Code: bistBufAddrError";

  if ((bistStatus & enum_bistBufDataError) != 0) 
    errorMessages << "Error Code: bistBufDataError";

  if ((bistStatus & enum_bistParmError) != 0) 
    errorMessages << "Error Code: bistParmError";

  return errorMessages.str();
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createRobinReadoutModule();
}
ReadoutModule* createRobinReadoutModule()
{
  return (new RobinReadoutModule());
}

