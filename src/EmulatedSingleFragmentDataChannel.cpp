//$Id$
/************************************************************************/
/*									*/
/* File             : EmulatedSingleFragmentDataChannel.cpp		*/
/*									*/
/* Authors          : G. Crone, B.Gorini, J.Petersen CERN		*/
/*									*/
/********* C 2007 - ROS/RCD *********************************************/

#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/EmulatedSingleFragmentDataChannel.h"


using namespace ROS;

/************************************************************************************************/
EmulatedSingleFragmentDataChannel::EmulatedSingleFragmentDataChannel(unsigned int channelId,
				   unsigned int channelIndex,
				   unsigned long rolPhysicalAddress,
				   DFCountedPointer<Config> configuration,
				   EmulatedSingleFragmentDataChannelInfo* info) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration,info)
/************************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::constructor: Entered");

  m_statistics = info;
}


/*********************************************************************/
EmulatedSingleFragmentDataChannel::~EmulatedSingleFragmentDataChannel() 
/*********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::destructor: Entered");
}


/******************************************************************************************/
int EmulatedSingleFragmentDataChannel::getNextFragment(u_int *buffer, int max_size,
                                                       u_int *status, u_long /*pciAddress*/)  //MJ: called by whom?
/******************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::getNextFragment: Entered");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSingleFragmentDataChannel::max_size = " << max_size);

  for (unsigned int i = 0; i < (max_size / sizeof(u_int)); i++) // fill buffer: words - bytes ...
    buffer[i] = i;

  *status = 0x5a5a;

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::leaving getNextFragment");
  return max_size;	// bytes ..
}

/****************************************************************/
  ISInfo*  EmulatedSingleFragmentDataChannel::getISInfo()
/****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::getIsInfo: called");
    return m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSingleFragmentDataChannel::getIsInfo: done");
}


/***********************************************************/
void EmulatedSingleFragmentDataChannel::clearInfo(void)
/***********************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 5, "EmulatedSingleFragmentDataChannel::clearInfo: Entered");
  SingleFragmentDataChannel::clearInfo();

  m_statistics->dummy = 0;
  DEBUG_TEXT(DFDB_ROSFM, 5, "EmulatedSingleFragmentDataChannel::clearInfo: Done");
}
