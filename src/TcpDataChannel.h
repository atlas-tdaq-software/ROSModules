// $Id: TcpDataChannel.h 99058 2011-04-15 10:22:10Z gcrone $

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: TcpDataChannel					*/
/*  Author: M.Joos							*/
/*									*/
/*** C 2005 - Ecosoft: Made from at least 80% recycled source code ******/

#ifndef TCPDATACHANNEL_H
#define TCPDATACHANNEL_H

#include "ROSInfo/EthSequentialDataChannelInfo.h"
#include "ROSCore/SequentialDataChannel.h"

namespace ROS 
{
  class TcpDataChannel : public SequentialDataChannel 
  {
  public:
    TcpDataChannel(u_int id,
                   u_int index,
		             DFCountedPointer<Config> configuration,
                   EthSequentialDataChannelInfo* info = new EthSequentialDataChannelInfo());	// default
    virtual ~TcpDataChannel();
    virtual int getNextFragment(u_int* buffer, int max_size, u_long pciAddress = 0);
    virtual int poll(void);
    void stopEB(void);
    void disconnect(void);
    void connect(int fd);
    void prepareForRun(void);


    void clearInfo(void);

    virtual ISInfo* getISInfo(void);

  private:
    int PollEth(void);        
    void cleanupLink(void);

    bool m_usePolling;
    int m_socket;
    int m_physaddr;
    bool m_triggerReady;

    EthSequentialDataChannelInfo* m_statistics;
  };
}
#endif //TCPDATACHANNEL_H
