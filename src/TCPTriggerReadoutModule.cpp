//$Id$
/************************************************************************/
/*									*/
/* File             : TCPTriggerReadoutModule.cpp			*/
/*									*/
/* Authors          : E. Pasqualucci, J.Petersen 			*/
/*									*/
/********* C 2007 - ROS/RCD  ********************************************/

#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "ROSModules/TCPTriggerReadoutModule.h"
#include "ROSModules/TCPTriggerDataChannel.h"


using namespace ROS;


/************************************************/
TCPTriggerReadoutModule::TCPTriggerReadoutModule() 
/************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::constructor: Entered");

  m_modInfo = 0;
  m_nconnected = 0;
}


/*************************************************/
TCPTriggerReadoutModule::~TCPTriggerReadoutModule() noexcept
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


// setup gets parameters from configuration
/*************************************************************************/
void TCPTriggerReadoutModule::setup(DFCountedPointer<Config> configuration) 
/*************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::setup: Entered");

  m_configuration = configuration;

  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);
  if (m_numberOfDataChannels > 1) 
  {
    char mytext[100];
    sprintf(mytext, " # channels = %d \n", m_numberOfDataChannels);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_ILLCHAN, mytext);
    throw(ex1);
  }

  u_int iport      = configuration->getInt ("TriggerSourcePort");
  m_tout           = configuration->getInt ("ConnectTimeout");

  m_port = (u_short) iport;

  std::cout << "TCPTriggerReadoutModule::setup : timeout " << m_tout << " port " << m_port << std::endl;  //MJ: cout?

  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
  }

  for (int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_id[chan] = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerReadoutModule::setup: chan = "
                                << chan << "  id = " << m_id[chan] << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
  }
}


/*******************************************/
void TCPTriggerReadoutModule::configure(const daq::rc::TransitionCmd&)
/*******************************************/
{
  // start the server

  int val = 1;
  struct sockaddr_in sinme;

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: Entered");

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {

    // chan == the Config Id
    TCPTriggerDataChannel *channel = new TCPTriggerDataChannel(
						 m_id[chan],
					         chan,
						 m_rolPhysicalAddress[chan],
						 m_configuration);
    m_dataChannels.push_back(channel);
    dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->enable (false);
  }

  //create socket
  if ((m_fd0 = socket (AF_INET, SOCK_STREAM, 0)) < 0)
  {
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SOCKETERROR, strerror(errno));
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: socket created");

  // set socket options
  if (setsockopt (m_fd0, SOL_SOCKET, SO_REUSEADDR, (char *) &val, sizeof (val)) < 0)
  {
    close (m_fd0);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SOCKETOPTERROR, strerror(errno));
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: socket option SO_REUSEADDR done ");

  if (setsockopt (m_fd0, getprotobyname ("tcp")->p_proto, TCP_NODELAY, (char *) &val, sizeof (val)) < 0)
  {
    close (m_fd0);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SOCKETOPTERROR, strerror(errno));
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: socket option TCP_NODELAY done");

  if (fcntl (m_fd0, F_SETFL, O_NONBLOCK) < 0)
  {
    close (m_fd0);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SOCKETOPTERROR, strerror(errno));
    throw(ex1);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: socket option O_NONBLOCK done");

  // bind host
  sinme.sin_family      = AF_INET;
  sinme.sin_addr.s_addr = INADDR_ANY;
  sinme.sin_port        = htons (m_port);

  if (bind (m_fd0, (struct sockaddr *) &sinme, sizeof (sinme)) < 0)
  {
    close (m_fd0);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_BINDERROR, strerror(errno));
    throw(ex1);
  }

  // listen
  if (listen (m_fd0, SOMAXCONN) < 0)
  {
    close (m_fd0);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_LISTENERROR, strerror(errno));
    throw(ex1);
  }

  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->descriptor(m_fd0);

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::configure: Done");
}


/*********************************************/
void TCPTriggerReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::unconfigure: Entered");

  //-------------------------------------------------------------
  //  WHY DOESN'T THIS UNDO ALL THE STUFF DONE IN CONFIGURE??????  //MJ: ??
  //-------------------------------------------------------------

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::unconfigure: Done");
}


/*****************************************/
void TCPTriggerReadoutModule::connect(const daq::rc::TransitionCmd&)
/*****************************************/
{
  int dt = 0, tout = m_tout * 1000000;
  int acceptCount = 0;
  struct timeval t0, t1;

  DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerReadoutModule::connect: Entered");

  gettimeofday(&t0, 0);

  m_fdmax = 0;
  m_fdmin = FD_SETSIZE;
  FD_ZERO(&m_fds);

  while (dt < tout)
  {
    int fd;
    struct sockaddr sinhim;
    socklen_t slen = sizeof (sinhim);

    if ((fd = accept (m_fd0, &sinhim, &slen)) > 0)
    {
      DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerReadoutModule::connect: fd = " << fd);
      int val = 1;
      struct sockaddr_in *s = (struct sockaddr_in *) &sinhim;
      std::cout << " Connection accepted from node " << inet_ntoa (s->sin_addr) << " fd = " << fd << std::endl;

      if (setsockopt (fd, getprotobyname ("tcp")->p_proto, TCP_NODELAY, (char *) &val, sizeof (val)) < 0)
      {
        close (fd);
        CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SOCKETOPTERROR, strerror(errno));
        throw(ex1);
      }

      m_fdmax = m_fdmax > fd ? m_fdmax : fd;
      m_fdmin = fd < m_fdmin ? fd : m_fdmin;
      FD_SET (fd, &m_fds);
    }

    acceptCount++;
    gettimeofday (&t1, 0);

    dt = (t1.tv_sec * 1000000 + t1.tv_usec) - (t0.tv_sec * 1000000 + t0.tv_usec);
  }

  std::cout << " # accept calls = " << acceptCount << std::endl;
  std::cout << "TCPTriggerReadoutModule::connect: m_fdmax = " << m_fdmax << " m_fdmin = " << m_fdmin << std::endl;

  if (m_fdmax == 0) // nobody connected
    SEND_ROS_INFO("TCPTriggerReadoutModule::connect: no clients connected");

  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdset(&m_fds);
  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdmax(m_fdmax);
  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdmin(m_fdmin);

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::connect: Done");
}


/********************************************/
void TCPTriggerReadoutModule::disconnect(const daq::rc::TransitionCmd&)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::disconnect: Entered");
 
  fd_set *p;

  p       = dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdset();
  m_fdmax = dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdmax();
  m_fdmin = dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->fdmin();

  memcpy (&m_fds, p, sizeof (fd_set));

  for (int i = m_fdmin ; i < (m_fdmax + 1); ++i)
    if (FD_ISSET (i, &m_fds))
    {
      close (i);
      FD_CLR (i, &m_fds);
      DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::disconnect: closed & CLRed fd = " << i);
    }
 
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::disconnect: Done");
}
    

/***********************************************/    
void TCPTriggerReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/***********************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::prepareForRun: Entered. Empty function");
  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->enable (true);
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::prepareForRun: Exited");
}


/****************************************/
void TCPTriggerReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::stopGathering: Entered. Empty function");
  dynamic_cast<TCPTriggerDataChannel *>(m_dataChannels[0])->enable (false);
}


/************************************************************/
DFCountedPointer < Config > TCPTriggerReadoutModule::getInfo()
/************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::getInfo: Entered");
  DFCountedPointer<Config> configuration = Config::New();
  return( configuration);
}


/***************************************/
void TCPTriggerReadoutModule::clearInfo()
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerReadoutModule::clearInfo  Entered. Empty function");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createTCPTriggerReadoutModule();
}
ReadoutModule* createTCPTriggerReadoutModule()
{
  return (new TCPTriggerReadoutModule());
}
