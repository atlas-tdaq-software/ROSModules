//$Id$


/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  File: EthSequentialReadoutModule.cpp				*/
/*  Author: M.Joos							*/
/*									*/
/*** C 2005 - Ecosoft: Made from at least 80% recycled source code ******/

#include <unistd.h>
#include <iostream>
#include "ROSModules/EthSequentialDataChannel.h"
#include "ROSModules/EthSequentialReadoutModule.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;


/******************************************************/
EthSequentialReadoutModule::EthSequentialReadoutModule() 
/******************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::constructor: Called");
}


/*******************************************************/
EthSequentialReadoutModule::~EthSequentialReadoutModule() noexcept
/*******************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::destructor: Done");
}


/****************************************************************************/
void EthSequentialReadoutModule::setup(DFCountedPointer<Config> configuration) 
/****************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::setup: Entered");
  
  m_configuration        = configuration;
  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);
  
  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    int d1, d2;
    d1 = configuration->getInt("channelId", chan);
    d2 = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::setup: channelId              = " << d1);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::setup: channelPhysicalAddress = " << d2);
    m_id.push_back(d1);
    m_rolPhysicalAddress.push_back(d2);
    m_usePolling.push_back(true);
    DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialReadoutModule::setup: chan = " << chan << "  id = " << d1 << " rolPhysicalAddress = " << d2);
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::setup: Done");
}


/**********************************************/
void EthSequentialReadoutModule::configure(const daq::rc::TransitionCmd&)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: Entered");
  
  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: Now creating a new data channel. Parameters are:");
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: chan                 = " << chan);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: m_id                 = " << m_id[chan]);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: m_rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: m_usePolling         = " << m_usePolling[chan]);

    EthSequentialDataChannel *channel = new EthSequentialDataChannel(m_id[chan], chan, m_rolPhysicalAddress[chan], m_usePolling[chan], m_configuration);
    m_dataChannels.push_back(channel);
    DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: Data channel created");
  }

  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->configure();

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::configure: Done");
}


/************************************************/
void EthSequentialReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::unconfigure: Entered");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->unconfigure();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::unconfigure: Done");
}


/********************************************/
void EthSequentialReadoutModule::connect(const daq::rc::TransitionCmd&)
/********************************************/
{
 
  DEBUG_TEXT(DFDB_ROSFM, 20, "EthSequentialReadoutModule::connect: Entered");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->connect();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::connect: Done");
}


/***********************************************/
void EthSequentialReadoutModule::disconnect(const daq::rc::TransitionCmd&)
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::disconnect: Entered");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->disconnect();  
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::disconnect: Done");
}
    

/**************************************************/    
void EthSequentialReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/**************************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::prepareForRun: Entered");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->prepareForRun();  
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::prepareForRun: Done");
}


/*******************************************/
void EthSequentialReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/*******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::stopFE: Called");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->stopEB();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::stopFE: Done");
}


/******************************************/
void EthSequentialReadoutModule::clearInfo()
/******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::clearInfo:  Entered");

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
    dynamic_cast<EthSequentialDataChannel *>(m_dataChannels[chan])->clearInfo();

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialReadoutModule::clearInfo:  Done");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createEthSequentialReadoutModule();
}
ReadoutModule* createEthSequentialReadoutModule()
{
  return (new EthSequentialReadoutModule());
}
