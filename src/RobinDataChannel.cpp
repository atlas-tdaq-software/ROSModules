/*********************************************************/
/*  ATLAS ROS Software 			 		 */
/*							 */
/*  Class: RobinDataChannel 			 	 */
/*  Author: Markus Joos, CERN PH/ESE 			 */
/*							 */
/*** C 2007 - The software with that certain something ***/
     
#include "ROSModules/RobinDataChannel.h"
#include "ROSModules/ModulesException.h"
#include "ROSRobin/Rol.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFThreads/DFThread.h"
#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_time_stamp/tstamp.h"
//#include "robin_ppc/robin.h"

#define PCI_ROBIN_SYNCHRONIZE

using namespace ROS;

/*********************************************************************************/
RobinDataChannel::RobinDataChannel(u_int id, u_int configId, u_int physicalAddress, 
                                   Rol & rol_ptr, u_int RobinphysicalAddress,
				   WrapperMemoryPool *mpool) :
/*********************************************************************************/
  DataChannel(id, configId, physicalAddress),
  m_memoryPool(mpool),
  m_rol(rol_ptr),
  m_requestMutex(0),
  m_gcdeleted(0),
  m_gcfree(0),
  m_id(id),
  m_l1id_modulo(0),
  m_RobinphysicalAddress(RobinphysicalAddress),
  m_tsStopLast({0,0,0}),
  m_numberOfLevel1Last(0)
{
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::constructor: Called");
  // Get the request mutex to protect the Fragment memory allocation

  for (u_int bit = 0; bit < 32; bit++)
    m_statistics.statusErrors.push_back(0);
  
  m_statistics.numberOfMayCome     = 0;
  m_statistics.numberOfNeverToCome = 0;
  m_statistics.numberOfNotDeleted  = 0;
  m_statistics.gcOK                = 0;
  m_statistics.gcBad               = 0;
  m_statistics.gcAvDeleted         = 0;
  m_statistics.gcAvFree            = 0;
  m_statistics.robId               = id & 0xffff;
  m_statistics.pagesFree           = 0;
  m_statistics.pagesInUse          = 0;
  m_statistics.mostRecentId        = 0;
  m_statistics.rolXoffStat         = 0;
  m_statistics.rolDownStat         = 0;
  m_statistics.bufferFull          = 0;
  m_statistics.fragsreceived       = 0;
  m_statistics.fragsreplaced       = 0;
  m_statistics.fragstruncated      = 0;
  m_statistics.fragscorrupted      = 0;
  m_statistics.level1RateHz        = 0;
  m_statistics.lastmsg             = 0;
  m_statistics.lastl1id            = 0;
  m_statistics.numberOfXoffs       = 0;
   
  rdw1 = 0;
  rdw2 = 0;
  rdw3 = 0;
  rdw4 = 0;
  hdw1 = 0;
  hdw2 = 0;
  hdw3 = 0;
  hdw4 = 0;
  
  m_requestMutex = DFFastMutex::Create((char *) "REQUESTMUTEX");
  if (configId == 0)
    m_infoflag = 1;
  else
    m_infoflag = 0;

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::constructor: m_memoryPool has " << m_memoryPool->numberOfPages() << " pages");
  m_pageIndex = new MemoryPage*[m_memoryPool->numberOfPages()];
  m_ticketPool = new u_int[m_memoryPool->numberOfPages()];
  for (int ticket = 0; ticket < m_memoryPool->numberOfPages(); ticket++) 
     m_ticketPool[ticket] = ticket;
  m_nextTicket = 0;

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::constructor: Done");
}


/***********************************/
RobinDataChannel::~RobinDataChannel()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::destructor: Called");
  m_requestMutex->destroy();
  delete [] m_pageIndex;
  delete [] m_ticketPool;
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::destructor: Done");
}


/***********************************/
ISInfo* RobinDataChannel::getISInfo()
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::getISInfo: called");
  return &m_statistics;
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::getISInfo: done");
}


/**************************************************/
void RobinDataChannel::setExtraParameters(int value)
/**************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::setExtraParameters: Called");
  m_l1id_modulo = value;
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::setExtraParameters: Done");
}


/*************************************************/
int RobinDataChannel::requestFragment(int level1Id)
/*************************************************/
{
  MemoryPage *mem_page;

  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::requestFragment (ROL " << m_id << "): Entering with L1ID = " << level1Id);

  // Lock the request mutex to allocate some memory for the requested fragment
  m_requestMutex->lock();
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::requestFragment: request Mutex locked");

  // Allocate memory for the fragment
  mem_page = m_memoryPool->getPage();
  
  u_int ticket = m_ticketPool[m_nextTicket++];
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::requestFragment: ticket = " << ticket);
  m_pageIndex[ticket] = mem_page;

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::requestFragment: Empty page created");
  m_requestMutex->unlock();
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::requestFragment: request Mutex unlocked");

  // Check if the allocated page is OK
  if (mem_page == 0) 
  {						
    DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDataChannel::requestFragment(ROL " << m_id << "): Failed to get a page from the memory pool");
    CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_NOPAGE, "");
    throw(ex1);
  }

  // Get the memory page start address and reserve the max available contiguous space.
  u_int *fragBufferAddress = static_cast<u_int *> (mem_page->reserve(mem_page->capacity()));

  // Call the Robin to get the fragment
  // TO BE DONE: shouldn't the data transfer be protected against possible transfers of data larger than the page size?
  m_rol.requestFragment(level1Id, reinterpret_cast<u_long> (fragBufferAddress));

  m_statistics.lastmsg = REQUESTFRAGMENT;
  m_statistics.lastl1id = level1Id;

  //Remember the L1ID for getFragment
  mem_page->setUserdata(level1Id);

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::requestFragment: Returned ticket = " << ticket);
  return (ticket);
}


/*******************************************************/
EventFragment * RobinDataChannel::getFragment(int ticket)
/*******************************************************/
{
  u_int l1id1, l1id2;
  MemoryPage *mem_page;

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: called with ticket = " << ticket);

  mem_page = m_pageIndex[ticket];
  u_long poll_address = reinterpret_cast<u_long> (mem_page->address());

  //Wait for the fragment
  m_rol.getFragment(poll_address);  //MJ: seems we don't need the return value of Rol::getFragment
  
  //Now release the extra allocated memory from the Buffer
  //BEWARE: here there is no check: we assume that the RobIn wrote at most up to the available memory
  //DO NOT MOVE THOSE LINES TO AFTER THE CREATION OF THE ROBFragment!!!!!!!
  //OTHERWISE THE REFERENCE COUNT ON THE MEMORY PAGE WOULD HAVE BEEN INCREASED TO MORE THAN 1
  //HENCE THE RESERVE/RELEASE OPERATIONS WOULD HAVE BEEN DISABLED
  u_int fragmentSize = (*((u_int *)(u_long)(poll_address + sizeof(u_int)))) * sizeof(u_int);
  mem_page->release(mem_page->usedSize() - fragmentSize);   
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: Parameters for mem_page->release:");
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: mem_page->usedSize() = " << mem_page->usedSize());
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: fragmentSize         = " << fragmentSize);

  //Seems to be the right time to create the robFragment
  //BG a mutex should not be needed here          
  m_requestMutex->lock();
  ROBFragment *robFragment = new ROBFragment(mem_page); 

  //Now the page is locked by the ROBFragment: we can release it 
  mem_page->free();

  // Return ticket to pool while mutex still locked
  m_ticketPool[--m_nextTicket] = ticket;

  //BG a mutex should not be needed here          
  m_requestMutex->unlock();

  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: robFragment->size (in words) = 0x" << HEX(robFragment->size()));
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: fragment status = 0x" << HEX(robFragment->status()));
  //First of all let's have a look at some errors
  u_int statusword;
  u_int isok = robFragment->checkstatus(&statusword);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment(ROL " << m_id << "): ROB status word = " << HEX(statusword) << " isok = " << isok);
  if (isok && statusword)
  { 
    //Update the error array
    for (u_int bit = 0; bit < 32; bit++)
      if (statusword & (1 << bit))
        m_statistics.statusErrors[bit]++;
  }
  if (!isok)
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDataChannel::getFragment: fragment is too small");
    CREATE_ROS_EXCEPTION(ex15, ModulesException, PCIROBIN_ROBTOOSMALL, "RobinDataChannel: ROB fragment too small: The ROL ID is " << m_id);
    throw(ex15);
  }
 
  if ((robFragment->status() & 0xf0000000) == enum_fragStatusLost) 
  {
    m_statistics.numberOfNeverToCome++;
    DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDataChannel::getFragment(ROL " << m_id << "): The fragment with L1ID = " << robFragment->level1Id() << " has been lost");

    u_int sid = robFragment->sourceIdentifier();
    u_int lid = robFragment->level1Id();    
    CREATE_ROS_EXCEPTION(ex10, ModulesException, PCIROBIN_ROBSW, "RobinDataChannel: Lost fragment detected. The L1ID is 0x" << HEX(lid) << ". The ROB Source ID is 0x" << HEX(sid));
    ers::warning(ex10);
  }

  if ((robFragment->status() & 0xf0000000) == enum_fragStatusPending) 
  {	
    DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::getFragment(ROL " << m_id << "): The requested fragment has not yet arrived");
    m_statistics.numberOfMayCome++;
  }
  
  l1id1 = mem_page->getUserdata();
  if (m_l1id_modulo)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: Setting L1ID to " << l1id1);
    robFragment->level1Id(l1id1);
  }
  else
  {
    //Check if the L1ID is the expected one
    l1id2 = robFragment->level1Id();
    DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: Compairing L1IDs " << l1id1 << " and " << l1id2);
    if (l1id1 != l1id2)
//    if ((l1id1 != l1id2) || ((l1id1 & 0xffff) == 0))
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDataChannel::getFragment(ROL " << m_id << "): Wrong L1ID " << l1id2 << " received but " << l1id1 << " expected");
      
      
      //Set additional error bit in ROB status word
      u_int rob_status = robFragment->status();
      rob_status = rob_status | 0x2;
      robFragment->setStatus(rob_status);
      m_statistics.statusErrors[1]++;   //Log the error for the second bit in the generic part of the status word

      
      CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_ILLL1ID, "ERROR in RobinDataChannel::getFragment(ROL " << m_id << "): Wrong L1ID 0x" << HEX(l1id2) << " received but 0x" << HEX(l1id1) << " expected.");
      ers::error(ex1);
      //throw(ex1);
    }
  }

  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::getFragment(ROL " << m_id << "): The L1ID is " << robFragment->level1Id());
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::getFragment: Done.");
  return(robFragment);
}


/**************************************************************************/
void RobinDataChannel::releaseFragment(const std::vector <u_int> *level1Ids)
/**************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragment(1): Called");
  
  std::vector<u_int>::const_iterator first = level1Ids->begin();
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::releaseFragment(1, ROL " << m_id << "): first L1ID = " << *first);

  m_statistics.numberOfNotDeleted += m_rol.releaseFragment(level1Ids);  //Blocking
  m_statistics.lastmsg = RELEASEFRAGMENT;
  m_statistics.lastl1id = *first;

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragment(1): Done ");
}


/*****************************************************************************/
void RobinDataChannel::releaseFragmentAll(const std::vector <u_int> *level1Ids)
/*****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragmentAll: Called");
  
  std::vector<u_int>::const_iterator first = level1Ids->begin();
  DEBUG_TEXT(DFDB_ROSFM, 10, "RobinDataChannel::releaseFragmentAll(ROL " << m_id << "): first L1ID = " << *first);

  m_statistics.numberOfNotDeleted += m_rol.releaseFragmentAll(level1Ids);  //Blocking
  m_statistics.lastmsg = RELEASEFRAGMENT;
  m_statistics.lastl1id = *first;

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragmentAll: Done");
}


/**************************************************/
void RobinDataChannel::releaseFragment(int level1Id)
/**************************************************/
{
  std::vector<u_int> level1Ids;

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragment(2): Called ");

  level1Ids.push_back((u_int)level1Id);  
  releaseFragment(&level1Ids);
  m_statistics.lastmsg = RELEASEFRAGMENT;
  m_statistics.lastl1id = level1Id;
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::releaseFragment(2): Done ");
}


/**************************************************/    
int RobinDataChannel::collectGarbage(u_int oldestId)
/**************************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::collectGarbage: Called for oldestId = " << oldestId);
  Rol::GCBlock res = m_rol.collectGarbage(oldestId);
  m_statistics.lastmsg = COLLECTGARBAGE;
  m_statistics.lastl1id = oldestId;
  
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::collectGarbage: res.done    = " << res.done);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::collectGarbage: res.deleted = " << res.deleted);
  DEBUG_TEXT(DFDB_ROSFM, 20, "RobinDataChannel::collectGarbage: res.free    = " << res.free);

  if (res.done)
  {
    m_statistics.gcOK++;
    m_gcdeleted += res.deleted;
  }  
  else
    m_statistics.gcBad++;
  
  m_gcfree += res.free;
  m_statistics.gcAvDeleted = m_gcdeleted / (m_statistics.gcOK + m_statistics.gcBad);
  m_statistics.gcAvFree = m_gcfree / (m_statistics.gcOK + m_statistics.gcBad);
    
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::collectGarbage: Done ");
  return(res.done);
}


/****************************************/
void RobinDataChannel::prepareForRun(void)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::prepareForRun: Called");
  
  if (!m_l1id_modulo) //if mode = Rolemu the link must not be enabled
  {
    m_rol.reset();
    m_rol.setConfig(enum_cfgRolEnabled, 1);
    m_rol.clearStatistics();
  }
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::prepareForRun: S-Link enabled");
}


/*********************************/
void RobinDataChannel::stopFE(void)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::stopFE: Called");
  m_rol.setConfig(enum_cfgRolEnabled, 0);
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::stopFE: S-Link disabled");
}


/****************************/
void RobinDataChannel::probe()
/****************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::probe: Entered");
  
  StatisticsBlock statistics = m_rol.getStatistics();

  //The Robin specific parameters should only be processed once
  if (m_infoflag)
  {
    //Check for over temperature
    if (statistics.tempOk == 0)
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "RobinDataChannel::probe: Robin is too hot");
      CREATE_ROS_EXCEPTION(ex11, ModulesException, PCIROBIN_OVTMP, "RobinDataChannel: Over temperature flag set on Robin " << m_RobinphysicalAddress);
      ers::warning(ex11);
    } 
  }
  
  // This is just a selection of the most interesting parameters
  //MJ: 64-BIT PARAMETERS HAVE TO BE WORD-SWAPPED!!!!!!!!!!!!!!!!!!!!!!!!!!
  m_statistics.pagesFree      = statistics.pagesFree;    
  m_statistics.pagesInUse     = statistics.pagesInUse;    
  m_statistics.mostRecentId   = statistics.mostRecentId;    
  m_statistics.rolXoffStat    = statistics.rolXoffStat;    
  m_statistics.rolDownStat    = statistics.rolDownStat;    
  m_statistics.bufferFull     = statistics.bufferFull;  
  m_statistics.numberOfXoffs = statistics.errors[enum_errXoff];

  //Create some 64-bit counters locally. The Robin cannot do it yet (fast enough)
  if (rdw1 > statistics.fragStat[enum_fragsReceived])
    hdw1++;  
  rdw1 = statistics.fragStat[enum_fragsReceived];
  m_statistics.fragsreceived = ((u_int64_t)hdw1 << 32) | (u_int64_t)statistics.fragStat[enum_fragsReceived];
  
  if (rdw2 > statistics.fragStat[enum_fragsTruncated])
    hdw2++;  
  rdw2 = statistics.fragStat[enum_fragsTruncated];
  m_statistics.fragstruncated = ((u_int64_t)hdw2 << 32) | (u_int64_t)statistics.fragStat[enum_fragsTruncated];
  
  if (rdw3 > statistics.fragStat[enum_fragsCorrupted])
    hdw3++;  
  rdw3 = statistics.fragStat[enum_fragsCorrupted];
  m_statistics.fragscorrupted = ((u_int64_t)hdw3 << 32) | (u_int64_t)statistics.fragStat[enum_fragsCorrupted];

  if (rdw4 > statistics.fragStat[enum_fragsReplaced])
    hdw4++;  
  rdw4 = statistics.fragStat[enum_fragsReplaced];
  m_statistics.fragsreplaced = ((u_int64_t)hdw4 << 32) | (u_int64_t)statistics.fragStat[enum_fragsReplaced];
  
  // level1 rate (for this channel)
  tstamp tsStop;
  ts_clock(&tsStop);

  u_int64_t numberOfLevel1Delta;
  if (m_numberOfLevel1Last == 0xffffffffffffffffLL)
    numberOfLevel1Delta = m_statistics.fragsreceived;
  else
    numberOfLevel1Delta = m_statistics.fragsreceived - m_numberOfLevel1Last;

  m_statistics.level1RateHz = (u_int)(numberOfLevel1Delta / (float)ts_duration(m_tsStopLast, tsStop));
  m_tsStopLast = tsStop;
  
  m_numberOfLevel1Last = m_statistics.fragsreceived;
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::probe: Done");
}


/***************************************************/
u_int64_t RobinDataChannel::swap64(u_int64_t data_in)
/***************************************************/
{
  u_int64_t w1, w2;

  w1 = data_in & 0xffffffff;
  w2 = data_in >> 32;
  return((w1 << 32) | w2);
}


/********************************/
void RobinDataChannel::clearInfo()
/********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::clearInfo: Entered");
  
  for (u_int bit = 0; bit < 32; bit++)
    m_statistics.statusErrors[bit] = 0;

  m_statistics.numberOfMayCome     = 0;
  m_statistics.numberOfNeverToCome = 0;
  m_statistics.numberOfNotDeleted  = 0;
  m_statistics.gcOK                = 0;
  m_statistics.gcBad               = 0;
  m_statistics.gcAvDeleted         = 0;
  m_statistics.gcAvFree            = 0;
  m_statistics.pagesFree           = 0;
  m_statistics.pagesInUse          = 0;
  m_statistics.mostRecentId        = 0;
  m_statistics.rolXoffStat         = 0;
  m_statistics.rolDownStat         = 0;
  m_statistics.bufferFull          = 0;
  m_statistics.fragsreceived       = 0;
  m_statistics.fragsreplaced       = 0;
  m_statistics.fragstruncated      = 0;
  m_statistics.fragscorrupted      = 0;
  m_statistics.level1RateHz        = 0;
  m_statistics.lastmsg             = 0;
  m_statistics.lastl1id            = 0;
  m_statistics.numberOfXoffs       = 0;

  rdw1 = 0;
  rdw2 = 0;
  rdw3 = 0;
  rdw4 = 0;
  hdw1 = 0;
  hdw2 = 0;
  hdw3 = 0;
  hdw4 = 0;

  ts_clock(&m_tsStopLast);
  m_numberOfLevel1Last = 0xffffffffffffffffLL;

  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::clearInfo: Done");
}


/*****************************/
void RobinDataChannel::enable()
/*****************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::enable: Called ");
  m_rol.configureDiscardMode(1);
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::enable: Done ");
}


/******************************/
void RobinDataChannel::disable()
/******************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::disable: Called ");
  m_rol.configureDiscardMode(0);
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::disable: Done ");
}


/****************************/
void RobinDataChannel::reset()
/****************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::reset: Called ");
  m_rol.reset();
  DEBUG_TEXT(DFDB_ROSFM, 15, "RobinDataChannel::reset: Done ");
}



 
