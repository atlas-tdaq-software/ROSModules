// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.20  2008/07/18 11:12:20  gcrone
//  Specify namespace std:: where necessary
//
//  Revision 1.19  2008/04/10 13:45:43  gcrone
//  Bug fix in loop over robs in unpackFragments.
//
//  Revision 1.18  2008/02/29 12:24:34  gcrone
//  Update for eformat 4.0
//
//  Revision 1.17  2007/05/30 08:16:36  gcrone
//  Load from separate file per ROB if available.  Use ROB source ID not ROD!
//
//  Revision 1.16  2007/05/02 13:53:12  gcrone
//  Use exceptions for warning messages
//
//  Revision 1.15  2007/04/19 13:12:18  gcrone
//  Use ROS specific data file if it exists
//
//  Revision 1.14  2006/11/28 15:34:25  jorgen
//   errors via ERS; fix byte/word problem in SingleFragment
//
//  Revision 1.13  2006/11/10 12:45:05  gcrone
//  Use correct wrap point in modulo event id (max+1).
//
//  Revision 1.12  2006/11/01 14:37:29  gcrone
//  Use new ROSErrorReporting macros instead of dfout
//
//  Revision 1.11  2006/08/22 16:50:41  gcrone
//  Recycle events with id modulo max id loaded
//
//  Revision 1.10  2006/08/02 15:32:52  gcrone
//  Suppress some compiler warnings.
//
//  Revision 1.9  2006/05/15 09:31:36  gcrone
//  Fix fragment delete in unconfigure (add []).
//
//  Revision 1.8  2005/11/15 11:09:41  gcrone
//  change cerr to DEBUG_TEXT
//
//  Revision 1.7  2005/07/20 11:22:20  gcrone
//  Delete buffer after use in configure(). Also added some debug
//
//  Revision 1.6  2005/07/10 17:56:31  gcrone
//  Update for new eformat library.  Now compiles but not yet tested.
//
//  Revision 1.5  2005/01/22 15:56:18  jorgen
//   update to DataChannel interface with config ID
//
//  Revision 1.4  2004/12/12 10:40:58  gcrone
//  Updated for new DF_ALLOCATOR macro
//
//  Revision 1.3  2004/03/31 16:23:46  gcrone
//  Rename initialise to configure and add unconfigure.  Delete EventMap
//  in unconfigure rather than destructor.  Fix bug - delete empty event
//  maps after deleting their contents!
//
//  Revision 1.2  2004/03/24 20:26:07  gcrone
//  Idiot: Remember to create the mutex in the constructor!
//
//  Revision 1.1  2004/03/24 19:03:09  gcrone
//  Added Preloaded ReadoutModule and DataChannel
//
//
// //////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "ROSModules/PreloadedDataChannel.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"

#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"

#include "EventStorage/pickDataReader.h"


using namespace ROS;

std::map<unsigned int, PreloadedDataChannel::EventMap> PreloadedDataChannel::s_store;
unsigned int PreloadedDataChannel::s_storeSize=0;
bool PreloadedDataChannel::s_initialised=false;
std::set<unsigned int> PreloadedDataChannel::s_rolIdList;
unsigned int PreloadedDataChannel::s_maxLevel1=0;

PreloadedDataChannel::PreloadedDataChannel(const int rolId,
					   const int configId,
                                           const int npages,
                                           const int pageSize,
                                           const int poolType):
   DataChannel(rolId,configId,0), m_rolId(rolId)
{
   DEBUG_TEXT(DFDB_ROSFM, 15, "PreloadedDataChannel  constructor entered rolId="<<std::hex<<rolId<<std::dec);
   s_rolIdList.insert(m_rolId);

   if (poolType == MemoryPool::ROSMEMORYPOOL_MALLOC) {
      m_memoryPool=new MemoryPool_malloc(npages, pageSize);
   }
   else if (poolType == MemoryPool::ROSMEMORYPOOL_CMEM_BPA ||
            poolType == MemoryPool::ROSMEMORYPOOL_CMEM_GFP) {
      m_memoryPool=new MemoryPool_CMEM(npages, pageSize, poolType);
   }
   else {
      // Raise an exception 'cos we don't know how to alloc memory pool
      std::cerr << "PreloadedDataChannel::configure() Unrecognised memory pool type\n";
   }

   char mutexName[]="REQUESTMUTEX";
   m_mutex = DFFastMutex::Create(mutexName);
}

PreloadedDataChannel::~PreloadedDataChannel()
{
   s_rolIdList.erase(s_rolIdList.find(m_rolId));
   m_mutex->destroy();
}

void PreloadedDataChannel::loadFile(const std::string& fileSpec)
{
   DEBUG_TEXT(DFDB_ROSFM, 10, "Loading data from " << fileSpec);
   struct stat statBuf;
   // Check that the input file exists.  FileStorage would assume its
   // fileName argument is an output file  if it doesn't exist i.e. we don't
   // get an error from it.
   if (stat(fileSpec.c_str(),&statBuf) != 0) {
      CREATE_ROS_EXCEPTION(ex1,ModulesException,NOFILE,fileSpec);
      throw(ex1);
   }

   DataReader* reader=pickDataReader(fileSpec);
   while (reader->good()) {
      unsigned int eventSize;
      char* buffer;
      DRError errorCode = reader->getData(eventSize,&buffer);
      if (errorCode==DROK) {
         unsigned int* header=reinterpret_cast<uint32_t*>(buffer);
         if (header[0]==0xaa1234aa) {
            const eformat::FullEventFragment<const uint32_t*> fragment(header);
            unpackFragments(fragment);
         }
         else {
            CREATE_ROS_EXCEPTION(ex, ModulesException, BAD_HEADER_MARKER,
                                 " (" << std::hex << header[0] << std::dec << ")");
            ers::error(ex);
         }
      }
      delete [] buffer;
   }
}

void PreloadedDataChannel::configure(
                                  const std::vector<std::string>& fileName,
                                  const std::string& /*fileTag*/)
{
   for (std::vector<std::string>::const_iterator fileIter=fileName.begin();
        fileIter!=fileName.end(); fileIter++) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "PreloadedDataChannel::configure() Loading data from " << *fileIter);

      struct stat statBuf;
      std::string splitDir=*fileIter+".per-rob";
      if (stat(splitDir.c_str(),&statBuf) != 0) {
         // Per ROL data files not available
         loadFile(*fileIter);
      }
      else {
         // OK, we load one file per ROL
         for (std::set<unsigned int>::const_iterator chanIter=s_rolIdList.begin(); chanIter!=s_rolIdList.end(); chanIter++) {
            std::ostringstream fnStream;
            fnStream << splitDir << "/0x" << std::setfill('0') << std::setw(8) << std::uppercase << std::hex << *chanIter << ".data";
            try {
               loadFile(fnStream.str());
            }
            catch (ModulesException& me) {
               if (me.getErrorId() == ModulesException::NOFILE) {
                  ers::warning(me);
               }
               else {
                  throw me;
               }
            }
         }
      }
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "Stored " << s_storeSize << " bytes for playback");
}

void PreloadedDataChannel::unconfigure()
{
   // Delete all events from event map
   for (std::map<unsigned int, EventMap>::iterator eventIter=s_store.begin();
        eventIter!=s_store.end(); eventIter++) {
      // Delete all fragments for this event
      for (EventMap::iterator fragmentIter=(*eventIter).second.begin();
           fragmentIter!=(*eventIter).second.end(); fragmentIter++) {
         delete [] fragmentIter->second;
      }
   }

   // delete the (now empty) events
   s_store.erase(s_store.begin(),s_store.end());
}


//***************************************************************************
void PreloadedDataChannel::unpackFragments
                             (const eformat::FullEventFragment<const uint32_t*>& event)
{

  uint32_t level1Id = event.lvl1_id();
  if (level1Id > s_maxLevel1) {
     s_maxLevel1=level1Id;
  }

  std::map<uint32_t, EventMap>::iterator dit = s_store.find(level1Id);
  if (dit == s_store.end()) {
     ///Adds a new entry with the new L1 identifier
     s_store[level1Id] = EventMap();
  }


  DEBUG_TEXT(DFDB_ROSFM, 15,
             "PreloadedDataChannel::unpackFragments() parsing event "
             << level1Id);


   for (unsigned int rob=0; rob < event.nchildren(); rob++) {
      const uint32_t* robp;
      event.child(robp, rob);
      eformat::ROBFragment<const uint32_t*> robFragment(robp);

      //gets (presumably unique) source identifer from ROB
      unsigned int  rolId = robFragment.source_id();

      // Am I interested on this ROB identifier?
      if (s_rolIdList.find(rolId) !=  s_rolIdList.end()) {
         EventMap::iterator rolIter = s_store[level1Id].find(rolId);
         //if I already have an entry there...
         if (rolIter != s_store[level1Id].end()) {
            CREATE_ROS_EXCEPTION(ex, ModulesException, DUPLICATE_ROBID, rolId << " for l1 id " << level1Id);
            ers::warning(ex);
            continue;
         }

         //  Now I need to store the data somewhere and keep a
         //  pointer to it in s_store
         DEBUG_TEXT(DFDB_ROSFM, 15, "Saving data from ROL " << std::hex << rolId << " (" << std::dec << rolId << ")" );
         DEBUG_TEXT(DFDB_ROSFM, 15, "copying " << robFragment.fragment_size_word()*4 << " bytes ");
         uint32_t* storage=new uint32_t[robFragment.fragment_size_word()];
         memcpy(storage, robp, robFragment.fragment_size_word()*4);
         s_store[level1Id][rolId]=storage;
         s_storeSize+=robFragment.fragment_size_word()*4;
      }
      else {
         DEBUG_TEXT(DFDB_ROSFM, 20, "Not using data from ROL " << std::hex << rolId << " (" << std::dec << rolId << ")" );
      }
   }
}

int PreloadedDataChannel::requestFragment(int level1Id)
{
   // return the level1Id as the ticket
   return(level1Id);
}


void* PreloadedDataChannel::findFragment(const unsigned int level1Id)
{
   unsigned int fakeLevel1Id=level1Id%(s_maxLevel1+1);

   std::map<uint32_t, EventMap>::iterator eventIter = s_store.find(fakeLevel1Id);
   if (eventIter == s_store.end()) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "Event " << level1Id << " not found");
      return 0;
   }

   EventMap* eventMap=&(*eventIter).second;
   EventMap::iterator rolIter=eventMap->find(m_rolId);
   if (rolIter == eventMap->end()) {
      DEBUG_TEXT(DFDB_ROSFM, 5, "PreloadedDataChannel::findFragment() Event " << level1Id
                 << " has no data for ROL " << m_rolId);
      return 0;
   }

   return (*rolIter).second;
}


EventFragment* PreloadedDataChannel::getFragment(int ticket)
{
   ROBFragment* fragment;
   // 'ticket' is the level1Id
   void* data=findFragment(ticket);
   if (data != 0) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "PreloadedDataChannel::getFragment() got fragment for ROL "
                 << m_rolId << " event " << ticket);

      m_mutex->lock();
      ROBFragment::ROBHeader* robHeader=(ROBFragment::ROBHeader*) data;
      RODFragment::RODHeader* rodHeader = (RODFragment::RODHeader*)((u_long)robHeader + robHeader->generic.headerSize * sizeof(int));
      if (rodHeader->level1Id != (unsigned int) ticket) {
         if (robHeader->generic.numberOfStatusElements == 2) {
            //Compensate 3 status elements
            if(robHeader->statusElement[2]) {
               robHeader->statusElement[2]=0;
               robHeader->generic.totalFragmentsize--;
            }
         }
         else {
            if(robHeader->crc_flag) {
               robHeader->crc_flag=0;
               robHeader->generic.totalFragmentsize--;
            }
         }
         rodHeader->level1Id=ticket;
      }

      fragment=new ROBFragment(m_memoryPool, data);
      m_mutex->unlock();
   }
   else {
      DEBUG_TEXT(DFDB_ROSFM, 15, "PreloadedDataChannel::getFragment() fragment not available for ROL "
                 << m_rolId << " event " << ticket);

      // 3rd arg should be sourceId which is more complex than just robId
      m_mutex->lock();
      fragment=new ROBFragment(m_memoryPool, ticket, m_rolId, 0);
      m_mutex->unlock();
   }
   return(fragment);
}

void PreloadedDataChannel::releaseFragment(const std::vector<unsigned int>* /*level1Id*/)
{
   // Dummy for now
}
void PreloadedDataChannel::releaseFragment(int /*level1Id*/)
{
   // Dummy for now
}

void PreloadedDataChannel::clearInfo()
{
   // Dummy for now
}
