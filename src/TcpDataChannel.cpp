//$Id:  $


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>  
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>  // struct sockaddr_in ...
#include <netinet/tcp.h> // TCP_NODELAY
#include <sys/socket.h>

#include <sys/ioctl.h>

#include "TcpDataChannel.h"
#include "ROSModules/ModulesException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;

/******************************************************************************************/
TcpDataChannel::TcpDataChannel(u_int id,
                               u_int index,
                               DFCountedPointer<Config> configuration,
                               EthSequentialDataChannelInfo* info) :
   SequentialDataChannel(id, index, id, true, configuration, info), m_socket(-1) {
/******************************************************************************************/
  DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::constructor: id = " << id);

  m_statistics = info;
  DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::constructor: Done");
}


/***************************************************/
TcpDataChannel::~TcpDataChannel() {
/***************************************************/
  DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::destructor: Called");
}


/**********************************************************************************************/
int TcpDataChannel::getNextFragment(u_int* buffer, int maxSize, u_long /*pciAddress*/) {
/**********************************************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::getNextFragment: Entered");

   // First read the size of the message
   u_int nrecv = 0;
   char buf[4];                  // place to store the message size
   while(nrecv < sizeof(int)) {
      int nread = read(m_socket, buf+nrecv, sizeof(int)-nrecv); 
      if (nread <= 0 && errno != EINTR) {
         u_int localErrno = errno;
         DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::getNextFragment: Error from first read()");
         CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BADSIZE, "errno = " << localErrno << ", nrecv = " << nrecv);
         throw(ex1);
      }
      else {
         nrecv += nread;
      }
   }

   // Get the size of the ROD/Full fragment
   int messageSize = (int)ntohl(*((u_long*)buf));

   // Now receive the data
   if (maxSize < messageSize) {
      DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::getNextFragment: fragment will be truncated size of fragment = " << messageSize << ",  max. allowed size = " << maxSize);
      CREATE_ROS_EXCEPTION(ex1, ModulesException, ALL_TRUNCATE, "size of fragment=" << messageSize << ", max. allowed size=" << maxSize);
      ers::warning(ex1);
   }

   int bytesReceived = 0;
   while(bytesReceived < messageSize) {
      int nread = read(m_socket, (void *)((u_long)buffer+bytesReceived), messageSize-bytesReceived);
      if (nread <= 0) {
         if (errno != EINTR) {
            DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::getNextFragment: Error from second read()");
            CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BADLINK, "");
            throw(ex1);
         }	
      }
      else {
         bytesReceived += nread;
      }
   }

   m_statistics->fragments_input++;  

   DEBUG_TEXT(DFDB_ROSFM, 20, "TcpDataChannel::getNextFragment: Fragment of " << bytesReceived << " bytes received, first word = 0x" << std::hex << *buffer << std::dec << " at " << buffer);
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::getNextFragment: Done");
   return(bytesReceived);
}


/**************************************/
int TcpDataChannel::poll(void) {
/**************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::poll: Entered");
   if (m_triggerReady == false) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::poll: Done(1)");
      return(0);
   }

   if(PollEth()) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::poll: Done(2)");
      DEBUG_TEXT(DFDB_ROSFM, 10, "TcpDataChannel::poll: Data waiting");
      return(1);
   }
   else {
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::poll: Done(3)");
      return(0);
   }
}


/*****************************************/
void TcpDataChannel::stopEB(void) {
/*****************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::stopEB: Entered");
   m_triggerReady = false;
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::stopEB: Done");
}


/******************************************/
void TcpDataChannel::connect(int acceptedSocket) {
/******************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::connect: Entered");
   m_socket = acceptedSocket; 
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::connect: Done");
}


/*********************************************/
void TcpDataChannel::disconnect(void) {
/*********************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::disconnect: Entered");
   if (m_socket!=-1) {
      int ret=close(m_socket);
      if (ret == -1) {
         CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_CLOSE,"TcpDataChannel::disconnect");
         throw(ex1);
      }
      m_socket=-1;
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::disconnect: Done");
}


/************************************************/
void TcpDataChannel::prepareForRun(void) {
/************************************************/
  DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::prepareForRun: Entered");

  cleanupLink();
  m_statistics->fragments_input = 0;

  m_triggerReady = true;
  DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::prepareForRun: Done");
}  
 

/****************************************************************/
ISInfo*  TcpDataChannel::getISInfo() {
/****************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::getIsInfo: called");
   return m_statistics;
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::getIsInfo: done");
}
 

/********************************************/
void TcpDataChannel::clearInfo(void) {
/********************************************/
   DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::clearInfo: Entered");
   SequentialDataChannel::clearInfo();

   m_statistics->fragments_input = 0;
   DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::clearInfo: Done");
}


/**********************************************/
void TcpDataChannel::cleanupLink(void) {
/**********************************************/
   char junk;

   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::cleanupLink: Entered");

   while (PollEth()) {
      read(m_socket, &junk, 1);
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::cleanupLink: Done");
}




/*****************************************/
int TcpDataChannel::PollEth(void) {
/*****************************************/
   struct timeval c_select_timeout = {0, 0};  //disable time out; just poll

   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::PollEth: Entered");
   fd_set recvFds;
   FD_ZERO(&recvFds);
   FD_SET(m_socket, &recvFds);

   int sel = select(m_socket+1, &recvFds, NULL, NULL, &c_select_timeout);
   DEBUG_TEXT(DFDB_ROSFM, 20, "TcpDataChannel::PollEth: back from select with sel = " << sel);

   if (sel == 0) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::PollEth: no data waiting");
      return(0);
   }
   else if (sel > 0) {
      if (FD_ISSET(m_socket, &recvFds)) {
         DEBUG_TEXT(DFDB_ROSFM, 15, "TcpDataChannel::PollEth: data waiting");
         int length;
         int status=ioctl(m_socket, FIONREAD, &length);
         if (status) {
            std::cout << "Error reading input size\n";
         }
         if (length) {
            //         std::cout << length << " bytes availabe for reading\n";
            return(1);
         }
         else {
            std::cout << "Nothing to read, eof??\n";
            m_triggerReady=false;
            CREATE_ROS_EXCEPTION(ex0,ModulesException,ETH_SELECT,"fd flagged but no data avialable probable EOF");
            ers::error(ex0);
            return(0);
         }
      }
      else {
         DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::PollEth: Error 1 in select");
         CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT,"");
         throw(ex1);
      }
   }
   else { // (select < 0)
      DEBUG_TEXT(DFDB_ROSFM, 5, "TcpDataChannel::PollEth: Error 2 in select");
      CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT,"");
      throw(ex1);
   }
}
