//$Id$
/*
  ATLAS ROS Software

  Class: EmulatedDataChannel
  Authors: ATLAS ROS group 	
*/

#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSEventFragment/ROBFragment.h"
//#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "DFThreads/DFThread.h" 
#include "ROSModules/ModulesException.h"
#include "ROSModules/EmulatedDataChannel.h"


using namespace ROS;


/*******************************************************************************/
EmulatedDataChannel::EmulatedDataChannel(unsigned int channelId,
					 unsigned int configId,
					 unsigned int channelPhysicalAddress,
                                         int npages,
					 int pageSize,
					 int pooltype,
					 EmulatedReadoutModule *module,
					 DFCountedPointer<Config> configuration,
					 int numberOfTickets) 
   : DataChannel(channelId, configId, channelPhysicalAddress),
     m_memoryPool(0)
/*******************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::constructor: called");

  char requestMutexName[]="REQUESTMUTEX";
  m_mutex                  = DFFastMutex::Create(requestMutexName);
  
  std::stringstream mutexName;
  mutexName << "PciRobinIOMessageMutex" << module->m_robInId << std::ends;  //MJ: Is module->m_robInId different for each instance of a module? Should it be?
  m_hwmutex                = DFFastMutex::Create(const_cast<char *>(mutexName.str().c_str()));
  m_ticketMutex            = DFFastMutex::Create();
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: mutex for rol # " << channelId << " = " << m_ticketMutex);
  
  m_module                 = module;
  m_channelPhysicalAddress = channelPhysicalAddress;
  m_bunchCrossingID        = 1;       //See SLIDAS data format
  m_triggerType            = 2;       //See SLIDAS data format      

  if (pooltype == MemoryPool::ROSMEMORYPOOL_MALLOC)
    m_memoryPool           = new MemoryPool_malloc(npages, pageSize);
  if (pooltype == MemoryPool::ROSMEMORYPOOL_CMEM_BPA || pooltype == MemoryPool::ROSMEMORYPOOL_CMEM_GFP)
  {
    DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: Calling MemoryPool_CMEM with pooltype = " << pooltype);
    m_memoryPool           = new MemoryPool_CMEM(npages, pageSize, pooltype);
  }
  
  m_getDelay               = configuration->getInt("GetFragmentDelay");
  m_releaseDelay           = configuration->getInt("ReleaseDelay");  
  m_releaseGroupDelay      = configuration->getInt("ReleaseGroupDelay");  
  m_messageDelay           = configuration->getInt("MessageDelay");
  m_detectorEventType      = configuration->getInt("detectorEventType");
  m_numberOfStatusElements = configuration->getInt("NumberOfStatusElements");
  m_numberOfDataElements   = ((pageSize - sizeof(ROBFragment::ROBHeader) -
                              sizeof(RODFragment::RODHeader) -
                              sizeof(RODFragment::RODTrailer)) >> 2 )	// bytes to words
                              - m_numberOfStatusElements;  
  m_statusBlockPosition    = configuration->getInt("StatusBlockPosition");
  m_getMiss                = configuration->getFloat("GetMissingFragment");

  //Now deal with the ticket vector
  m_nextTicket             = 0;
  m_numberOfTickets        = numberOfTickets;
  m_requestVector          = new tstamp[m_numberOfTickets];
  m_ticketVector           = new int[m_numberOfTickets];
  m_l1idVector             = new int[m_numberOfTickets];
  // Initialise vector of tickets
  for (int i=0; i<m_numberOfTickets; i++) {
     m_ticketVector[i]=i;
  }
 
  // reset IS Info (not really required here ..)
  m_statistics.QHist.assign(m_numberOfTickets, 0);

  m_statistics.noYieldsGetFragmentWaitReply = 0;
  m_statistics.noYieldsReleaseWaitReply     = 0;
  m_statistics.noYieldsGetTicket            = 0;

  // initialise the Queue histogram
  m_QueueHist.assign(m_numberOfTickets, 0);

  m_noYieldsGetFragmentWaitReply = 0;
  m_noYieldsReleaseWaitReply     = 0;
  m_noYieldsGetTicket            = 0;
 
  std::cout << " Emulated DataChannel Configuration parameters: " << std::endl;

  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: Configuration parameters: ");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: # data elements (words)         = " << m_numberOfDataElements);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: Channel ID                      = " << channelId);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: Internal ROL number             = " << channelPhysicalAddress);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: # pages in memory pool          = " << npages);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: page size                       = " << pageSize);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: pool type                       = " << pooltype);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: getFragment delay               = " << m_getDelay << " microseconds");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: requestFragment (message) delay = " << m_messageDelay << " microseconds");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: releaseFragment delay           = " << m_releaseDelay << " microseconds");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: releaseGroupFragment delay      = " << m_releaseGroupDelay << " microseconds");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::constructor: getFragment missing %           = " << m_getMiss);
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::constructor: done");
}


/*****************************************/
EmulatedDataChannel::~EmulatedDataChannel()
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::destructor: called");
  m_mutex->destroy();
  m_hwmutex->destroy();
  m_ticketMutex->destroy();
  delete m_memoryPool;

  delete [] m_requestVector;
  delete [] m_ticketVector;
  delete [] m_l1idVector;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::destructor: done");
}


/******************************************/
ISInfo *EmulatedDataChannel::getISInfo(void)
/******************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getISInfo: called");

  for (int i = 0; i < m_numberOfTickets; i++) {
    m_statistics.QHist[i] = m_QueueHist[i];
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getISInfo: done");
  return &m_statistics;
}


/***************************************/
void EmulatedDataChannel::clearInfo(void)
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::clearInfo: called");

  for (int i=0; i<m_numberOfTickets;i++) {
    m_statistics.QHist[i] = 0;
  }

  m_statistics.noYieldsGetFragmentWaitReply = 0;
  m_statistics.noYieldsReleaseWaitReply     = 0;
  m_statistics.noYieldsGetTicket            = 0;
  m_noYieldsGetFragmentWaitReply            = 0;
  m_noYieldsReleaseWaitReply                = 0;
  m_noYieldsGetTicket                       = 0;

  for (int l_index = 0; l_index < m_numberOfTickets; l_index++) {
     m_QueueHist[l_index] = 0;
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::clearInfo: done");
}


/****************************************************/
int EmulatedDataChannel::requestFragment(int level1Id)
/****************************************************/
{
  int ticket;
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::requestFragment: called");
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::requestFragment on channel " << this->id() << ", fragment " << level1Id << " requested");
 
  if (m_messageDelay > 0)
  { 
    m_hwmutex->lock();
    ts_delay(m_messageDelay);
    m_hwmutex->unlock();
  }

  ticket = getNewTicket();
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::requestFragment, channel " << this->id() << ", ticket = " << ticket);
  m_requestVector[ticket] = m_module->scheduleRequest(m_getDelay);
  m_l1idVector[ticket] = level1Id;
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::requestFragment, ts_stamp = " << m_requestVector[ticket].high << m_requestVector[ticket].low);
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::requestFragment: done");
  return(ticket);
}


/*****************************************************/
void EmulatedDataChannel::releaseFragment(int level1Id)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseFragment: called");
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::releaseFragment: fragment " << level1Id << " released");
    
  if (m_releaseDelay > 0)
  {
    m_hwmutex->lock();
    ts_delay(m_releaseDelay);
    m_hwmutex->unlock();
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseFragment: done");
}


/************************************************************************************/
void EmulatedDataChannel::releaseFragment(const std::vector <unsigned int>* level1Ids)
/************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseFragment: called");
  DEBUG_TEXT(DFDB_ROSFM, 10, "EmulatedDataChannel::releaseFragment executing ReleaseRequest" <<
                             " on channel " << id() << " for " << level1Ids->size() <<
                             " events "  << level1Ids->front() << ".." << level1Ids->back());
  
  if (m_releaseDelay > 0) {
    m_hwmutex->lock();
    ts_delay(m_releaseDelay);
    m_hwmutex->unlock();
  }

  TS_RECORD(TS_H1, 2010);

  // wait for ACK
  unsigned int nyield = 0;
  if (m_releaseGroupDelay > 0) {
    // we take a ticket here only to ensure max number of queued requests	
    int ticket = getNewTicket();	
    ts_wait_until(m_module->scheduleRequest(m_releaseGroupDelay), &nyield);
    m_statistics.noYieldsReleaseWaitReply += nyield;
    m_noYieldsReleaseWaitReply += nyield;
    releaseTicket(ticket);
  }    

  TS_RECORD(TS_H1, 2090);
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseFragment: done");
}


/**************************************************************/
ROS::EventFragment *EmulatedDataChannel::getFragment(int ticket)
/**************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getFragment: called");
  TS_RECORD(TS_H1, 1010);
 
  //decide if we have this fragment
  float alea = ((float)rand() / (float)RAND_MAX);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::getFragment  alea = " << alea << "   m_getMiss  = " << m_getMiss);

  EventFragment* fragment;
  m_mutex->lock();  //protect unsafe allocation
		    // BCID = L1Id ( = ticket)
  if (alea < m_getMiss) {
     fragment = new ROBFragment(m_memoryPool, m_l1idVector[ticket], id(), 0);
     fragment->setStatus(0x40000004);
  }
  else {
     fragment = new ROBFragment(m_memoryPool,
                                id(),
                                m_l1idVector[ticket], 
                                m_l1idVector[ticket],
                                m_triggerType,
                                m_detectorEventType,
                                m_numberOfStatusElements,
                                m_numberOfDataElements,
                                m_statusBlockPosition);
  }
  TS_RECORD(TS_H1,1030);
  m_mutex->unlock();
  TS_RECORD(TS_H1, 1040);
   
  unsigned int nyield = 0;
  if (m_getDelay > 0) {
    ts_wait_until(m_requestVector[ticket], &nyield);
    m_statistics.noYieldsGetFragmentWaitReply += nyield;
    m_noYieldsGetFragmentWaitReply += nyield;
  }
  releaseTicket(ticket);
  
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::getFragment: got fragment " << ticket);
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedDataChannel::getFragment: ts_wait called sched_yield " << nyield << " times");
  TS_RECORD(TS_H1, 1090);
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getFragment: done");
  return(fragment);
}


//Called by requestFragment and releaseFragment
/*****************************************/
int EmulatedDataChannel::getNewTicket(void) 
/*****************************************/
{
  int ticket;

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getNewTicket: called");
  m_ticketMutex->lock();

  while (m_nextTicket == m_numberOfTickets)  {
    m_ticketMutex->unlock();
    DFThread::yield();
    m_statistics.noYieldsGetTicket++;
    m_noYieldsGetTicket++;
    m_ticketMutex->lock();
  }
  ticket = m_ticketVector[m_nextTicket];
  m_nextTicket++;

  // histogram the Q length
  m_QueueHist[m_nextTicket - 1]++;

  m_ticketMutex->unlock();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::getNewTicket: done");
  return ticket;
}


//Called by getFragment and releaseFragment
/*************************************************/
void EmulatedDataChannel::releaseTicket(int ticket)
/*************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseTicket: called");
  m_ticketMutex->lock();
  m_nextTicket--;
  m_ticketVector[m_nextTicket] = ticket;
  m_ticketMutex->unlock();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedDataChannel::releaseTicket: done");
}
