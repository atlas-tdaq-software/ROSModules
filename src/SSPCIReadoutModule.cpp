//$Id$
/************************************************************************/
/*									*/
/* File             : SSPCIReadoutModule.cpp				*/
/*									*/
/* Authors          : Markus Joos, CERN					*/
/*									*/
/* The purpose of this class is (by means of a thread) to input         */
/* emulated event fragments into a number of event buffers, one per 	*/
/* ReadOut link. It will be started and controlled from the IOManager   */ 
/* It serves as a skeleton for other applications with real input links */
/*									*/
/********* C 2004 - The software with that certain something ************/

#include "ROSModules/SSPCIReadoutModule.h"
#include "ROSModules/ModulesException.h"
#include "ROSCore/PCMemoryDataChannel.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSEventFragment/ROBFragment.h"

using namespace ROS;


/**************************************/
SSPCIReadoutModule::SSPCIReadoutModule() 
/**************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::constructor: Entered");
  char mutexName[]="REQUESTMUTEX";
  m_fastAllocateMutex = DFFastMutex::Create(mutexName);
}


/***************************************/
SSPCIReadoutModule::~SSPCIReadoutModule() noexcept
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::destructor: Entered");
  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;
  }

  m_fastAllocateMutex->destroy();
}


/********************************************************************/
void SSPCIReadoutModule::setup(DFCountedPointer<Config> configuration) 
/********************************************************************/
{ 
   m_configuration=configuration;
}

/**********************************/
void SSPCIReadoutModule::configure(const daq::rc::TransitionCmd&)
/**********************************/
{
  m_numberOfDataChannels = m_configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: m_numberOfDataChannels = " << m_numberOfDataChannels);

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_eventInputManager.push_back(0);
    m_memoryPool.push_back(0);
    m_ssp_device.push_back(0);
    m_slink_input_state.push_back(WAIT_SPACE);
    m_robPageSize.push_back(0);
    m_statistics.rod_L1id_prev.push_back(0);
    m_mem_page.push_back(0);   
    m_statistics.fragments_input.push_back(0);
    m_statistics.stat_head.push_back(0);
    m_statistics.stat_size.push_back(0);
    m_statistics.stat_l1id.push_back(0);
  }

  err_type r_code = SLINK_Open();		// possibly multiply open's
  if (r_code != SLINK_OK && r_code != SLINK_ISOPEN) 
  { 
    DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::configure, Error from SLINK_Open");
    rcc_error_string(m_rcc_err_str, r_code);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_OPEN, m_rcc_err_str);
    throw(ex1);
  }

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    u_int id = m_configuration->getInt("channelId", chan);
    u_int rolPhysicalAddress = m_configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: chan = " << chan << "  id = " << id << " rolPhysicalAddress = " << rolPhysicalAddress);
    int poolPageSize = m_configuration->getInt("memoryPoolPageSize", chan);
    m_robPageSize[chan] = poolPageSize;
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: memoryPoolPageSize = " << m_robPageSize[chan]);
    PCMemoryDataChannel *channel = new PCMemoryDataChannel(id, chan, rolPhysicalAddress, m_configuration);
    m_dataChannels.push_back(channel);

    //Get object pointers from the data channel
    m_eventInputManager[chan] = channel->getEIM(); 
    m_memoryPool[chan] = m_eventInputManager[chan]->getMemoryPool();	   

    // open the SLINK  for one instance  defined by "rolPhysicalAddress"
    SLINK_parameters ssp_params;
    SLINK_InitParameters(&ssp_params);

    //According the the 3rd ROD workshop the RODs should deliver big-endian data. It has turned out that several RODs
    //did not respect this convention and provide the data in little endian byte ordering.
    //As this could not be changed quickly enough we finally decided to make the byte ordering configurable.
    //If 0x1000 is added to the parameter "rolPhysicalAddress" in the database it will tell the S/W below
    //not to enable the byte swapping

    int donotswap = m_configuration->getInt("ByteSwapping");
    ssp_params.occurence = (rolPhysicalAddress);
    if (donotswap)
    {
      ssp_params.byte_swapping = SLINK_DISABLE;
      ssp_params.start_word = 0xB0F00000;
      ssp_params.stop_word  = 0xE0F00000;
    }
    else
    {
      ssp_params.byte_swapping = SLINK_ENABLE;
      ssp_params.start_word = 0x0000F0B0;
      ssp_params.stop_word  = 0x0000F0E0;
    }

    r_code = SLINK_SSPOpen(&ssp_params, &m_ssp_device[chan]);
    if (r_code != SLINK_OK) 
    { 
      DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::configure, Error from SLINK_SSPOpen");
      rcc_error_string(m_rcc_err_str, r_code);
      int slen = strlen(m_rcc_err_str);
      sprintf(m_rcc_err_str + slen - 1, "This error came from ROL with channel #  = %d\n", chan); 
      CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_SSPOPEN, m_rcc_err_str);
      throw(ex1);
    }
    std::cout << " Occurrence " << rolPhysicalAddress << " corresponds to id " << id << std::endl;
    m_statistics.rod_L1id_prev[chan] = INIT_L1ID;
  }

  m_triggerQueueFlag = m_configuration->getBool("triggerQueue");
  DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: triggerQueue = " << m_triggerQueueFlag); 
  if (m_triggerQueueFlag) 
  {
    m_triggerQueueSize = m_configuration->getInt("triggerQueueSize");
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: triggerQueueSize = " << m_triggerQueueSize);

    m_triggerQueueUnblockOffset = m_configuration->getInt("triggerQueueUnblockOffset");
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure: triggerQueueUnblockOffset = " << m_triggerQueueUnblockOffset);

    m_inputToTriggerQueue = TriggerInputQueue::instance();
    m_inputToTriggerQueue->setSize(m_triggerQueueSize);

    std::cout << " SSPCIReadoutModule: Q pointer = " << m_inputToTriggerQueue << " Q size = " << m_triggerQueueSize << std::endl;
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::configure, Q = " << m_inputToTriggerQueue << " size = " <<  m_triggerQueueSize);
  }
}


/****************************************/
void SSPCIReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
/****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::unconfigure: Entered");
  
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    err_type r_code = SLINK_SSPClose(m_ssp_device[chan]);
    if (r_code != SLINK_OK) 
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::unconfigure, Error from SLINK_SSPClose");
      rcc_error_string(m_rcc_err_str, r_code);
      int slen = strlen(m_rcc_err_str);
      sprintf(m_rcc_err_str + slen - 1, "This error came from ROL with channel #  = %d\n", chan);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_SSPCLOSE, m_rcc_err_str);
      throw(ex1);
    }
  }
  
  u_int r_code = SLINK_Close();
  if (r_code != SLINK_OK) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::unconfigure, Error from SLINK_Close");
    rcc_error_string(m_rcc_err_str, r_code);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_CLOSE, m_rcc_err_str);
    throw(ex1);
  }

  if (m_triggerQueueFlag) 
  {
    m_inputToTriggerQueue->destroy();
  }

  m_eventInputManager.clear();
  m_memoryPool.clear();
  m_ssp_device.clear();
  m_slink_input_state.clear();
  m_robPageSize.clear();
  m_statistics.rod_L1id_prev.clear();
  m_mem_page.clear();
  m_statistics.fragments_input.clear();
  m_statistics.stat_head.clear();
  m_statistics.stat_size.clear();
  m_statistics.stat_l1id.clear();
}


/******************************************/    
void SSPCIReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/******************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::prepareForRun: starting SSPCI input thread");
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_statistics.rod_L1id_prev[chan] = INIT_L1ID; 
    m_statistics.fragments_input[chan] = 0;
    m_statistics.stat_head[chan] = 0;
    m_statistics.stat_size[chan] = 0;
    m_statistics.stat_l1id[chan] = 0;
  }
  m_statistics.yield = 0;
  
  startExecution(); 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::prepareForRun: SSPCI input started");
}
    

/***********************************/
void SSPCIReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::stopGathering: Entered");

  //Stop the thread 
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::stopGathering: stopping SSPCI input thread");
  stopExecution();
  waitForCondition(DFThread::TERMINATED, 1);	 // FIXME: handle timeout
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::stopGathering: SSPCI input thread stopped");

  if (m_triggerQueueFlag) 
    m_inputToTriggerQueue->reset();

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_statistics.rod_L1id_prev[chan] = INIT_L1ID;

    u_int r_code = SSP_Reset(m_ssp_device[chan]);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::stop, Error from SSP_Reset");
      rcc_error_string(m_rcc_err_str, r_code);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_RESET, m_rcc_err_str);
      throw(ex1);
    }
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::stop: SSP_Reset called for rol = " << chan);
    m_slink_input_state[chan] = WAIT_SPACE;
  }

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_eventInputManager[chan]->reset();
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::stopGathering: Done");
}


/**********************************/
void SSPCIReadoutModule::clearInfo()
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::clearInfo  Function entered");
  for(int chan = 0; chan < m_numberOfDataChannels; chan++)
  {
    m_statistics.fragments_input[chan] = 0;
    m_statistics.stat_head[chan] = 0;
    m_statistics.stat_size[chan] = 0;
    m_statistics.stat_l1id[chan] = 0;
  }

  m_statistics.yield = 0;
}


// if the L1id just arrived is the smallest of the current L1ids on the ROLS
// then all fragments for L1id have arrived ..
/****************************************************/
bool SSPCIReadoutModule::FragmentsAreThere(u_int l1id)
/****************************************************/
{
  //MJ: would this really work with a 32-bit L1ID where 0xffffffff is legal?

  std::vector<u_int>::iterator max_it = max_element(m_statistics.rod_L1id_prev.begin(), m_statistics.rod_L1id_prev.end());
  if (*max_it == INIT_L1ID)
  {
    return false;
  }

  std::vector<u_int>::iterator min_it = min_element(m_statistics.rod_L1id_prev.begin(), m_statistics.rod_L1id_prev.end());
  if (l1id == *min_it)
    return true;
  else
    return false;
}


/*********************************************/
void SSPCIReadoutModule::InitReadSSPCI(int rol)
/*********************************************/
{  
  DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::InitReadSSPCI: checking page availability");

  if (m_memoryPool[rol]->isPageAvailableNonReserved()) // leave mutex unprotected: READ ONLY (thread safe ..)
  {
    m_fastAllocateMutex->lock();  // lock memory allocation
    m_mem_page[rol] = m_memoryPool[rol]->getPage(); 
 
    //Reserve the full page size (at this point we do not know how many bytes will be trasferred)
    u_int* buffer = (u_int *)(m_mem_page[rol]->reserve(m_robPageSize[rol]));

    m_fastAllocateMutex->unlock();

    if (buffer == 0) 
    {
      DEBUG_TEXT(DFDB_ROSFM, 5, "MemoryPool returned an already locked page or a page with wrong size: ");
      DEBUG_TEXT(DFDB_ROSFM, 5, "the capacity of the page appears to be " << m_mem_page[rol]->capacity()); 
      DEBUG_TEXT(DFDB_ROSFM, 5, "instead of " << m_robPageSize[rol]); 
      CREATE_ROS_EXCEPTION(ex1,ModulesException,ALL_INVALIDPAGE,"");
      throw(ex1);
    }

    u_int pci_addr = m_mem_page[rol]->physicalAddress() + sizeof(ROBFragment::ROBHeader);
    u_int maxsize =  m_robPageSize[rol] - sizeof(ROBFragment::ROBHeader);

    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::InitReadSSPCI: m_mem_page = " << m_mem_page[rol] << "  pageNo = " << m_mem_page[rol]->pageNumber());
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::InitReadSSPCI: pci_addr = " << pci_addr);
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::InitReadSSPCI: maxsize = " << maxsize);
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::InitReadSSPCI: Initiating transfer from ROL " << rol); 

    u_int r_code = SSP_InitRead(m_ssp_device[rol], (char *)pci_addr, maxsize, SLINK_ENABLE, 0);
    if (r_code) 
    {    
      DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::InitReadSSPCI, Error from SSP_InitRead");
      rcc_error_string(m_rcc_err_str, r_code);
      int slen = strlen(m_rcc_err_str);
      sprintf(m_rcc_err_str + slen - 1, "This error came from ROL with logical id = %d\n", rol);
      CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_INITREAD,m_rcc_err_str);
      throw(ex1);
    }
    m_slink_input_state[rol] = WAIT_TRANSFER;
  }
}


/******************************************/
u_int SSPCIReadoutModule::ReadSSPCI(int rol)
/******************************************/
{
  u_int r_code = 0;
  int delta_size = 0, status;

  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::ReadSSPCI: Entered for rol = " << rol);

  r_code = SSP_ControlAndStatus(m_ssp_device[rol], &status);
  if (r_code) 
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::ReadSSPCI, Error from SSP_ControlAndStatus");
    rcc_error_string(m_rcc_err_str, r_code);
    int slen = strlen(m_rcc_err_str);
    sprintf(m_rcc_err_str + slen - 1, "This error came from ROL with logical id = %d\n", rol);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_CAS,m_rcc_err_str);
    throw(ex1);
  }

  if (status == SLINK_FINISHED) // one ROD fragment done
  {
    delta_size = SSP_PacketSize(m_ssp_device[rol]);
    m_slink_input_state[rol] = WAIT_SPACE;
    DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::ReadSSPCI: status SLINK_FINISHED received from SSP_ControlAndStatus");
  }

  else if (status == SLINK_RECEIVING)
  {
    DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::ReadSSPCI: status SLINK_RECEIVING received from SSP_ControlAndStatus");
  }

  else if (status == SLINK_WAITING_FOR_SYNC)
  {
    DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::ReadSSPCI: status SLINK_WAITING_FOR_SYNC received from SSP_ControlAndStatus");
  }

  else if (status == SLINK_NEED_OF_NEW_PAGE) // current page full
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::ReadSSPCI: ROD fragment from ROL " << rol << " will be truncated");
    char mytext[100];
    sprintf(mytext, " Fragment from ROL = %d \n", rol);
    CREATE_ROS_EXCEPTION(ex1,ModulesException,ALL_TRUNCATE,mytext);
    throw(ex1);
  }

  else
  {
    DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::ReadSSPCI: unexpected status " << status << " received from SSP_ControlAndStatus");
    CREATE_ROS_EXCEPTION(ex1,ModulesException,SSP_CAS,"");
    throw(ex1);
  }

  return(delta_size);
}


/********************************/
void SSPCIReadoutModule::run(void)
/********************************/
{
  bool busy;
  
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::run: Entered");

  while (1)
  {
    busy = true;
    while(busy)
    {
      busy = false;
      for(int chan = 0; chan < m_numberOfDataChannels; chan++)
      {
        DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run1: m_slink_input_state[" << chan << "] = " << m_slink_input_state[chan]);

        if (m_slink_input_state[chan] == WAIT_SPACE)
          InitReadSSPCI(chan);

        DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run2: m_slink_input_state[" << chan << "] = " << m_slink_input_state[chan]);

        if (m_slink_input_state[chan] == WAIT_TRANSFER)
        {
          //Get the event fragment from the SSPCI card
          u_int msg_nrecv = ReadSSPCI(chan);

	  DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run: msg_nrecv = " << msg_nrecv);
          if (msg_nrecv)
          {
            busy = true; 

            //Check the marker of the ROD fragment
            u_int *markerptr = (u_int *) ((u_long)m_mem_page[chan]->address() + sizeof(ROBFragment::ROBHeader));
            if ((*markerptr != 0xeeeeeeee) && (*markerptr != 0xee1234ee))
//            if ((*markerptr != 0xeeeeeeee) && (*markerptr != 0x1234))  // debug hook
            {
              m_statistics.stat_head[chan]++;
              CREATE_ROS_EXCEPTION(e, ModulesException,ALL_MARKER, "\nROD Marker = " << HEX(*markerptr) << ", ROL = " << chan);
              ers::warning(e);
            }

            //Compute address of L1ID and ROD trailer
            u_int rodbase     = (u_long)m_mem_page[chan]->address() + sizeof(ROBFragment::ROBHeader);
            u_int *l1idptr    = (u_int *) (rodbase + (4 * EventFragment::rod_l1id_offset));
            u_int *trailerptr = (u_int *) (rodbase + msg_nrecv - sizeof(RODFragment::RODTrailer));
            DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run: l1idptr = " << l1idptr << "   trailerptr = " << trailerptr);

            u_int l1id = *l1idptr;

            //Check if the number of words received is consistent with the information in the ROD TRAILER
            u_int rodfragsize = sizeof(RODFragment::RODHeader) + sizeof(RODFragment::RODTrailer) + trailerptr[0] * 4 + trailerptr[1] * 4;

            DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run:  Expected fragment size = " << rodfragsize << "   real fragment size = " << msg_nrecv);
            if (rodfragsize != msg_nrecv)
//            if (rodfragsize != 1) 	// debug hook
            {
              DEBUG_TEXT(DFDB_ROSFM, 5, "SSPCIReadoutModule::run: Size mismatch:  Expected fragment size = " << rodfragsize
                                         << "   real fragment size = " << msg_nrecv);
              m_statistics.stat_size[chan]++;
              CREATE_ROS_EXCEPTION(e, ModulesException, ALL_ILLSIZE2 ,"\n L1ID = " << l1id
                                   << ", received  size = " << msg_nrecv << ", expected size = " << rodfragsize);
              ers::warning(e);
            }

            DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run: ROD marker = " << HEX(*markerptr));
            DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run: L1ID       = " << l1id);
            //Lock event creation
            m_fastAllocateMutex->lock();

	    //Release unused memory from the memory page
	    //This call could need to be mutex protected even 
	    //though this page should only be accessed by this thread 
	    //at this moment.  => To Be Checked
	    m_mem_page[chan]->release(m_robPageSize[chan] - (msg_nrecv + sizeof(ROBFragment::ROBHeader)));

            // Get the event descriptor
            evDesc_t *ed = m_eventInputManager[chan]->getEventDescriptor(m_mem_page[chan]);
	    if (m_statistics.rod_L1id_prev[chan] == INIT_L1ID) 
	      std::cout << "SSPCIReadoutModule::run: First L1ID on link " << chan << " is " << HEX(l1id) << std::endl;

            //Check the L1ID for consistency
            if ((m_statistics.rod_L1id_prev[chan] != INIT_L1ID) && (l1id != (m_statistics.rod_L1id_prev[chan] + 1))) 
//            if ((m_statistics.rod_L1id_prev[chan] != INIT_L1ID) && (l1id != 12345)) 	// debug hook
            {
              m_statistics.stat_l1id[chan]++;
              CREATE_ROS_EXCEPTION(e, ModulesException, ALL_SEQUENCE, "\n Last L1ID = " << m_statistics.rod_L1id_prev[chan]
                                   << ", New L1Id = " << l1id << ", ROL = " << chan);
              ers::warning(e);
            }
            m_statistics.rod_L1id_prev[chan] = l1id;

            //Initialize the event descriptor        
            ed->L1id = l1id;
            ed->RODFragmentSize = msg_nrecv / 4;  //MJ: words?
            DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run: L1id = " << ed->L1id << " RODFragmentSize = " << ed->RODFragmentSize);

            //Insert the event into the Event Input Manager
            m_eventInputManager[chan]->createEvent(ed);

            m_fastAllocateMutex->unlock();

            DEBUG_TEXT(DFDB_ROSFM, 10, "SSPCIReadoutModule::run created event with L1id = " << ed->L1id << "  on chan " << chan);

            if (m_triggerQueueFlag)	// only in self-triggering mode
            {
              bool complete = FragmentsAreThere(ed->L1id); 
              DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run complete = " << complete);

              if (complete)  
              {
                DEBUG_TEXT(DFDB_ROSFM, 20, "SSPCIReadoutModule::run:: push L1id " << ed->L1id  << " onto Q");
                m_inputToTriggerQueue->push(ed->L1id);
              }
            }
          }   // if msg_nrecv
        }	// page available
      }		// ROL done
    }		// busy
    // We have scanned all ROLs but none has data or all pages are used in the memory pool of the
    // respective ROL. There is nothing to do. Therefore: 
    DFThread::yieldOrCancel();  
    m_statistics.yield++;
  }  // while(1)
}


/********************************/
void SSPCIReadoutModule::cleanup()
/********************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "SSPCIReadoutModule::cleanup: Empty method");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createSSPCIReadoutModule();
}
ReadoutModule* createSSPCIReadoutModule()
{
  return (new SSPCIReadoutModule());
}












