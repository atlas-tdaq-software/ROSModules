//$Id$
/************************************************************************/
/*                                                                      */
/* File             : TCPTriggerDataChannel.cpp       		        */
/*                                                                      */
/* Authors          : E. Pasqualucci, J.Petersen 	                */
/*                                                                      */
/********* C 2005 - ROS/RCD *********************************************/

#include <sys/types.h>
#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"
#include "DFThreads/DFThread.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "ROSModules/TCPTriggerDataChannel.h"

using namespace ROS;

/**********************************************************************************************/
TCPTriggerDataChannel::TCPTriggerDataChannel(u_int id, u_int configId, u_int rolPhysicalAddress,
					      DFCountedPointer<Config> /*configuration*/)
  : DataChannel(id, configId, rolPhysicalAddress)
/**********************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::constructor: Entered");

  struct timeval wt = TCPTRIG_WRTIMEOUT;
  struct timeval rt = TCPTRIG_RDTIMEOUT;

  m_outinfo     = 0;
  m_busySources = 0;
  m_trigger     = 0;
  m_reset       = 0;
  m_enable      = false;
  m_fd0         = 0;
  m_fdmax       = 0;

  memcpy(&m_rdtout, &rt, sizeof(struct timeval));
  memcpy(&m_wrtout, &wt, sizeof(struct timeval));

  FD_ZERO(&m_fds);
  FD_ZERO(&m_zfds);

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::constructor: Exited");
}


/*********************************************/
TCPTriggerDataChannel::~TCPTriggerDataChannel()
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::destructor: Entered. Empty function");
}


/*******************************************************/
DFCountedPointer<Config> TCPTriggerDataChannel::getInfo()
/*******************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getInfo: Entered");

  if(m_outinfo == 0)
    m_outinfo = Config::New();

  m_outinfo->set("TriggerNumber", m_trigger);
  m_outinfo->set("ResetNumber",   m_reset);
  m_outinfo->set("BusySources",   m_busySources);

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getInfo: Exited");
  return(m_outinfo);
}


/*************************************/
void TCPTriggerDataChannel::clearInfo()
/*************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::clearInfo: Entered");
  m_trigger     = 0;
  m_reset       = 0;
  m_busySources = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::clearInfo: Exited");
}


/****************************************************/
int TCPTriggerDataChannel::requestFragment(int lvl1id)
/****************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::requestFragment: Entered");

  int i, j, nmax = m_fdmax + 1;

  while(!m_enable)
    DFThread::yieldOrCancel();

  // the busy sources are re-calculated dynamically since a link may be added or removed during a run
  // I.e. the "m_fds" may change dynamically - at least that's the theory .... 
  for(i = 0, j = 0; i < nmax; ++i)
    if(FD_ISSET(i, &m_fds))
      ++j;

  m_busySources = j;
  DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerDataChannel::requestFragment: # busy sources = " << j);
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::requestFragment: Exited");
  return lvl1id;
}


/***********************************************************/
EventFragment *TCPTriggerDataChannel::getFragment(int /*ticket*/)
/***********************************************************/
{
  char c;
  int nmin, nmax, nsel;
  fd_set jfds;
  struct timeval tout;

  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: Entered");

  if(memcmp(&m_fds, &m_zfds, sizeof(fd_set)))
  {
    fd_set fd_done;
    FD_ZERO(&fd_done);
      
    nmax = m_fdmax + 1;
    nmin = m_fdmin;
    DEBUG_TEXT(DFDB_ROSFM, 20, "TCPTriggerDataChannel::getFragment: nmin = " << nmin <<  "  nmax = " << nmax);

    while(memcmp(&fd_done, &m_fds, sizeof(fd_set)))
    {
      int i;

      memcpy(&jfds, &m_fds, sizeof(fd_set));
      memcpy(&tout, &m_wrtout, sizeof(struct timeval));

      for(i = nmin; i < nmax; ++i) 
      {
	if(FD_ISSET(i, &fd_done))
	{
          DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: clear fd # " << i);
	  FD_CLR(i, &jfds);
        }
      }

      if((nsel = select(nmax, 0, &jfds, 0, &tout)) <= 0)
      {
	if(nsel < 0 && errno != EINTR) 
	{
          CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SELECTERROR, strerror(errno));
          throw(ex1);
        }
      }
      else
      {
	int cout;

        for(i = nmin; i < nmax; ++i) 
	{
	  if(FD_ISSET(i, &jfds))
	  {
	    // send a trigger 
            DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: send char on fd = " << i);
	    c = 1;
	    cout = send(i, &c, sizeof(char), 0);
            DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: sent char. status = " << cout);

	    if(cout == 0)   // connection has been closed 
	    {
	      close(i);
	      FD_CLR(i, &m_fds);
	      FD_CLR(i, &jfds);
              CREATE_ROS_EXCEPTION(e, ModulesException, TCPTRIGGER_CONNECTIONCLOSED, " fd = " << i);
              ers::warning(e);
	      continue;
	    }
	    else if(cout < 0 && errno != EINTR)
	    {
              CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_SENDERROR, strerror(errno));
              throw(ex1);
	      continue;
	    }

	    FD_SET(i, &fd_done);
	  }
        }
      }
    }

    m_trigger++;

    // get the busy reset 
    DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: wait for the busy reset ");

    FD_ZERO(&fd_done);

    while(memcmp(&fd_done, &m_fds, sizeof(fd_set)))
    {
      int i;

      memcpy(&jfds, &m_fds, sizeof(fd_set));
      memcpy(&tout, &m_rdtout, sizeof(struct timeval));

      for(i = nmin; i < nmax; ++i) 
      {
	if(FD_ISSET(i, &fd_done)) 
	{
          DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: clear fd # " << i);
	  FD_CLR(i, &jfds);
        }
      }

      DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: do select, nmax =  " << nmax);
      if((nsel = select(nmax, &jfds, 0, 0, &tout)) <= 0)
      {
        DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: " << strerror(errno));
	if(nsel < 0 && errno != EINTR) 
	{
          CREATE_ROS_EXCEPTION(ex1,ModulesException,TCPTRIGGER_SELECTERROR,strerror(errno));
          throw(ex1);
        }
      }
      else
      {  
	int cout;
        DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: test fds ");

        for(i = nmin; i < nmax; ++i) 
	{
	  if(FD_ISSET(i, &jfds))
	  {
            DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: read  char on fd = " << i);
	    cout = read(i, &c, sizeof(char));

	    if(cout == 0)   // connection has been closed 
	    {
	      close(i);
	      FD_CLR(i, &m_fds);
	      FD_CLR(i, &jfds);
              CREATE_ROS_EXCEPTION(e, ModulesException, TCPTRIGGER_CONNECTIONCLOSED, " fd = " << i);
              ers::warning(e);
	      continue;
	    }
	    else if(cout < 0 && errno != EINTR)
	    {
              CREATE_ROS_EXCEPTION(ex1, ModulesException, TCPTRIGGER_RECEIVEERROR, strerror(errno));
              throw(ex1);
	      continue;
	    }

	    FD_SET(i, &fd_done);
	  }
        }
      }
    }
  }

  m_reset++;
  DEBUG_TEXT(DFDB_ROSFM, 15, "TCPTriggerDataChannel::getFragment: Exited");
  return(0);
}
