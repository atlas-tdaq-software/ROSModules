// $Id$
/*
  ATLAS ROS Software

  Class: EmulatedReadoutModule
  Authors: ATLAS ROS group 	
*/

#include <stdint.h>
#include <string.h>
#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"
#include "ROSModules/EmulatedReadoutModule.h"
#include "ROSModules/EmulatedDataChannel.h"


using namespace ROS;


/********************************************/
EmulatedReadoutModule::EmulatedReadoutModule()
/********************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, " EmulatedReadoutModule::constructor: Entered (empty function)");
}


/*********************************************/
EmulatedReadoutModule::~EmulatedReadoutModule() noexcept
/*********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::destructor entered");
  while (m_dataChannels.size() > 0) {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;
  }

  m_requestLock->destroy();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::destructor completed");
}


/***********************************************************************/
void EmulatedReadoutModule::setup(DFCountedPointer<Config> configuration) 
/***********************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::setup: called");
  m_configuration = configuration;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::setup: done");
}


/*****************************************/
void EmulatedReadoutModule::configure(const daq::rc::TransitionCmd&)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::configure: called");

  // Now initialise the variables needed to emulate the ROBIN request Queue: 
  // requests cannot overtake each other.
  m_requestLock = DFFastMutex::Create();

  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedReadoutModule::configure: m_requestLock = " << m_requestLock);
  ts_clock(&m_busyUntil);
  m_statistics.ppcIdleCount = 0;

  int numberOfDataChannels = m_configuration->getInt("numberOfChannels");
  for (int i = 0; i < numberOfDataChannels; i++) 
  {
    unsigned int id                 = m_configuration->getInt("channelId", i);
    unsigned int rolPhysicalAddress = m_configuration->getInt("ROLPhysicalAddress", i);
    int nPoolPages                  = m_configuration->getInt("memoryPoolNumPages", i);
    int poolPageSize                = m_configuration->getInt("memoryPoolPageSize", i);
    int poolType                    = m_configuration->getInt("poolType", i);
    DataChannel *channel = new EmulatedDataChannel(id,
						   i,			// config Id
                                                   rolPhysicalAddress,
                                                   nPoolPages,
                                                   poolPageSize,
                                                   poolType,
						   this,
                                                   m_configuration,
                                                   8);
    m_dataChannels.push_back(channel);
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::configure: done");
}


/*****************************************/
void EmulatedReadoutModule::clearInfo(void)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::clearInfo: called");
  m_statistics.ppcIdleCount = 0;
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::clearInfo: done");
}


//MJ: called from?
/*********************************************************************************/
int EmulatedReadoutModule::getUserCommandStatistics(std::string command,
                                                     unsigned char *buffer,
                                                     unsigned int maxSize,
                                                     unsigned long physicalAddress)
/*********************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 7, "EmulatedReadoutModule::getUserCommandStatistics entered");
  int used = 0;
  if (command != "ignore") {
     DEBUG_TEXT(DFDB_ROSFM, 7, "EmulatedReadoutModule::getUserCommandStatistics buffer=" << HEX((uintptr_t)buffer) << ", physicalAddress=" << HEX(physicalAddress));
     size_t spaceLeft = maxSize;
     if (spaceLeft > 4) {
        uint32_t *size = (uint32_t*) buffer;
        used = 4;
        buffer += used;
        spaceLeft -= used;
        if (command.size() < spaceLeft) {
           strcpy((char*)buffer, command.c_str());
           used += command.size();
        }
        if (used % 4 != 0) {
           used = used - (used % 4) + 4;  // round up to multiple of 4
        }
        *size = used;
     }
  }
  DEBUG_TEXT(DFDB_ROSFM, 7, "EmulatedReadoutModule::getUserCommandStatistics, done. returning " << used);
  return used;
}


/*********************************************************************/
const std::vector<DataChannel *> *EmulatedReadoutModule::channels(void) 
/*********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::channels: called / done");
  return &m_dataChannels;
}


// This method is in the Module class and not with the DataChannel because it emulates 
// message passing between ROS and Robin.
/**************************************************************/
tstamp EmulatedReadoutModule::scheduleRequest(unsigned int usec) 
/**************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::scheduleRequest: called");
  // Schedule a new request and calculate until when the module will be busy
  tstamp now, busyUntil;

  m_requestLock->lock();

  ts_clock(&now);
  if (ts_compare(now, m_busyUntil) > 0) {
    m_busyUntil = now;
    m_statistics.ppcIdleCount++;
  }
  ts_offset(&m_busyUntil, usec);
  busyUntil = m_busyUntil;  // This copy is needed to avoid race conditions

  m_requestLock->unlock();

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedReadoutModule::scheduleRequest: done");
  return busyUntil;
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
   extern ReadoutModule* createEmulatedReadoutModule();
}
ReadoutModule* createEmulatedReadoutModule()
{
   return (new EmulatedReadoutModule());
}


