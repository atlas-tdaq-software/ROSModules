//$Id: $


#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <errno.h>  
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>  // struct sockaddr_in ...
#include <netinet/tcp.h> // TCP_NODELAY
#include <sys/socket.h>

#include "TcpDataChannel.h"
#include "TcpReadoutModule.h"
#include "ROSModules/ModulesException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;


/******************************************************/
TcpReadoutModule::TcpReadoutModule() {
/******************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::constructor: Called");
}


/*******************************************************/
TcpReadoutModule::~TcpReadoutModule() noexcept {
/*******************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::destructor: Entered");

   while (m_dataChannels.size() > 0) {
      DataChannel *channel = m_dataChannels.back();
      m_dataChannels.pop_back();
      delete channel;
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::destructor: Done");
}


/****************************************************************************/
void TcpReadoutModule::setup(DFCountedPointer<Config> configuration) {
/****************************************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::setup: Entered");
   m_configuration        = configuration;
   m_numberOfDataChannels = configuration->getInt("numberOfChannels");

   m_connectTimeout=configuration->getInt("ConnectTimeout");

   DEBUG_TEXT(DFDB_ROSFM, 20, "TcpReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::setup: Done");
}


/**********************************************/
void TcpReadoutModule::configure(const daq::rc::TransitionCmd&) {
/**********************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::configure: Entered");

   //Create the DataChannel objects
   for (int chan = 0; chan < m_numberOfDataChannels; chan++) {
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::configure: Now creating a new data channel. Parameters are:");
      unsigned int channelId=m_configuration->getUInt("channelId", chan);
      TcpDataChannel *channel = new TcpDataChannel(channelId, chan, m_configuration);
      m_dataChannels.push_back(channel);
      DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::configure: Data channel created");
   }

   m_socket = socket(AF_INET, SOCK_STREAM, 0);
   if (m_socket == -1) {
      CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_SOCKET, "");
      throw(ex1);
   }
   if (setSocketOptions(m_socket)) {
      CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_SSO, "");
      throw(ex1);
   }

   sockaddr_in sin;
   bzero((char *)&sin, sizeof(sin));
   sin.sin_family=AF_INET;
   sin.sin_addr.s_addr=htonl(INADDR_ANY);   

   int port=m_configuration->getInt("Port");
   sin.sin_port=htons(port);
   if (bind(m_socket, (struct sockaddr *)&sin, sizeof(sin)) == -1) {
      CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_BIND, "port = " << sin.sin_port);
      throw(ex1);
   }

   if (listen (m_socket, SOMAXCONN) == -1) {
      CREATE_ROS_EXCEPTION(ex1, ModulesException, ETH_LISTEN, "port = " << sin.sin_port);
      throw(ex1);
   }

   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::configure: Done");
}


int TcpReadoutModule::setSocketOptions(int fd)
/****************************************************/
{
  int sockopt = 0;
  int SOCKET_ERROR = -1;
  static const int c_so_rcvbuf = 256 * 1024; // limit set by Linux!
  static const int c_so_sndbuf = 256 * 1024; // limit set by Linux!

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::SetSocketOptions: Entered");

  // setsockopt SO_REUSEADDR - enable/disable local address reuse
  sockopt = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR) {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt1");
    return -1;
  }

  // setsockopt SO_SNDBUF - set buffer size for output
  sockopt = c_so_sndbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR) {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt2");
    return -1;
  }
 
  // setsockopt SO_RCVBUF - set buffer size for input
  sockopt = c_so_rcvbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR) {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt3");
    return -1;
  }

  // setsockopt TCP_NODELAY - to defeat the packetization algorithm
  sockopt = 0; // 0 to enable Nagle algorithm
  if (setsockopt(fd, getprotobyname("tcp")->p_proto, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) == SOCKET_ERROR) {
    DEBUG_TEXT(DFDB_ROSFM, 5, "EthSequentialDataChannel::SetSocketOptions  error from setsockopt4");
    return -1;
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EthSequentialDataChannel::SetSocketOptions: Done OK");
  return 0;
}

/************************************************/
void TcpReadoutModule::unconfigure(const daq::rc::TransitionCmd&) {
/************************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::unconfigure: Entered");

   close(m_socket);

   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::unconfigure: Done");
}


/********************************************/
void TcpReadoutModule::connect(const daq::rc::TransitionCmd&) {
/********************************************/
   DEBUG_TEXT(DFDB_ROSFM, 20, "TcpReadoutModule::connect: Entered");


   struct timeval now;
   gettimeofday(&now, 0);
   long startTime=now.tv_sec*1000000+now.tv_usec;
   long elapsedTime=0;
   int channelsConnected=0;
   while (channelsConnected<m_numberOfDataChannels && elapsedTime<m_connectTimeout*1000) {
      fd_set recvFds;
      FD_ZERO(&recvFds);
      FD_SET(m_socket, &recvFds);
      struct timeval selectTimeout;
      selectTimeout.tv_sec=m_connectTimeout/1000;
      selectTimeout.tv_usec=m_connectTimeout%1000;
      int sel = select(m_socket+1, &recvFds, NULL, NULL, &selectTimeout);
      if (sel > 0) {
         if (FD_ISSET(m_socket, &recvFds)) {
            struct sockaddr_in sockAddress;
            socklen_t slen=sizeof(sockAddress);
            int fd=accept(m_socket, (struct sockaddr *)&sockAddress, &slen);
            dynamic_cast<TcpDataChannel*>(m_dataChannels[channelsConnected])->connect(fd);
            channelsConnected++;
            std::cout << channelsConnected << " channels of " << m_numberOfDataChannels << " connected\n";
         }
      }
      else if (sel==0) {
            CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT,"Timeout waiting for connection");
            throw(ex1);
      }
      else {
         CREATE_ROS_EXCEPTION(ex1,ModulesException,ETH_SELECT," errno="<<errno);
         throw(ex1);
      }

      gettimeofday(&now,0);
      long usec=now.tv_sec*1000000+now.tv_usec;
      elapsedTime=usec-startTime;
      std::cout << elapsedTime << " usec elapsed\n";
   }

   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::connect: Done");
}


/***********************************************/
void TcpReadoutModule::disconnect(const daq::rc::TransitionCmd&) {
/***********************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::disconnect: Entered");
   for (int chan = 0; chan < m_numberOfDataChannels; chan++) {
      dynamic_cast<TcpDataChannel *>(m_dataChannels[chan])->disconnect();
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::disconnect: Done");
}
    

/**************************************************/    
void TcpReadoutModule::prepareForRun(const daq::rc::TransitionCmd&) {
/**************************************************/    
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::prepareForRun: Entered");
   for (int chan = 0; chan < m_numberOfDataChannels; chan++) {
      dynamic_cast<TcpDataChannel *>(m_dataChannels[chan])->prepareForRun();
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::prepareForRun: Done");
}


/*******************************************/
void TcpReadoutModule::stopGathering(const daq::rc::TransitionCmd&) {
/*******************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::stopFE: Called");
   for (int chan = 0; chan < m_numberOfDataChannels; chan++) {
      dynamic_cast<TcpDataChannel *>(m_dataChannels[chan])->stopEB();
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::stopFE: Done");
}


/******************************************/
void TcpReadoutModule::clearInfo() {
/******************************************/
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::clearInfo:  Entered");

   for(int chan = 0; chan < m_numberOfDataChannels; chan++){
      dynamic_cast<TcpDataChannel *>(m_dataChannels[chan])->clearInfo();
   }
   DEBUG_TEXT(DFDB_ROSFM, 15, "TcpReadoutModule::clearInfo:  Done");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createTcpReadoutModule();
}
ReadoutModule* createTcpReadoutModule()
{
  return (new TcpReadoutModule());
}
