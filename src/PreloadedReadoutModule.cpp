// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.10  2008/07/18 11:12:20  gcrone
//  Specify namespace std:: where necessary
//
//  Revision 1.9  2007/04/19 13:12:18  gcrone
//  Use ROS specific data file if it exists
//
//  Revision 1.8  2005/07/15 15:55:58  gcrone
//  Get file names as a vector.  Pool type is in "poolType" not just "pool".
//
//  Revision 1.7  2005/01/22 15:56:18  jorgen
//   update to DataChannel interface with config ID
//
//  Revision 1.6  2005/01/14 13:39:05  joos
//  Adaptations to new API in DFSubSystemItem/Config.cpp
//
//  Revision 1.5  2004/03/31 16:21:14  gcrone
//  Added unconfigure(), call static configure/unconfigure methods of
//  PreloadedDataChannel.
//
//  Revision 1.4  2004/03/30 19:16:53  gcrone
//  Delete DataChannels in the destructor!
//
//  Revision 1.3  2004/03/30 13:19:39  gcrone
//  Removed ROS from plugin entry point name (:  Another new module
//  relying on PluginFactory's compatability hack.
//
//  Revision 1.2  2004/03/24 20:26:48  gcrone
//  Add implementations for missing methods.
//
//  Revision 1.1  2004/03/24 19:03:09  gcrone
//  Added Preloaded ReadoutModule and DataChannel
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSModules/PreloadedReadoutModule.h"
#include "ROSModules/PreloadedDataChannel.h"

#include "DFSubSystemItem/Config.h"

using namespace ROS;

PreloadedReadoutModule::PreloadedReadoutModule()
{

}
PreloadedReadoutModule::~PreloadedReadoutModule() noexcept
{
   while (m_dataChannels.size() > 0) {
      DataChannel *channel = m_dataChannels.back();
      m_dataChannels.pop_back();
      delete channel;
   }
}


void PreloadedReadoutModule::setup(DFCountedPointer<Config> configuration)
{
   int numberOfDataChannels=configuration->getInt("numberOfChannels");
   m_fileTag = configuration->getString("appName");

   for (int link=0; link<numberOfDataChannels; link++) {
      unsigned int robId = configuration->getInt("channelId", link);
      int nPoolPages  = configuration->getInt("memoryPoolNumPages", link);
      int poolPageSize = configuration->getInt("memoryPoolPageSize", link);
      int poolType = configuration->getInt("poolType", link);

      DataChannel *channel = new
         PreloadedDataChannel(robId,
			      link,		// config Id
                              nPoolPages,
                              poolPageSize,
                              poolType);
      m_dataChannels.push_back(channel);
   }

   m_dataFiles=configuration->getVector<std::string>("dataFiles");
}

void PreloadedReadoutModule::configure(const daq::rc::TransitionCmd&)
{
   // Static method globally configures PreloadedDataChannel
   PreloadedDataChannel::configure(m_dataFiles, m_fileTag);
}

void PreloadedReadoutModule::unconfigure(const daq::rc::TransitionCmd&)
{
   // Static method globally unconfigures PreloadedDataChannel
   PreloadedDataChannel::unconfigure();
}

void PreloadedReadoutModule::clearInfo()
{
   // Dummy for now
}

DFCountedPointer<Config> PreloadedReadoutModule::getInfo()
{
   // Dummy for now
  DFCountedPointer<Config> dummy=Config::New();
  return (dummy);
}

extern "C" 
{
   extern ReadoutModule* createPreloadedReadoutModule();
}
ReadoutModule* createPreloadedReadoutModule()
{
   return (new PreloadedReadoutModule());
}
