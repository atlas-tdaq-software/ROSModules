//$Id:  $


#ifndef TCPREADOUTMODULE_H
#define TCPREADOUTMODULE_H

#include "ROSInterruptScheduler/InterruptHandler.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"

#include <sys/select.h>

namespace ROS
{
  class TcpReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    TcpReadoutModule();
    virtual ~TcpReadoutModule() noexcept;

    virtual const std::vector<DataChannel *> *channels();

  private:
    int setSocketOptions(int fd);
    int m_numberOfDataChannels;
    DFCountedPointer<Config> m_configuration;
    std::vector<DataChannel *> m_dataChannels;	// generic DataChannels ??

    int m_socket;
    int m_connectTimeout;

  };

  inline const std::vector<DataChannel *> * TcpReadoutModule::channels()
  {
    return &m_dataChannels;
  }
}
#endif // TCPREADOUTMODULE_H
