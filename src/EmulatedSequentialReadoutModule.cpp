//$Id$
/************************************************************************/
/*									*/
/* File             : EmulatedSequentialReadoutModule.cpp		*/
/*									*/
/* Authors          : B.Gorini, M.Joos, J.Petersen ATD/CERN		*/
/*									*/
/********* C 2008 - ROS/RCD  ********************************************/

#include <unistd.h>
#include <iostream>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/EmulatedSequentialDataChannel.h"
#include "ROSModules/EmulatedSequentialReadoutModule.h"


using namespace ROS;


/****************************************************************/
EmulatedSequentialReadoutModule::EmulatedSequentialReadoutModule() :
   m_numberOfDataChannels(0)
/****************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::constructor: Entered (empty function)");
}


/*****************************************************************/
EmulatedSequentialReadoutModule::~EmulatedSequentialReadoutModule() noexcept
/*****************************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::destructor: Entered");

  while(m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel; // IOManager::unconfigure calls both EmulatedSequentialReadoutModule::unconfigure and
                    // this destructor. For historic reasons the destruction of the data channels is done here and not in unconfigure
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::destructor: Done");
}


// setup gets parameters from configuration
/*********************************************************************************/
void EmulatedSequentialReadoutModule::setup(DFCountedPointer<Config> configuration) 
/*********************************************************************************/
{ 
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::setup: Entered");

  m_configuration = configuration;
  m_numberOfDataChannels = configuration->getInt("numberOfChannels");
  DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialReadoutModule::setup: numberOfDataChannels = " << m_numberOfDataChannels);

  // initialize the vectors for the channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
    m_usePolling.push_back(0);
  }

  for(int chan = 0; chan < m_numberOfDataChannels; chan++)  //Two loops for clarity!?
  {
    m_id[chan] = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_ROSFM, 20, "EmulatedSequentialReadoutModule::setup: chan = "
                                << chan << "  id = " << m_id[chan] << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
    m_usePolling[chan] = true;
  }
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::setup: Done");
}


/***************************************************/
void EmulatedSequentialReadoutModule::configure(const daq::rc::TransitionCmd&)
/***************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::configure: Entered");

  //Create the DataChannel objects
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    // chan == the Config Id
    EmulatedSequentialDataChannel *channel = new EmulatedSequentialDataChannel(
						 m_id[chan],
					         chan,
						 m_rolPhysicalAddress[chan],
						 m_usePolling[chan],
						 m_configuration);
    m_dataChannels.push_back(channel);
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::configure: Done");
}



/*******************************************************/    
void EmulatedSequentialReadoutModule::prepareForRun(const daq::rc::TransitionCmd&)
/*******************************************************/    
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::prepareForRun: Entered");

  sleep(1);
  std::cout << " EmulatedSequentialReadoutModule::prepareForRun: waited a second " << std::endl;

  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EmulatedSequentialDataChannel *>(m_dataChannels[chan])->prepareForRun();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::prepareForRun: Done");
}


/************************************************/
void EmulatedSequentialReadoutModule::stopGathering(const daq::rc::TransitionCmd&)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::stopGathering: Entered");
  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
    dynamic_cast<EmulatedSequentialDataChannel *>(m_dataChannels[chan])->stopEB();
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::stopGathering: Done");
}


/***********************************************/
void EmulatedSequentialReadoutModule::clearInfo()
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedSequentialReadoutModule::clearInfo: Entered. Empty function!");

}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createEmulatedSequentialReadoutModule();
}
ReadoutModule* createEmulatedSequentialReadoutModule()
{
  return (new EmulatedSequentialReadoutModule());
}
