tdaq_package()

tdaq_generate_isinfo(ROSModules_ISINFO schema/ROSModulesInfo.schema.xml
  NAMESPACE ROSInfo
  NO_NAMESPACE
  OUTPUT_DIRECTORY ROSInfo
  CPP_OUTPUT is_cpp_srcs)

tdaq_generate_isinfo(ROSModules_JAVA_ISINFO schema/ROSModulesInfo.schema.xml
  NO_NAMESPACE
  PACKAGE ROSModules_IS_Info
  OUTPUT_DIRECTORY ROSModules_IS_Info
  CLASSNAME RobinDataChannelInfo
  JAVA_OUTPUT is_java_srcs)

tdaq_add_is_schema(schema/ROSModulesInfo.schema.xml)

if("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
  add_definitions(-DnoTSTAMP)
endif()

tdaq_add_library(ROSTcpReadoutModule 
  src/TcpReadoutModule.cpp 
  src/TcpDataChannel.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore 
  LINK_LIBRARIES ROSCore ROSVMEInterrupts)

tdaq_add_library(ROSPreloadedDataChannel	
  src/PreloadedDataChannel.cpp
  ${is_cpp_srcs} 
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore tdaq-common::eformat tdaq-common::DataReader)

tdaq_add_library(ROSEmulatedReadoutModule		
  src/EmulatedReadoutModule.cpp
  src/EmulatedDataChannel.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES SubSystemItem ROSCore)

tdaq_add_library(ROSPreloadedReadoutModule 
  src/PreloadedReadoutModule.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore ROSPreloadedDataChannel SubSystemItem)

tdaq_add_library(ROSEmulatedSequentialReadoutModule 
  src/EmulatedSequentialReadoutModule.cpp
  src/EmulatedSequentialDataChannel.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore 
  LINK_LIBRARIES ROSCore ROSVMEInterrupts SubSystemItem)

tdaq_add_library(ROSEmulatedSingleFragmentReadoutModule 
  src/EmulatedSingleFragmentReadoutModule.cpp
  src/EmulatedSingleFragmentDataChannel.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore SubSystemItem)

tdaq_add_library(ROSTCPTriggerReadoutModule	
  src/TCPTriggerReadoutModule.cpp
  src/TCPTriggerDataChannel.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore SubSystemItem)

tdaq_add_library(ROSEthSequentialReadoutModule 
  src/EthSequentialReadoutModule.cpp
  src/EthSequentialDataChannel.cpp	 
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore ROSVMEInterrupts SubSystemItem)

tdaq_add_library(ROSFilarReadoutModule
  src/FilarReadoutModule.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore filar SubSystemItem)

tdaq_add_library( ROSSSPCIReadoutModule
  src/SSPCIReadoutModule.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore
  LINK_LIBRARIES ROSCore ROSslink SubSystemItem)

tdaq_add_library(ROSRobinReadoutModule
  src/RobinReadoutModule.cpp
  src/RobinDataChannel.cpp
  src/DDTScheduledUserAction.cpp
  ${is_cpp_srcs}
  INCLUDE_DIRECTORIES ROSCore ROSInterruptScheduler
  LINK_LIBRARIES ROSCore SubSystemItem robin robin_rol rcc_time_stamp ROSRobinInterrupts ROSUserActionScheduler)

tdaq_add_executable(test_Robin
  src/test/test_Robin.cpp
  LINK_LIBRARIES ROSRobinReadoutModule ROSCore robin robin_rol rcc_time_stamp getinput is ROSMemoryPool)

tdaq_add_jar(ROSModules_IS
  ${is_java_srcs}
  INCLUDE_JARS is/is.jar ipc/ipc.jar 
  )
