// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: EmulatedDataChannel
  Authors: ATLAS ROS group 	
*/


#ifndef EMULATEDDATACHANNEL_H
#define EMULATEDDATACHANNEL_H

#include "ROSCore/DataChannel.h"
//#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
//#include "DFThreads/DFThread.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "DFSubSystemItem/Config.h"
#include "ROSInfo/EmulatedDataChannelInfo.h"
#include "ROSModules/EmulatedReadoutModule.h"


namespace ROS {
  class EmulatedDataChannel : public DataChannel {
  public:
    EmulatedDataChannel(unsigned int id,
			unsigned int configId,
                        unsigned int rolPhysicalAddress,
			int nPoolPages, int poolPageSize, int poolType,
                        EmulatedReadoutModule *module,
			DFCountedPointer<Config> configuration,
			int numberOfTickets = 8);
     EmulatedDataChannel(const EmulatedDataChannel&)=delete;
     EmulatedDataChannel& operator=(const EmulatedDataChannel&)=delete;

    virtual ~EmulatedDataChannel() ;
    int requestFragment(int level1Id);
    ROS::EventFragment *getFragment(int ticket);
    void releaseFragment(const std::vector <unsigned int> *level1Ids);
    void releaseFragment(int level1Id);
    void clearInfo(void);
    virtual ISInfo *getISInfo(void);
    
  private:
    int m_getDelay;	                          // DB parameter "GetFragmentDelay"
    int m_releaseDelay;                           // DB parameter "ReleaseDelay"
    int m_releaseGroupDelay;	                  // DB parameter "ReleaseGroupDelay"
    int m_messageDelay;	                          // DB parameter "MessageDelay"
    unsigned int m_channelPhysicalAddress;	  // logical channel ID!  //MJ: Used?
    int m_bunchCrossingID;	                  // MJ: Used?
    int m_triggerType;   	                  // MJ: Could be simplyfied
    int m_nextTicket;	                          // index for m_ticketVector 
    int m_numberOfTickets;	                  // Hardwired to "8" by call to constructor of the dataChannel in the module. This parameter defines the number of emulated requests that can be executed in parallel
    tstamp *m_requestVector;	                  // Implements the H/W delay between requestFragment and getFragment
    int *m_ticketVector;	                  // Provides tickets. //MJ: It seems this could be simplified	
    int *m_l1idVector;	                          // Used to transfer L1ID from requestFragment to getFragment
    unsigned int m_detectorEventType;	          // DB parameter "detectorEventType"
    unsigned int m_numberOfStatusElements;	  // DB parameter "NumberOfStatusElements"
    unsigned int m_numberOfDataElements;	  // Number of data words in the ROB (ROD) fragment 
    unsigned int m_statusBlockPosition;	          // DB parameter "StatusBlockPosition"
    float m_getMiss;	                          // DB parameter "GetMissingFragment". Controls the chance for a "may come"
    DFFastMutex *m_mutex;	                  // REQUESTMUTEX for access to the event storage memory
    DFFastMutex *m_hwmutex; 	                  // For sequential (emulated) message passing with the Robin 
    DFFastMutex *m_ticketMutex;	                  // For sequential access to the ticket generator
    MemoryPool *m_memoryPool;	                  // Memory for the creation of emulated ROB fragments
    EmulatedReadoutModule *m_module;	          // Pointer to the module to which this channel belongs
    EmulatedDataChannelInfo m_statistics;	  // For IS
    std::vector <int> m_QueueHist;                // histogram of ROBIN request Q occupancy
    unsigned int m_noYieldsGetFragmentWaitReply;  // # yields waiting for reply in getFragment
    unsigned int m_noYieldsReleaseWaitReply;      // # yields waiting for reply in releaseFragment
    unsigned int m_noYieldsGetTicket;             // # yields waiting for ticket

    int getNewTicket();	                           
    void releaseTicket(int ticket);        	   	  
  };
}
#endif //EMULATEDDATACHANNEL_H
