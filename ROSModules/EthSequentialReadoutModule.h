//$Id$

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: EthSequentialReadoutModule					*/
/*  Author: M.Joos							*/
/*									*/
/*** C 2005 - Ecosoft: Made from at least 80% recycled source code ******/

#ifndef ETHSEQUENTIALMODULE_H
#define ETHSEQUENTIALMODULE_H

#include "ROSInterruptScheduler/InterruptHandler.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"

namespace ROS
{
  class EthSequentialReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    EthSequentialReadoutModule();
    virtual ~EthSequentialReadoutModule()noexcept;

    virtual const std::vector<DataChannel *> *channels();

  private:
    int m_numberOfDataChannels;
    DFCountedPointer<Config> m_configuration;
    std::vector <unsigned int> m_id;
    std::vector <unsigned int> m_rolPhysicalAddress;
    std::vector <bool> m_usePolling;
    std::vector<DataChannel *> m_dataChannels;	// generic DataChannels ??
    InterruptHandler *m_handler;		// generalise: generic handlers ..
  };

  inline const std::vector<DataChannel *> * EthSequentialReadoutModule::channels()
  {
    return &m_dataChannels;
  }
}
#endif // ETHSEQUENTIALMODULE_H
