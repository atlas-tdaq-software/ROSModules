// $Id$

/*
  ATLAS TDAQ ROS/RCD

  Class:TCPTriggerDataChannel 
  Authors: E. Pasqualucci, J.Petersen

*/

#ifndef TCPTRIGGERDATACHANNEL_H
#define TCPTRIGGERDATACHANNEL_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>      /* close ... */
#include <fcntl.h>       /* fcntl ... */
#include <sys/time.h>    /* select struct timeval */
#include <string.h>      /* memset & bzero in FD_ZERO */
#include <netinet/in.h>  /* struct sockaddr_in ... */
#include <arpa/inet.h>   /* for inet_addr */
#include <netinet/tcp.h> /* TCP_NODELAY */
#include <sys/socket.h>  /* socket setsockopt bind listen connect */
#include <netdb.h>       /* getprotobyname gethostbyaddr */
#include <errno.h>       /* errno */

#include "ROSCore/DataChannel.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "DFSubSystemItem/Config.h"

#define TCPTRIG_WRTIMEOUT      {0,100000}
#define TCPTRIG_RDTIMEOUT      {0,100000}

namespace ROS {

  class TCPTriggerDataChannel : public DataChannel
  {
   public:
    TCPTriggerDataChannel(unsigned int id, unsigned int configId, unsigned int rolPhysicalAddress,
                          DFCountedPointer<Config> configuration);


    virtual ~TCPTriggerDataChannel ();

    virtual EventFragment *getFragment (int ticket);
    virtual int requestFragment (int nrequested);
    virtual void clearInfo ();
    virtual DFCountedPointer<Config> getInfo ();

    /** Not relevant for Rod Crate DAQ */

    void releaseFragment (const std::vector < unsigned int >* /*level1Ids*/) {};
    void releaseFragment (int /*level1Id*/) {};

    /** Specific methods */

    bool enable ()            { return m_enable; };
    void enable (bool e)      { m_enable = e; };

    int  descriptor ()        { return m_fd0; };
    void descriptor (int fd0) { m_fd0 = fd0; };

    fd_set *fdset ()          { return &m_fds; };
    void    fdset (fd_set *f) { memcpy (&m_fds, f, sizeof (fd_set)); };

    int fdmin ()              { return m_fdmin; };
    void fdmin (int fmin)     { m_fdmin = fmin; };

    int fdmax ()              { return m_fdmax; };
    void fdmax (int fmax)     { m_fdmax = fmax; };

  private:
    DFCountedPointer<Config>    m_outinfo;

    bool m_enable;

    int                         m_busySources;
    int                         m_trigger;
    int                         m_reset;

    struct timeval              m_rdtout;
    struct timeval              m_wrtout;

    int                         m_fd0;
    int                         m_fdmax, m_fdmin;
    fd_set                      m_fds, m_zfds;
  };
}
#endif // TCPTRIGGERDATACHANNEL_H
