//$Id$

/*
  ATLAS ROS/RCD Software

  Class: TCPTriggerReadoutModule
  Author: Enrico Pasqualucci, J.O.Petersen
*/

#ifndef TCPTRIGGERMODULE_H
#define TCPTRIGGERMODULE_H

#include "ROSModules/TCPTriggerDataChannel.h"
#include "ROSCore/ReadoutModule.h"

namespace ROS

{
  class TCPTriggerReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    TCPTriggerReadoutModule();
    virtual ~TCPTriggerReadoutModule()noexcept;

    virtual const std::vector<DataChannel *> * channels();

  private:

    DFCountedPointer<Config> m_configuration;

    // channel parameters
    std::vector <unsigned int> m_id;
    std::vector <unsigned int> m_rolPhysicalAddress;

    int m_numberOfDataChannels;
    std::vector<DataChannel *> m_dataChannels;	// generic DataChannels ??

    DFCountedPointer<Config> m_modInfo;
    int m_fd0;
    int  m_fdmax, m_fdmin;
    int m_nconnected;
    int m_tout;

    fd_set m_fds;

    unsigned short m_port;

  };

  inline const std::vector<DataChannel *> * TCPTriggerReadoutModule::channels()
  {
    return &m_dataChannels;
  }

}
#endif // TCPTRIGGERMODULE_H
