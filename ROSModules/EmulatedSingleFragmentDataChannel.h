// $Id$

/*
  ATLAS ROS Software

  Class: EmulatedSingleFragmentDataChannel
  Authors: G. Crone, B.Gorini, J.Petersen

*/

#ifndef EMULATEDSINGLEFRAGMENTDATACHANNEL_H
#define EMULATEDSINGLEFRAGMENTDATACHANNEL_H

#include "ROSInfo/EmulatedSingleFragmentDataChannelInfo.h"
#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS {

  class EmulatedSingleFragmentDataChannel : public SingleFragmentDataChannel {
  public:
    
    EmulatedSingleFragmentDataChannel(unsigned int id, unsigned int configId,
                                      unsigned long rolPhysicalAddress,
                                      DFCountedPointer<Config> configuration,
                                      EmulatedSingleFragmentDataChannelInfo* info = new EmulatedSingleFragmentDataChannelInfo());
    
    virtual ~EmulatedSingleFragmentDataChannel() ;

    virtual int getNextFragment(unsigned int* buffer, int max_size,
                                unsigned int* status,
                                unsigned long pciAddress = 0);

    void clearInfo(void);
    virtual ISInfo* getISInfo(void);

  private:

  EmulatedSingleFragmentDataChannelInfo* m_statistics;
  
  };
  
}
#endif //EMULATEDSINGLEFRAGMENTDATACHANNEL_H
