// $Id$
// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: EmulatedReadoutModule
  Authors: ATLAS ROS group 	
*/


#ifndef EMULATEDREADOUTMODULE_H
#define EMULATEDREADOUTMODULE_H

#include "DFThreads/DFFastMutex.h"
#include "ROSCore/ReadoutModule.h"
#include "ROSInfo/EmulatedReadoutModuleInfo.h"


namespace ROS {
  class EmulatedReadoutModule : public ReadoutModule {
  public:
    EmulatedReadoutModule();
    virtual ~EmulatedReadoutModule()noexcept;
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void clearInfo(void);
    virtual int getUserCommandStatistics(std::string command, unsigned char *buffer,
                                         unsigned int maxSize, unsigned long physicalAddress);
    virtual const std::vector<DataChannel *> *channels(); //Get the list of channels connected to this module
    ISInfo *getISInfo(void);
    tstamp scheduleRequest(unsigned int usec);	
    
    unsigned int m_robInId;		        // The RobIn ID number (= PCI logical number)  
    //MJ: why public? See EmulatedDataChannel.cpp
    //MJ: It seems m_robInId never gets a value assigned.
  
  private:    
    std::vector<DataChannel *> m_dataChannels;  // Global vector of (emulated) data channels
    DFFastMutex *m_requestLock;	   	        // Emulate ROBIN request Queue
    tstamp m_busyUntil;	                        // Time when the module stops being busy handling the scheduled requests
    DFCountedPointer<Config> m_configuration;   // A configuration object
    EmulatedReadoutModuleInfo m_statistics;     // For IS
  };

  inline ISInfo* EmulatedReadoutModule::getISInfo(){
     return &m_statistics;
  }
}
#endif //EMULATEDREADOUTMODULE_H
