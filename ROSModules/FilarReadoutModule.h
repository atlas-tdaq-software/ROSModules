// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: FilarReadoutModule
  Author: Markus Joos, CERN 	
*/

#ifndef FILARMODULE_H
#define FILARMODULE_H

#include "ROSCore/ReadoutModule.h"
#include "ROSCore/TriggerInputQueue.h"
#include "ROSInfo/FilarReadoutModuleInfo.h"
#include "ROSEventInputManager/EventInputManager.h"
#include "DFThreads/DFFastMutex.h"
#include "DFThreads/DFThread.h"

#define INIT_L1ID 0xffffffff

namespace ROS 
{
  class FilarReadoutModule : public ReadoutModule, public DFThread
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual const std::vector<DataChannel *> * channels();
    FilarReadoutModule();
    virtual ~FilarReadoutModule() noexcept;
    ISInfo* getISInfo();

  protected:
    bool m_triggerQueueFlag;
    u_int m_triggerQueueSize;
    u_int m_triggerQueueUnblockOffset;
    TriggerInputQueue* m_inputToTriggerQueue; 
    
    virtual void run();
    virtual void cleanup();
    
  private:
    std::vector <EventInputManager *> m_eventInputManager;
    std::vector <u_int> m_rod_L1id_ecr;
    std::vector <u_int> m_ecr;
    std::vector <MemoryPool *> m_memoryPool;
    std::vector <MemoryPage *> m_mem_page;
    std::vector <int> m_filar_device;
    int m_fastTestMode;
    int m_filarpagesize;
    int m_numberOfDataChannels;
    int m_filarsizelimit;
    int m_bswap;
    err_str m_rcc_err_str;    
    DFFastMutex *m_fastAllocateMutex;
    std::vector<DataChannel *> m_dataChannels;  // Global vector of physical addresses of emulated links
    FilarReadoutModuleInfo m_statistics;

    bool FragmentsAreThere(u_int l1id, u_int chan);
    int filar_fill(int rol);
    };

  inline const std::vector<DataChannel *> * FilarReadoutModule::channels() 
  {
    return &m_dataChannels;
  }

  inline ISInfo* FilarReadoutModule::getISInfo()
  {
     return &m_statistics;
  }

}
#endif //FILARMODULE_H
