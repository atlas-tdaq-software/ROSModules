// -*- c++ -*-
// $Id$ 

#ifndef PRELOADEDDATACHANNEL_H
#define PRELOADEDDATACHANNEL_H

#include "ROSModules/ModulesException.h"
#include "ROSCore/DataChannel.h"

#include <vector>
#include <set>
#include <map>
#include <string>

#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "DFSubSystemItem/Config.h"

#include "eformat/eformat.h"

namespace ROS {

  class PreloadedDataChannel : public DataChannel {
  public:
     PreloadedDataChannel(const int rolId,
			  const int configId,
                          const int npages,
                          const int pageSize,
                          const int poolType);

    virtual ~PreloadedDataChannel() ;

     /**
      * Called at "configure" to read in data files and store data only
      * for registered ROLs.
      */
     static void configure(const std::vector<std::string>& fileName, const std::string& fileTag);
     static void unconfigure();


    /**
     * Request a Fragment.  It returns a ticket that has to be
     * passed to the getFragment method to obtain the requested fragment.
     * @param level1Id 32bit Level 1 event identifier of fragment to be retrieved.
     */
    int requestFragment(int level1Id) override;

    /**
     * Get an event fragment
     * @param ticket the ticket returned by the preceding requestFragment call
     */
    ROS::EventFragment * getFragment(int ticket) override;
    
    /**
     * Release all the event fragments listed in the vector
     * @param level1Ids Pointer to a vector of 32bit level 1 event identifiers of fragments to be released.
     */
    void releaseFragment(const std::vector < unsigned int > * level1Ids) override;
    
    /**
     * Request an individual event Fragment by Level1 Id
     * @param level1Id 32bit Level 1 event identifier of fragment to release.
     */
    void releaseFragment(int level1Id) override;
    

    /** Reset internal statistics */
    void clearInfo() override;
    
  private:
     static void unpackFragments(const eformat::FullEventFragment<const uint32_t*>& event);
     void* findFragment(const unsigned int level1Id);
     static void loadFile(const std::string& fileSpec);

     unsigned int m_rolId;

     typedef std::map<unsigned int, uint32_t*> EventMap;
     static std::map<unsigned int, EventMap> s_store;
     static std::set<unsigned int> s_rolIdList;

     static bool s_initialised;
     static unsigned int s_storeSize;
     static unsigned int s_maxLevel1;

     DFFastMutex* m_mutex;
     MemoryPool* m_memoryPool;
  };

}
#endif
