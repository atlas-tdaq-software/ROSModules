// -*- c++ -*-

/*
  ATLAS ROS Software

  Class: SSPCIReadoutModule
  Author: Markus Joos, CERN 	
*/

#ifndef SSPCIMODULE_H
#define SSPCIMODULE_H

#include "ROSCore/ReadoutModule.h"
#include "ROSCore/TriggerInputQueue.h"
#include "ROSInfo/SSPCIReadoutModuleInfo.h"
#include "ROSEventInputManager/EventInputManager.h"
#include "DFThreads/DFFastMutex.h"
#include "DFThreads/DFThread.h"
#include "ROSslink/slink.h"

#define INIT_L1ID 0xffffffff

namespace ROS 
{
  class SSPCIReadoutModule : public ReadoutModule, public DFThread
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual const std::vector<DataChannel *> * channels();
    SSPCIReadoutModule();
    virtual ~SSPCIReadoutModule() noexcept;
    ISInfo* getISInfo();

  protected:
    bool m_triggerQueueFlag;
    u_int m_triggerQueueSize;
    u_int m_triggerQueueUnblockOffset;
    TriggerInputQueue* m_inputToTriggerQueue; 
    
    virtual void run();
    virtual void cleanup();
    
  private:
    enum t_slink_input_state {WAIT_SPACE = 1, WAIT_TRANSFER};
    std::vector <EventInputManager *> m_eventInputManager;
    std::vector <MemoryPool *> m_memoryPool;
    std::vector <int> m_robPageSize;
    std::vector <SLINK_device *> m_ssp_device;
    std::vector <t_slink_input_state> m_slink_input_state;
    std::vector <u_int> m_rod_L1id_prev;
    std::vector <MemoryPage *> m_mem_page;
    std::vector <int> m_fragments_input;
    std::vector <int> m_stat_head;
    std::vector <int> m_stat_size;
    std::vector <int> m_stat_l1id;
    int m_yield;
    int m_numberOfDataChannels;
    err_str m_rcc_err_str;
    DFFastMutex * m_fastAllocateMutex;
    
    // Global vector of physical addresses of emulated links 
    std::vector<DataChannel *> m_dataChannels;

    DFCountedPointer<Config> m_configuration;
    
    SSPCIReadoutModuleInfo m_statistics;
    
    bool FragmentsAreThere(u_int l1id);
    u_int ReadSSPCI(int rol);
    void InitReadSSPCI(int rol);
  };

  inline const std::vector<DataChannel *> * SSPCIReadoutModule::channels() 
  {
    return &m_dataChannels;
  }
  
  inline ISInfo* SSPCIReadoutModule::getISInfo()
  {
     return &m_statistics;
  }
}
#endif //SSPCIMODULE_H
