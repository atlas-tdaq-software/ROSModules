// $Id$

/*
  ATLAS ROS Software

  Class: EmulatedSequentialDataChannel
  Authors: B.Gorini, M.Joos, J.Petersen
*/

#ifndef EMULATEDSEQUENTIALDATACHANNEL_H
#define EMULATEDSEQUENTIALDATACHANNEL_H

#include <sys/types.h>
#include "ROSInfo/EmulatedSequentialDataChannelInfo.h"
#include "ROSCore/SequentialDataChannel.h"

namespace ROS 
{
  class EmulatedSequentialDataChannel : public SequentialDataChannel 
  {
  public:
    EmulatedSequentialDataChannel(u_int id, u_int configId, u_int rolPhysicalAddress,
		    bool usePolling, DFCountedPointer<Config> configuration,
		    EmulatedSequentialDataChannelInfo* info = new EmulatedSequentialDataChannelInfo());
    virtual ~EmulatedSequentialDataChannel();
    virtual int getNextFragment(u_int* buffer, int max_size, u_long pciAddress = 0);
    virtual int poll(void);
    void prepareForRun(void);
    void stopEB(void);
    void clearInfo(void);
    virtual void probe();
    virtual ISInfo* getISInfo(void);
    
  private:
    int m_numberOfDataElements;                      //Number of data elements in the emulated ROD fragment
    u_int m_l1id;                                    //The L1ID of the emulated ROD fragment
    bool m_triggerReady;                             //True between "start" and "stop". Controls the poll() method
    EmulatedSequentialDataChannelInfo *m_statistics; //For IS
  };
}
#endif //EMULATEDSEQUENTIALDATACHANNEL_H
