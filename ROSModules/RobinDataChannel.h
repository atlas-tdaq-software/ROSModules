// -*- c++ -*-

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: RobinDataChannel						*/
/*  Author: Markus Joos, PH/ESS						*/	
/**** C 2005 - Ecosoft: Made from at least 70% recycled source code *****/

#ifndef ROBINDATACHANNEL_H
#define ROBINDATACHANNEL_H

#include <vector>
#include <sstream>
#include "rcc_time_stamp/tstamp.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSEventFragment/EventFragment.h"
#include "DFSubSystemItem/Config.h"
#include "ROSModules/ModulesException.h"
#include "ROSCore/DataChannel.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSInfo/RobinDataChannelInfo.h"
#include "ROSRobin/Rol.h"

namespace ROS 
{
  class RobinDataChannel : public DataChannel
  {
  public:
    RobinDataChannel(u_int id, u_int configId, u_int physicalAddress, ROS::Rol &rol_ptr, u_int RobinphysicalAddress, ROS::WrapperMemoryPool *mpool);
    RobinDataChannel(const RobinDataChannel&)=delete;
    RobinDataChannel& operator=(const RobinDataChannel&)=delete;

    ~RobinDataChannel();
    ROS::EventFragment *getFragment(int ticket);
    void releaseFragment(const std::vector < u_int > * level1Ids);
    void releaseFragmentAll(const std::vector < u_int > * level1Ids);
    void releaseFragment(int level1Id);
    //u_long requestFragment(int level1Id);
    int requestFragment(int level1Id);
    int collectGarbage(u_int oldestId);
    void setExtraParameters(int value);
    virtual ISInfo*  getISInfo(void);
    void clearInfo(void);
    void stopFE(void);
    void prepareForRun(void);
    void probe(void);
    void enable(void);
    void disable(void);
    void reset(void);
    u_int64_t swap64(u_int64_t data_in);

  private: 
    ROS::WrapperMemoryPool *m_memoryPool;
    ROS::Rol & m_rol;
    DFCountedPointer<Config> m_configuration;
    DFFastMutex *m_requestMutex;            // The request mutex necessary to access the memory pool
    unsigned long long int m_gcdeleted;
    unsigned long long int m_gcfree;
    u_int m_id;
    u_int m_l1id_modulo;
    u_int m_RobinphysicalAddress;
    u_int m_infoflag;
    tstamp m_tsStopLast;         // time stamp for level1 rate calculation
    u_int64_t m_numberOfLevel1Last;
    RobinDataChannelInfo m_statistics;
    u_int rdw1;
    u_int rdw2;
    u_int rdw3;
    u_int rdw4;
    u_int hdw1;
    u_int hdw2;
    u_int hdw3;
    u_int hdw4;
    u_int m_nextTicket;
    u_int *m_ticketPool;
    MemoryPage** m_pageIndex;    
    
    enum Robin_Messages 
    {
      NOMESSAGE = 0,
      REQUESTFRAGMENT,
      RELEASEFRAGMENT,
      COLLECTGARBAGE
    };
  };
}
#endif
