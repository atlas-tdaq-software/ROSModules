// -*- c++ -*-
// $Id$ 

#ifndef PRELOADEDREADOUTMODULE_H
#define PRELOADEDREADOUTMODULE_H

#include "ROSCore/ReadoutModule.h"

namespace ROS {

   class PreloadedReadoutModule : public ReadoutModule {
   public:
      /**
       * Set/update the values of the class internal variables
       */
      virtual void setup(DFCountedPointer<Config> configuration);
    
      /** Reset internal statistics */
      virtual void clearInfo() ;
    
      /** Get performance statistics */
      virtual DFCountedPointer < Config > getInfo() ;
    
      /** 
       * Get the list of channels connected to this module
       */
      virtual const std::vector<DataChannel *> * channels();

      PreloadedReadoutModule();
      virtual ~PreloadedReadoutModule() noexcept;
 
      /*
       * From IOMPlugin (parent of ReadoutModule)
       */
      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);
  private:
      std::vector<std::string> m_dataFiles;
      std::vector<DataChannel*> m_dataChannels;
      std::string m_fileTag;
   };

   inline const std::vector<DataChannel*>* PreloadedReadoutModule::channels() {
      return &m_dataChannels;
   }
}
#endif
