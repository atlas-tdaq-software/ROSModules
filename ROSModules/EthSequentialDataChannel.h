// $Id$

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: EthSequentialDataChannel					*/
/*  Author: M.Joos							*/
/*									*/
/*** C 2005 - Ecosoft: Made from at least 80% recycled source code ******/

#ifndef ETHSEQUENTIALDATACHANNEL_H
#define ETHSEQUENTIALDATACHANNEL_H

#include "ROSInfo/EthSequentialDataChannelInfo.h"
#include "ROSCore/SequentialDataChannel.h"

namespace ROS 
{
  class EthSequentialDataChannel : public SequentialDataChannel 
  {
  public:
    EthSequentialDataChannel(u_int id, u_int configId, u_int rolPhysicalAddress,
		             bool usePolling, DFCountedPointer<Config> configuration,
			     EthSequentialDataChannelInfo* info = new EthSequentialDataChannelInfo());	// default
    virtual ~EthSequentialDataChannel();
    virtual int getNextFragment(u_int* buffer, int max_size, u_long pciAddress = 0);
    virtual int poll(void);
    void stopEB(void);
    void configure(void);
    void unconfigure(void);
    void disconnect(void);
    void connect(void);
    void prepareForRun(void);
    void cleanupLink(void);
    int SetSocketOptions(int fd);
    int PollEth(void);        

    void clearInfo(void);
    virtual void probe();
    virtual ISInfo* getISInfo(void);

  private:
    bool m_usePolling;
    int m_socketDescriptor;
    int m_fileDescriptor;
    int m_physaddr;
    bool m_triggerReady;
    fd_set m_recv_fds;

    EthSequentialDataChannelInfo* m_statistics;
  };
}
#endif //ETHSEQUENTIALDATACHANNEL_H
