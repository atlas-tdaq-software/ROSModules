//$Id$

/*
  ATLAS ROS Software

  Class: EmulatedSequentialReadoutModule
  Author: J.O.Petersen CERN 	
*/

#ifndef EMULATEDSEQUENTIALMODULE_H
#define EMULATEDSEQUENTIALMODULE_H

#include "ROSInterruptScheduler/InterruptHandler.h" 
////MJ obsolete?#include "ROSCore/SequentialDataChannel.h"       
#include "ROSCore/ReadoutModule.h"

namespace ROS
{
  class EmulatedSequentialReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    EmulatedSequentialReadoutModule();
    virtual ~EmulatedSequentialReadoutModule() noexcept;

    virtual const std::vector<DataChannel *> *channels();

  private:
    DFCountedPointer<Config> m_configuration;   //A configuration object
    std::vector <unsigned int> m_id;                 //vector of channel IDs
    std::vector <unsigned int> m_rolPhysicalAddress; //Not really a physical address. Maybe relatively dummy but required for API consistency
    std::vector <bool> m_usePolling;                 //Flag indicating if we use polling (we do) or interrupts (we never do in this class) 
    int m_numberOfDataChannels;                 //The number of data channels
    std::vector <DataChannel *> m_dataChannels;	//vector of generic DataChannels of this ReadoutModule
  };

  inline const std::vector<DataChannel *> *EmulatedSequentialReadoutModule::channels()
  {
    return &m_dataChannels;
  }
}
#endif // EMULATEDSEQUENTIALMODULE_H
