//$Id$

/*
  ATLAS ROS Software

  Class: EmulatedSingleFragmentReadoutModule
  Author: J.O.Petersen CERN 	
*/

#ifndef EMULATEDSINGLEFRAGMENTMODULE_H
#define EMULATEDSINGLEFRAGMENTMODULE_H

#include "ROSCore/SingleFragmentDataChannel.h"
#include "ROSCore/ReadoutModule.h"

namespace ROS

{
  class EmulatedSingleFragmentReadoutModule : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    EmulatedSingleFragmentReadoutModule();
    virtual ~EmulatedSingleFragmentReadoutModule()noexcept;

    virtual const std::vector<DataChannel *> * channels();

  private:

    DFCountedPointer<Config> m_configuration;

    // channel parameters
    std::vector <unsigned int> m_id;
    std::vector <unsigned int> m_rolPhysicalAddress;

    int m_numberOfDataChannels;
    std::vector<DataChannel *> m_dataChannels;	// generic DataChannels ??

  };

  inline const std::vector<DataChannel *> * EmulatedSingleFragmentReadoutModule::channels()
  {
    return &m_dataChannels;
  }

}
#endif // EMULATEDSINGLEFRAGMENTMODULE_H
