// -*- c++ -*-
//$Id$

/************************************************************************/
/*  ATLAS ROS Software							*/
/*									*/
/*  Class: RobinReadoutModule						*/
/*  Author: Markus Joos, PH/ESS						*/	
/**** C 2005 - Ecosoft: Made from at least 70% recycled source code *****/

#ifndef ROBINREADOUTMODULE_H
#define ROBINREADOUTMODULE_H

#include <vector>
#include <string>
#include <map>
#include "ROSRobin/Robin.h"
#include "ROSModules/DDTScheduledUserAction.h"
#include "ROSMemoryPool/WrapperMemoryPool.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSModules/RobinDataChannel.h"

namespace ROS 
{
  class RobinReadoutModule : public ReadoutModule 
  {
  public:
    RobinReadoutModule(void);
    virtual ~RobinReadoutModule() noexcept;
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual const std::vector<DataChannel *> * channels();
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopGathering(const daq::rc::TransitionCmd&);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);         //MJ: undo setup() (e.g. ROBIN_Close)
    virtual void clearInfo();
    virtual void publishFullStatistics(void);
    virtual void disable(const std::vector<std::string>&);
    virtual void enable(const std::vector<std::string>&);
    virtual void user(const daq::rc::UserCmd& command);

  private:
    std::string getBistErrorString(u_int bistStatus);

    WrapperMemoryPool *m_memoryPool;
    Robin *m_robIn;                    // A pointer to the PciRobIn object controlling the RobIn hardware
    u_int m_physicalAddress;           // The RobIn ID number (= PCI logical number)
    u_int m_numberOfDataChannels;
    u_int m_firstRolPhysicalAddress;
    u_int m_rolPhysicalAddressTable[3];
    std::map <std::string, RobinDataChannel *> m_uidMap;
    std::vector <DataChannel *> m_dataChannels;
    ScheduledUserAction *m_DDTscheduledUserAction;
    static bool s_firstModuleFound;
    static bool s_DDTCreated;
    DFCountedPointer<Config> m_configuration;
  };
}
#endif 
